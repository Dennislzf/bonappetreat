from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('',
                                              
    # user and authentication urls
    
    #===========================================================================
    # only need to regex expression for whatever follows /Users/ in url
    # for example domain.com/listings/ directs to this file so you only need to
    # declare a r"signup" in urls below and that will trigger the redirect for 
    # domain.com/Users/signup
    #===========================================================================
    url(r'^login', 'UserInfo.views.auth_login'),
    url(r'^logout', 'UserInfo.views.auth_logout'),
    url(r'^signup/user', 'UserInfo.views.user_signup'),
    url(r'^addcompany', 'UserInfo.views.add_company'),	
    url(r'^usersuccess/(?P<tag>\w+)/$', 'UserInfo.views.user_success'),
    url(r'^usercustomize/(?P<tag>\w+)/$', 'UserInfo.views.user_customize'),
    url(r'^editaccount', 'UserInfo.views.edit_account'),
    url(r'^changepassword', 'UserInfo.views.change_password'),

    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )
