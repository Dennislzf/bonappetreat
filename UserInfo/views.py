# Create your views here.
# Create your views here.
from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404  
from UserInfo.models import  *
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
import Image
from django.core.validators import validate_email
from django.core.mail import EmailMessage,send_mail
from django.template.loader import render_to_string
from Users.models import *
import cStringIO
import imghdr
import urllib
from boto.s3.key import Key
import boto
import pygeoip


def user_signup(request):
	if request.method == "GET":
		return render_to_response("UserInfo/user_signup.html",context_instance=RequestContext(request))
	if request.method == "POST":
		email = request.POST.get("email")
		password = request.POST.get("password")
		confirmpassword = request.POST.get("confirmpassword")
		firstname = request.POST.get("firstname")
		lastname = request.POST.get("lastname")
		GEOLOCATION_DIR = settings.GEOLOCATION_DIR
		gi4 = pygeoip.GeoIP(GEOLOCATION_DIR+'GeoLiteCity.dat', pygeoip.MEMORY_CACHE)
		clientipaddress= get_client_ip(request)
		clientaddress =  gi4.record_by_addr(clientipaddress)	
		city = clientaddress.get('city')
		provincestate = clientaddress.get('region_name')
		country = clientaddress.get('country_name')
		form_args = {'email':email,'password':password,'firstname':firstname,'lastname':lastname}
		#backend to check if all fields are filled
		if email is None:
			form_args['errormessage']= "You must enter an email"
			return render_to_response("UserInfo/user_signup.html",form_args,context_instance=RequestContext(request))
		if password is None:
			form_args['errormessage']= "You must enter an password"
			return render_to_response("UserInfo/user_signup.html",form_args,context_instance=RequestContext(request))
		if confirmpassword is None:
			form_args['errormessage']= "You must confirm your password"
			return render_to_response("UserInfo/user_signup.html",form_args,context_instance=RequestContext(request))
		if firstname is None:
			form_args['errormessage']= "You must enter your first name "
			return render_to_response("UserInfo/user_signup.html",form_args,context_instance=RequestContext(request))
		if lastname is None:
			form_args['errormessage']= "You must enter your last name"
			return render_to_response("UserInfo/user_signup.html",form_args,context_instance=RequestContext(request))
		if confirmpassword != password  is None:
			form_args['errormessage']= "Your passwords do not match "
			return render_to_response("UserInfo/user_signup.html",form_args,context_instance=RequestContext(request))
		#check to see if user exists in the system
		try: 
			User.objects.get(username = email)
			form_args['errormessage']= "That email address has been taken "
			return render_to_response("UserInfo/user_signup.html",form_args,context_instance=RequestContext(request))
		except:
			pass
		try:
			validate_email(email)
		except:
			form_args['errormessage']= "You must use a valid email"
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))

		user =User.objects.create_user(email,email,password)
		user.first_name = firstname
		user.last_name = lastname
		user.save()
		userinfo = UserInfo(user=user)
		userinfo.first_name = firstname
		userinfo.last_name = lastname
		userinfo.country = country
		userinfo.provincestate = provincestate
		userinfo.city =city
		userinfo.save()
		#create following list 
		userfollowing = UserFollowing(user=userinfo)
		userfollowedby= UserFollowedBy(user=userinfo)
		userfollowing.save()
		userfollowedby.save()
		# send email to user
		form_args={'user':user}
		msg = EmailMessage('Welcome to WhereTheFork', render_to_string('Emails/signupuseremail.txt', form_args), 'info@wherethefork.com', ['dennislzf@gmail.com'])
		
		msg.send()
		loginuser=authenticate(username=email,password=password)
		login(request,loginuser)
		return HttpResponseRedirect("/")

def company_signup(request):
	if request.method == "GET":
		return render_to_response("UserInfo/company_signup.html",context_instance=RequestContext(request))
	if request.method == "POST":
		email = request.POST.get("email")
		password = request.POST.get("password")
		confirmpassword = request.POST.get("confirmpassword")
		companyname =request.POST.get("companyname")
		
		form_args = {'email':email,'password':password,'companyname':companyname}
		#backend to check if all fields are filled
		if email is None:
			form_args['errormessage']= "You must enter an email"
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))
		if password is None:
			form_args['errormessage']= "You must enter an password"
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))
		if confirmpassword is None:
			form_args['errormessage']= "You must confirm your password"
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))
		if companyname is None:
			form_args['errormessage']= "You must enter your company name "
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))
		if confirmpassword != password  is None:
			form_args['errormessage']= "Your passwords do not match "
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))
		#check to see if user exists in the system
		try: 
			User.objects.get(username = email)
			form_args['errormessage']= "That email address has been taken "
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))
		except:
			pass
		try:
			validate_email(email)
		except:
			form_args['errormessage']= "You must use a valid email"
			return render_to_response("UserInfo/company_signup.html",form_args,context_instance=RequestContext(request))

		group = Group.objects.get(name="company")
		user = User.objects.create_user(email,email,password)
		group.user_set.add(user)
		companyinfo = CompanyInfo(user=user,companyname=companyname)
		companyinfo.save()
		authenticate(username=email,password=password)
		loginuser=authenticate(username=email,password=password)
		login(request,loginuser)
		return HttpResponseRedirect("/myaccount/companysuccess/%s"%str( user.id))

def user_success(request,tag):
	if request.user.is_authenticated() == False:
		raise Http404
	# check to see if user exists
	try:
		user = User.objects.get(id=tag)
	except:
		raise Http404
	if request.user != user:
		raise Http404
	try:
		UserInfo.objects.get(user=user)
	except:
		raise Http404
	return render_to_response('UserInfo/user_success.html',context_instance=RequestContext(request))

# second step of user creation
def user_customize(request,tag):
	if request.user.is_authenticated() == False:
		raise Http404
	# check to see if user exists
	try:
		user = User.objects.get(id=tag)
	except:
		raise Http404
	if request.user != user:
		raise Http404
	try:
		userinfo = UserInfo.objects.get(user=user)
	except:
		raise Http404
	if request.method == "GET":	
		form_args={'userinfo':userinfo}
		return render_to_response('UserInfo/user_customize.html',form_args,context_instance=RequestContext(request))
	if request.method == "POST":
		description = request.POST.get("description")
		image = request.FILES.get("logoupload")
		#check to see if user is uploading picture
		if request.POST.get("isuploadpicture") == "True":
			#see if image is right file type
			try:
	               		testimage= Image.open(image)
			except:
			#if file type is not an image that is supported	
		                errormessage= "The image with filename " + image.name + " is not supported."
				form_args={'userinfo':userinfo,'description':description,'errormessage':errormessage,}
				return render_to_response('UserInfo/user_customize.html',form_args,context_instance=RequestContext(request))
			#check to see if image size is less then 5mb
			if image.size > 5242880:
				errormessage= "The image with filename " + image.name + " is too large (over 4mb)."
				form_args={'userinfo':userinfo,'description':description,'errormessage':errormessage}
				return render_to_response('UserInfo/user_customize.html',form_args,context_instance=RequestContext(request))
					#else is image is not that size
			#if image file is supported
			#delete profile picture and upload new one
			conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
			bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)	
			#uplaod new picture
			k= Key(bucket)	
			#get time and format to make eah filename unique
			time = str(datetime.datetime.now())
			time.replace(" ","")
			time.replace(".","")
			#get file name and extention time
			filename = image.name
			if filename.endswith('.jpg') or filename.endswith('.jpeg'):
				fileextention = 'jpeg'
			elif filename.endswith('.png'):
				fileextention = 'png'
			elif filename.endswith('.gif'):
				fileextention = 'gif'
			else:
				fileextention = None
			#get filename to save to
			filename = filename.split('.')
			filename = filename[0]
			if fileextention == None:
				form_args={'userinfo':userinfo,'description':description,'errormessage':errormessage}
				return render_to_response('UserInfo/user_customize.html',form_args,context_instance=RequestContext(request))



			#saveurl to point there
			url ='photos/users/'+filename+time+"."+fileextention
			#get key name to upload to s3, filename
			k.key='photos/users/'+filename+time+"."+fileextention
			out_im2 = cStringIO.StringIO()
			if fileextention == "jpg":
				fileextention = "jpeg"
			#save image to s3	
			testimage.save(out_im2, fileextention)
			k.set_contents_from_string(out_im2.getvalue())
			#set as public
			k.set_acl('public-read')
			#set url and key in s3 server
			url=urllib.quote_plus(url)	
			userinfo.profilepictureurl= url
			userinfo.s3key= k.key
			userinfo.save()
			form_args={'userinfo':userinfo,'description':description}
			return render_to_response('UserInfo/user_customize.html',form_args,context_instance=RequestContext(request))
		
		#if image isnt being uploaded and user wants to go to next step
		userinfo.description = description
		userinfo.save()
		return HttpResponseRedirect("/")


		return render_to_response('UserInfo/user_customize.html',form_args,context_instance=RequestContext(request))


#login user
def auth_login(request):
    errormessage = None 
    if request.method == "GET":
    	return render_to_response('UserInfo/login.html',context_instance=RequestContext(request))
    if request.method == 'POST':
        username = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(username=username, password = password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            errormessage = "Your username or password is incorrect please try again"
            print errormessage
            form_args = {'errormessage':errormessage,'username':username}
            return render_to_response("UserInfo/login.html",form_args,context_instance = RequestContext(request))
#edit account settings
def edit_account(request):
	if request.user.is_authenticated == False:
		return HttpResponseRedirect('/myaccount/login.html')
	try:
		userinfo = UserInfo.objects.get(user = request.user)
	except:
		raise Http404
	if request.method == "GET":
		form_args = {'userinfo':userinfo}
		return render_to_response("UserInfo/editaccount.html",form_args,context_instance = RequestContext(request))
	if request.method == "POST":
		isaddpicture = request.POST.get("isaddpicture")
		firstname = request.POST.get('firstname')
		lastname  = request.POST.get("lastname")
		city = request.POST.get('city')
		provincestate = request.POST.get('provincestate')
		country = request.POST.get('country')
		description = request.POST.get("description")
		twitterhandle = request.POST.get('twitterhandle')
		instagramhandle = request.POST.get("instagramhandle")
		pintresthandle = request.POST.get('pintresthandle')
		image =  request.FILES.get("profilepicture")
		form_args= {'firstname':firstname,'lastname':lastname,'city':city,'description':description,'twitterhandle':twitterhandle,'instagramhandle':instagramhandle,'pintresthandle':pintresthandle,'userinfo':userinfo,'country':country,'provincestate':provincestate}
		if isaddpicture != "":
			try:
	               		testimage= Image.open(image)

			except:
			#if file type is not an image that is supported	
		                errormessage= "The image with filename " + image.name + " is not supported."
				form_args['errormessage']=errormessage
				return render_to_response('UserInfo/editaccount.html',form_args,context_instance=RequestContext(request))
			#check to see that image size is no larger then 4mb
			if image.size > 5242880:
					errormessage= "The image with filename " + image.name + " is too large (over 4mb)."
					form_args['errormessage']=errormessage
					return render_to_response('UserInfo/editaccount.html',form_args,context_instance=RequestContext(request))
			#if image file is supported
			
			#delete old
			conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
			bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)	
			#if the key is not the default key(i.e if a picture has already been uplaoded)
			if userinfo.s3key is not None:
				k = Key(bucket)	
				k.key = userinfo.s3key
				bucket.delete_key(k)
			#uplaod new picture
			k= Key(bucket)	
			#get time and format to make eah filename unique
			time = str(datetime.datetime.now())
			time.replace(" ","")
			time.replace(".","")
			#get file name and extention time
			filename = image.name
			filename = image.name
			if filename.endswith('.jpg') or filename.endswith('.jpeg'):
				fileextention = 'jpeg'
			elif filename.endswith('.png'):
				fileextention = 'png'
			elif filename.endswith('.gif'):
				fileextention = 'gif'
			else:
				fileextention = None
			#get filename to save to
			filename = filename.split('.')
			filename = filename[0]
			if fileextention == None:
				errormessage= "The image with filename " + image.name + " is too large (over 4mb)."
				form_args['errormessage']=errormessage
				return render_to_response('UserInfo/editaccount.html',form_args,context_instance=RequestContext(request))

			#saveurl to point there
			url ='photos/users/'+filename+time+"."+fileextention
			#get key name to upload to s3, filename
			k.key='photos/users/'+filename+time+"."+fileextention
			out_im2 = cStringIO.StringIO()
			#save image to s3	
			testimage.save(out_im2, fileextention)
			k.set_contents_from_string(out_im2.getvalue())
			#set as public
			k.set_acl('public-read')
			#set url and key in s3 server
			url=urllib.quote_plus(url)	
			userinfo.profilepictureurl= url
			userinfo.s3key= k.key
			userinfo.save()
			userinfo.save()
			return render_to_response('UserInfo/editaccount.html',form_args,context_instance=RequestContext(request))
		user = request.user
		user.first_name = firstname
		user.last_name = lastname
		user.save()
		userinfo.city = city
		userinfo.provicestate= provincestate
		userinfo.country = country
		userinfo.first_name = firstname
		user.last_name = lastname
		userinfo.description = description
		userinfo.twitterhandle = twitterhandle
		userinfo.instagramhandle = instagramhandle
		userinfo.pintresthandle=pintresthandle
		userinfo.save()
		form_args['successmessage'] = "Your changes have been saved"
		return render_to_response('UserInfo/editaccount.html',form_args,context_instance=RequestContext(request))

#change password
def change_password(request):
	if request.user.is_authenticated == False:
		return HttpResponseRedirect("/myaccount/login")
	if request.method == "GET":
		return render_to_response('UserInfo/change_password.html',context_instance=RequestContext(request))
	if request.method == "POST":
		oldpassword = request.POST.get('oldpassword')
		newpassword = request.POST.get('newpassword')
		confirmpassword = request.POST.get('confirmpassword')
		if oldpassword is None:
			errormessage = "You must enter your old password"
			form_args = {'errormessage':errormessage}
			return render_to_response('UserInfo/change_password.html',form_args,context_instance=RequestContext(request))
		if newpassword is None:
			errormessage = "You must enter your new password"
			form_args = {'errormessage':errormessage}
			return render_to_response('UserInfo/change_password.html',form_args,context_instance=RequestContext(request))
		if confirmpassword is None:
			errormessage = "You must confirm your new password"
			form_args = {'errormessage':errormessage}
			return render_to_response('UserInfo/change_password.html',form_args,context_instance=RequestContext(request))
		if request.user.check_password(oldpassword) == False:
			errormessage = "The password you entered is incorrect . Please try again"
			form_args = {'errormessage':errormessage}
			return render_to_response('UserInfo/change_password.html',form_args,context_instance=RequestContext(request))
		if newpassword != confirmpassword:
			errormessage = "Your new passwords do not match "
			form_args = {'errormessage':errormessage}
			return render_to_response('UserInfo/change_password.html',form_args,context_instance=RequestContext(request))

		request.user.set_password(newpassword)
		request.user.save()
		successmessage = "You have successfully changed your password"
		userinfo = UserInfo.objects.get(user = request.user)
		form_args={'userinfo':userinfo,'successmessage':successmessage}
		return render_to_response('UserInfo/editaccount.html',form_args,context_instance=RequestContext(request))

	
def auth_logout(request):
	if request.user.is_authenticated():
        	logout(request)
        	return HttpResponseRedirect('/')
    	else:
        	return HttpResponseRedirect('/myaccount/login')

#add company if doesnt exist
def add_company(request):
	if request.user.is_authenticated() == False:
		return HttpResponseRedirect("/myaccount/login")
	if request.method == "GET":
		return render_to_response('UserInfo/addcompany.html',context_instance=RequestContext(request))
	if request.method == "POST":
		companyname = request.POST.get("companyname")
		address = request.POST.get("address")
		postalcode = request.POST.get("postalcode")
		phone = request.POST.get("phone")
		city = request.POST.get("city")
		neighbourhood=request.POST.get("neighbourhood")
		form_args = {'companyname':companyname,'address':address,'postalcode':postalcode,'phone':phone,'city':city,'neighbourhood':neighbourhood}
		#serverside validation
		if companyname == "":
			errormessage = "You must enter a company name"
			form_arge['errormessage'] = errormessage
			return render_to_response('UserInfo/addcompany.html',form_args,context_instance=RequestContext(request))
		if address == "":
			errormessage = "You must enter an address"
			form_arge['errormessage'] = errormessage
			return render_to_response('UserInfo/addcompany.html',form_args,context_instance=RequestContext(request))
		if postalcode == "":
			errormessage = "You must enter a postal/zip code"
			form_arge['errormessage'] = errormessage
			return render_to_response('UserInfo/addcompany.html',form_args,context_instance=RequestContext(request))
		if phone  == "":
			errormessage = "You must enter a phone number "
			form_arge['errormessage'] = errormessage
			return render_to_response('UserInfo/addcompany.html',form_args,context_instance=RequestContext(request))
		if city  == "":
			errormessage = "You must enter a city"
			form_arge['errormessage'] = errormessage
			return render_to_response('UserInfo/addcompany.html',form_args,context_instance=RequestContext(request))
		
		#get municipal  and neighbourhood
		municipal = city.split(',')
		municipalqueryset = Municipal.objects.filter(name = municipal[0])
		#check to see if db has a municipal with same name
		chosenmunicipal=None
		if municipalqueryset.count() > 1:
			for individualmunicipal in municipalqueryset:
				if individualmunicipal.city.provincestate == municipal[1]:
					chosenmunicipal = individualmunicipal
					break
		else:
			chosenmunicipal = municipalqueryset[0]

	#now that we have the chosen municipal, we can get all the neighbourhoods
		city = chosenmunicipal.city	
		try:
			neighbourhood = Neighbourhood.objects.get(name=neighbourhood,municipal = chosenmunicipal)			
		except:
			neighbourhood  = None

		#check to see if resturant exists
		try:
			company = CompanyInfo.objects.get(companyname = companyname, address=address,phonenumber=phonenumber,postalcode=postalcode)
			errormessage= "A company like this already exists"
			form_args['errormessage'] = errormessage
			return render_to_response('UserInfo/addcompany.html',form_args,context_instance=RequestContext(request))
		except:
			pass
		#if neighbourhood is not none, add neighbourhood, else leave it null
		if neighbourhood != None:
			company = CompanyInfo(companyname = companyname,address = address,city = city,neighbourhood = neighbourhood,municipal = chosenmunicipal, postalcode= postalcode,phonenumber=phone)
		else:
			company = CompanyInfo(companyname = companyname,address = address,city = city,municipal = chosenmunicipal, postalcode= postalcode,phonenumber=phone)
		company.save()
		return HttpResponseRedirect("/items/new")

#get ip address of user
def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[0]
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip




		

#get provinces
def get_provinces():
	choices = [
		"AB",
		"BC",
		"MB",
		"NB",
		"NL",
		"NT",
		"NS",
		"NU",
		"ON",
		"PE",
		"QC",
		"SK",
		"YT"
			]
	return choices

def get_neighbourhood_choices(city):
	choices =[
	"Ambleside",
	"Arbutus Ridge",
	"Burnaby Heights",
	"Central Lonsdale",
	"Champlain Heights",
	"Chinatown",
	"Coal Harbour",
	"Deep Cove",
	"Downtown",
	"Downtown Eastside",
	"Dunbar-Southlands",
	"Dundarave",
	"Edgemont Village",
	"Fairview Slopes",
	"Fraserview",
	"Gastown",
	"Golden Village",
	"Grandview-Woodlands",
	"Granville Entertainment District",
	"Granville Island/False Creek",
	"Hastings-Sunrise",
	"Horseshoe Bay",
	"Kensington-Cedar Cottage",
	"Kerrisdale",
 	"Killarney",
	"Kitsilano",
	"Lower Lonsdale",
	"Lynn Valley",
	"Marpole",
	"Metrotown",
	"Mount Pleasant",
	"Oakridge",
	"Point Grey",
	"Punjabi Market",
	"Renfrew-Collingwood",
	"Riley Park",
	"SFU",
	"Shaughnessy",
	"South Cambie",
	"South Granville",
	"Steveston",
	"Strathcona",
	"Sunset",
	"The Drive",
	"UBC",
	"West End",
	"YVR",
	"Yaletown",
	]	
	return choices
