from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.conf import settings
from Cities.models import *

def upload_to_user(instance, filename):
    return 'photos/Users/%s/%s' % (instance.user.id, filename)

def upload_to_company(instance,filename):
	return 'photos/Company/%s/%s' % (instance.user.id,filename)


                    
GENDER_CHOICES=( ('Male', 'Male'), ('Female', 'Female'),  ('Other', 'Other'),)
class UserInfo(models.Model):
    user= models.ForeignKey(User)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    profilepictureurl = models.CharField(max_length = 200,default="photos/static/default.jpg")
    s3key = models.CharField(max_length=200,blank = True, null = True)
    description = models.CharField(max_length = 500)
    creditcardposted = models.BooleanField(default=False)
    country = models.CharField(max_length=50,blank = False)
    provincestate = models.CharField(max_length =  50, blank=False)
    city = models.CharField(max_length=50,blank=False)
    twitterhandle = models.CharField(max_length=30,blank= False)
    website = models.CharField(max_length=50,blank=False)
    instagramhandle=models.CharField(max_length=30,blank = False)
    itemsposted = models.IntegerField(default = 0)
    pintresthandle= models.CharField(max_length= 30)
    picturesposted = models.IntegerField(default=0,blank = False)
    usersfollowing = models.ManyToManyField(User, related_name="userinfo_usersfollowing")
    usersfollowed = models.ManyToManyField(User,related_name="userinfo_usersfollowed")
    numusersfollowing= models.IntegerField(default = 0)
    numusersfollowed = models.IntegerField(default = 0)
    numberofpoints= models.IntegerField(default=0)
    

class CompanyInfo(models.Model):
	companyname = models.CharField(max_length=200)
	companylogo = models.CharField(max_length=200,default="photo/static/default.jpg")
	address = models.CharField(max_length=200, blank = False)
	city = models.ForeignKey(City)
	neighbourhood = models.ForeignKey(Neighbourhood,blank=True,null=True)
	municipal = models.ForeignKey(Municipal)
	postalcode = models.CharField(max_length = 10, blank = False)
	phonenumber = models.CharField(max_length=15)
	email = models.CharField(max_length = 50)
	visitstopage = models.IntegerField(default=0)
	itemsposted = models.IntegerField(default =  0)
	useradded =models.BooleanField(default= False)


class UserFoodPreference(models.Model):
	user = models.ForeignKey(User)
	tag= models.CharField(max_length=100)


