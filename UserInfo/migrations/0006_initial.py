# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserInfo'
        db.create_table('UserInfo_userinfo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('profilepictureurl', self.gf('django.db.models.fields.CharField')(default='photo/static/default.jpg', max_length=200)),
            ('s3key', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('creditcardposted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('provincestate', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('twitterhandle', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('instagramhandle', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('itemsposted', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('pintresthandle', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('picturesposted', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('numusersfollowing', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('numusersfollowed', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('numberofpoints', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('UserInfo', ['UserInfo'])

        # Adding M2M table for field usersfollowing on 'UserInfo'
        m2m_table_name = db.shorten_name('UserInfo_userinfo_usersfollowing')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userinfo', models.ForeignKey(orm['UserInfo.userinfo'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['userinfo_id', 'user_id'])

        # Adding M2M table for field usersfollowed on 'UserInfo'
        m2m_table_name = db.shorten_name('UserInfo_userinfo_usersfollowed')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userinfo', models.ForeignKey(orm['UserInfo.userinfo'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['userinfo_id', 'user_id'])

        # Adding model 'CompanyInfo'
        db.create_table('UserInfo_companyinfo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('companyname', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('companylogo', self.gf('django.db.models.fields.CharField')(default='photo/static/default.jpg', max_length=200)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Cities.City'])),
            ('neighbourhood', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Cities.Neighbourhood'], null=True, blank=True)),
            ('municipal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Cities.Municipal'])),
            ('postalcode', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('phonenumber', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('visitstopage', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('itemsposted', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('useradded', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('UserInfo', ['CompanyInfo'])

        # Adding model 'UserFoodPreference'
        db.create_table('UserInfo_userfoodpreference', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('tag', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('UserInfo', ['UserFoodPreference'])


    def backwards(self, orm):
        # Deleting model 'UserInfo'
        db.delete_table('UserInfo_userinfo')

        # Removing M2M table for field usersfollowing on 'UserInfo'
        db.delete_table(db.shorten_name('UserInfo_userinfo_usersfollowing'))

        # Removing M2M table for field usersfollowed on 'UserInfo'
        db.delete_table(db.shorten_name('UserInfo_userinfo_usersfollowed'))

        # Deleting model 'CompanyInfo'
        db.delete_table('UserInfo_companyinfo')

        # Deleting model 'UserFoodPreference'
        db.delete_table('UserInfo_userfoodpreference')


    models = {
        'Cities.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'Cities.municipal': {
            'Meta': {'object_name': 'Municipal'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Cities.neighbourhood': {
            'Meta': {'object_name': 'Neighbourhood'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'UserInfo.companyinfo': {
            'Meta': {'object_name': 'CompanyInfo'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'companylogo': ('django.db.models.fields.CharField', [], {'default': "'photo/static/default.jpg'", 'max_length': '200'}),
            'companyname': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'phonenumber': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'postalcode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'useradded': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visitstopage': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'UserInfo.userfoodpreference': {
            'Meta': {'object_name': 'UserFoodPreference'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'UserInfo.userinfo': {
            'Meta': {'object_name': 'UserInfo'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditcardposted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instagramhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'numberofpoints': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'picturesposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pintresthandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'profilepictureurl': ('django.db.models.fields.CharField', [], {'default': "'photo/static/default.jpg'", 'max_length': '200'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            's3key': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'twitterhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'usersfollowed': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowed'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'usersfollowing': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowing'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['UserInfo']