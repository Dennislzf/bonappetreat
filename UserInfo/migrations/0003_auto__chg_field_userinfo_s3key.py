# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'UserInfo.s3key'
        db.alter_column('UserInfo_userinfo', 's3key', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

    def backwards(self, orm):

        # Changing field 'UserInfo.s3key'
        db.alter_column('UserInfo_userinfo', 's3key', self.gf('django.db.models.fields.CharField')(default=None, max_length=200))

    models = {
        'Cities.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'Cities.municipal': {
            'Meta': {'object_name': 'Municipal'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Cities.neighbourhood': {
            'Meta': {'object_name': 'Neighbourhood'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'UserInfo.companyinfo': {
            'Meta': {'object_name': 'CompanyInfo'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'companylogo': ('django.db.models.fields.files.ImageField', [], {'default': "'companydefault.png'", 'max_length': '100'}),
            'companyname': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'phonenumber': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'postalcode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'slogan': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'useradded': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visitstopage': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'UserInfo.userfoodpreference': {
            'Meta': {'object_name': 'UserFoodPreference'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'UserInfo.userinfo': {
            'Meta': {'object_name': 'UserInfo'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditcardposted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instagramhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'numberofpoints': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'picturesposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pintresthandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'profilepictureurl': ('django.db.models.fields.CharField', [], {'default': "'static/img/defaultuser.jpg'", 'max_length': '200'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            's3key': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'twitterhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'usersfollowed': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowed'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'usersfollowing': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowing'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['UserInfo']