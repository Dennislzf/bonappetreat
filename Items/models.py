from django.db import models
# Create your models here.
from django.contrib.auth.models import User
from UserInfo.models import CompanyInfo,UserInfo
from django.conf import settings
from Cities.models import *
def upload_to(instance, filename):
            return 'photos/Item/%s/%s' % (instance.id, filename)

class Item(models.Model):
        postedby = models.ForeignKey(User, related_name="item_postedby")
        companyinfo = models.ForeignKey(CompanyInfo, default=None, null = True, blank = True)
        name  = models.CharField(max_length=100, default=None, null = True, blank = True)
        price = models.DecimalField(max_digits=6 , decimal_places=2, default=None, null = True, blank = True)
        category = models.CharField(max_length=30, default=None)
        municipal = models.ForeignKey(Municipal)
	city = models.ForeignKey(City)
        neighbourhood = models.ForeignKey(Neighbourhood, null = True, blank = True)
        description = models.CharField(max_length= 300)
        fatfree= models.BooleanField(default=False)
        vegan = models.BooleanField(default=False)
        glutenfree = models.BooleanField(default=False)
        peanutfree = models.BooleanField(default=False)
        vegetarian = models.BooleanField(default=False)
        lactosefree = models.BooleanField(default=False)
        mainitemimage  = models.ForeignKey('Items.ItemImage',related_name = "main_image", null=True, blank=True)
        rating = models.PositiveSmallIntegerField(default  = 0)
        viewcount = models.IntegerField(default=0)
        featured  = models.BooleanField(default = False)
        reviewcount = models.IntegerField(default = 0)
        forkedcount = models.IntegerField(default= 0)
        flagcount = models.IntegerField(default = 0)
        flaggedby = models.ManyToManyField(User, related_name="item_flaggedby")
	forkedby = models.ManyToManyField(User,related_name="item_forkedby")
	itemingredient = models.ManyToManyField('Items.ItemIngredient',related_name ="item_itemingredienti")

class ItemIngredient(models.Model):
	ingredient = models.CharField(max_length=50)


class EditItem(models.Model):
	item = models.ForeignKey(Item)
	postedby = models.ForeignKey(User, related_name="edititem_postedby")
        name  = models.CharField(max_length=100, default=None, null = True, blank = True)
        price = models.DecimalField(max_digits=6 , decimal_places=2, default=None, null = True, blank = True)
        category = models.CharField(max_length=30, default=None)
        description = models.CharField(max_length= 300)
        fatfree= models.BooleanField(default=False)
        vegan = models.BooleanField(default=False)
        glutenfree = models.BooleanField(default=False)
        peanutfree = models.BooleanField(default=False)
        vegetarian = models.BooleanField(default=False)
        lactosefree = models.BooleanField(default=False)
	isviewed = models.BooleanField(default= False)
	isaccepted = models.BooleanField(default= False)


class ItemImage(models.Model):
        item = models.ForeignKey(Item,null = True,blank = True)
	postedby  = models.ForeignKey(User)
	finished = models.BooleanField(default= False)
        mainimage = models.BooleanField(default=False)
        key = models.CharField(max_length=100)
        url = models.CharField(max_length=200)


class ItemTag(models.Model):
        item = models.ForeignKey(Item)
        tag= models.CharField(max_length=50)
                        
                        
class ItemPurchase(models.Model):
        item = models.ForeignKey(Item)
        priceperdozen  = models.DecimalField(max_digits=6,decimal_places=2, null = True, blank = True)
        pricehalfdozen = models.DecimalField(max_digits=6,decimal_places= 2,null = True,blank = True)
        allowpickup= models.BooleanField(default=False)
        allowshipping = models.BooleanField(default=False)

class ItemReview(models.Model):
        item = models.ForeignKey(Item)
        postedby = models.ForeignKey(User,related_name = "itemreview_postedby")
        userinfo = models.ForeignKey(UserInfo, default=None, null = True, blank = True)
	recommendedby = models.ManyToManyField(User, related_name="itemreview_recommendedby")
        companyreviewed =models.ForeignKey(CompanyInfo) 
        rating = models.PositiveSmallIntegerField()
        numrecommends = models.PositiveSmallIntegerField(default  = 0)
        comments = models.CharField(max_length=1000,blank = True,null = True)
        dateadded = models.DateTimeField(auto_now_add=True, blank=True)
        flagcount=models.IntegerField(default=0)
        flaggedby = models.ManyToManyField(User, related_name="itemreview_flaggedby")


class UserForked(models.Model):
	item = models.ForeignKey(Item)
	user = models.ForeignKey(User)

class ItemTip(models.Model):
    item =models.ForeignKey(Item)
    postedby = models.ForeignKey(User, related_name="itemtip_postedby")
    companyinfo = models.ForeignKey(CompanyInfo)
    userinfo = models.ForeignKey(UserInfo)
    tip = models.CharField(max_length=200)
    numrecommends = models.IntegerField(default=0)
    recommendedby =models.ManyToManyField(User, related_name="itemtip_recommendedby") 
    dateposted=  models.DateTimeField(auto_now_add=True, blank=True)
