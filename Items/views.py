# Cre# Create your views here.
# Create your views here.
from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from boto.s3.key import Key
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404 ,HttpResponse 
import boto
from UserInfo.models import  UserInfo, CompanyInfo
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
from django.core.validators import validate_email
from Items.models import *
import Image
import json
from random import randint
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.template.loader import render_to_string
from Foods.models import Food
import os
from Cities.models import *
from Users.models import *
import cStringIO
import imghdr
import urllib


def new(request):
	if request.user.is_authenticated == False:
		return HttpResponseRedirect("/myaccount/signup/user.html")
		#get all the food categories
	foods = Food.objects.filter(category = "Food").order_by('foodtype')
	desserts = Food.objects.filter(category = "Dessert").order_by('foodtype')
	bakerygoods  = Food.objects.filter(category="Bakery Goods").order_by('foodtype')
	drinks = Food.objects.filter(category ="Drinks").order_by("foodtype")
	if request.method == "GET":
		items = ItemImage.objects.filter(postedby=request.user, finished = False)
		form_args = {'items':items,'foods':foods,'desserts':desserts,'bakerygoods':bakerygoods,"drinks":drinks}
		return render_to_response("Items/new.html",form_args, context_instance=RequestContext(request))
	if request.method == "POST":
		images = request.FILES.getlist('uploadpictures')
		isuploadpicture = request.POST.get("isuploadpicture")
		
		#if user is uploading pictures
		if isuploadpicture == "True":
			for image in images:
					try:
				    		testimage= Image.open(image)

					except:
							#if file type is not an image that is supported	
					    	errormessage= "The image with filename " + image.name + " is not supported."
						items= ItemImage.objects.filter(postedby = request.user, finished = False)
						form_args={'errormessage':errormessage,'items':items}
						return render_to_response('Items/new.html',form_args,context_instance=RequestContext(request))
							
							#if imagefile is supported
					#check to see that image size is no larger then 4mb
					if image.size > 5242880:
						errormessage= "The image with filename " + image.name + " is too large (over 4mb)."
						items= ItemImage.objects.filter(postedby = request.user, finished = False)
						form_args={'errormessage':errormessage,'items':items}
						return render_to_response('Items/new.html',form_args,context_instance=RequestContext(request))
					#else is image is not that size

					newimage  = ItemImage( postedby = request.user)
					#scale image
					maxwidth= 400
					maxheight=400

					if int(testimage.size[0]) > maxwidth or int(testimage.size[1])>maxheight:
						#see if width is more is more
						if float(testimage.size[0])/float(testimage.size[1]) >1:
							widthratio=  float(testimage.size[1])/float(testimage.size[0])
							height = int(maxwidth*widthratio)
							size = (maxwidth,height)
							testimage = testimage.resize(size,Image.ANTIALIAS)
						#see if height is more is more
						if float(testimage.size[1])/float(testimage.size[0]) >1:
							heightratio=  float(testimage.size[0])/float(testimage.size[1])
							width = int(heightratio*maxwidth)
							size = (width,maxheight)
							testimage = testimage.resize(size,Image.ANTIALIAS)
	

							


					#connect to s3 bucket
					conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
					bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)	
					k= Key(bucket)

					
					#get time and format to make eah filename unique
					time = str(datetime.datetime.now())
					time.replace(" ","")
					time.replace(".","")
					#get file name and extention time
					filename = image.name
					#check to see if file type is image	
					if filename.endswith('.jpg') or filename.endswith('.jpeg'):
						fileextention = 'jpeg'
					elif filename.endswith('.png'):
						fileextention = 'png'
					elif filename.endswith('.gif'):
						fileextention = 'gif'
					else:
						fileextention = None
					#get filename to save to
					filename = filename.split('.')
					filename = filename[0]
					if fileextention == None:

						errormessage= "The image with filename " + image.name + " is not an image"
						items= ItemImage.objects.filter(postedby = request.user, finished = False)
						form_args={'errormessage':errormessage,'items':items}
						return render_to_response('Items/new.html',form_args,context_instance=RequestContext(request))

					#saveurl to point there
					url ='photos/items/'+filename+time+"."+fileextention
					#get key name to upload to s3, filename
					k.key='photos/items/'+filename+time+"."+fileextention
					out_im2 = cStringIO.StringIO()
					#save image to s3	
					testimage.save(out_im2, fileextention, quality=50)
					k.set_contents_from_string(out_im2.getvalue())
					#set as public
					k.set_acl('public-read')
					#set url and key in s3 server
					url=urllib.quote_plus(url)	
					newimage.url = url	
					newimage.key = k.key
					newimage.save()
					

			 #itemimages =add_images(request,images,item)
			items= ItemImage.objects.filter(postedby= request.user,finished = False) 
		
			form_args={'items':items,'desserts':desserts,'foods':foods,'bakerygoods':bakerygoods}
			return render_to_response("Items/new.html",form_args,context_instance=RequestContext(request))

def percent_cb(complete, total):
    sys.stdout.write('.')
    sys.stdout.flush()

#displays item
def single(request,tag):
	#check to see if it exists
	try:
		item = Item.objects.get(id= tag)
	except: 
		raise Http404
    #check to see if user allows purchase
	try:
		itempurchase = ItemPurchase.objects.get(item = item)
	except:
		itempurchase = None
	#get item tags and itemimages	
	itemtags = item.itemingredient.all()
	itemimages = ItemImage.objects.filter(item = item)
	itemreviews = ItemReview.objects.filter(item=item)
	try:
		userinfo = UserInfo.objects.get(user=item.postedby)
	except:
		raise Http404
	#get Company
	try:
		company = CompanyInfo.objects.get(user=item.postedby)
	except:
		company = None
	if request.method =="GET":
		#check to see if user got redirected back after sending review querystring paramenter "reviewsuccess"
		if request.GET.get("reviewsuccess") == "true":
			reviewsuccess = "Thank you for submitting your review!"
		else:
			reviewsuccess = None
		if request.GET.get('addtipsuccess')=='true':
			addtipsuccess= "Thank you for submitting your tip!"
		else:
			addtipsuccess=None
		if request.GET.get("itemeditrequest")=='true':
			editrequest= "Thanks for submitting an edit request! It may take a couple of days to process it"
		else:
			editrequest = False
			#check to see if user has flagged the item
		flagged = None
		for userflagged in item.flaggedby.all():
			if request.user == userflagged:
				flagged = True
		#get number of stars that arent starred
		numunfilledstars = 5- item.rating
		form_args = {'item':item,'itempurchase':itempurchase,'itemtags':itemtags,'itemimages':itemimages,'userinfo':userinfo,
				'company':company,'itemreviews':itemreviews,'reviewsuccess':reviewsuccess,'flagged':flagged,'addtipsuccess':addtipsuccess,'editrequest':editrequest,'numunfilledstars':numunfilledstars}
		return render_to_response("Items/single.html",form_args,context_instance=RequestContext(request))

#function that allows users  to review an item
def review(request,tag):
	if request.user.is_authenticated == False:
		return HttpResponseRedirect("/myaccount/login")
	itemid = tag
	try:
		item = Item.objects.get(id = itemid)
	except:
		raise Http404

	company = item.companyinfo
	#check to see if user has already rated this item
	try:
		userreview = ItemReview.objects.get(item = item, postedby = request.user)
	except:
		userreview = None
	#check to see if user is a user and not a company as companies cant rate each other
	try: 
		userinfo = UserInfo.objects.get(user=request.user)
	except:
		userinfo = None

	if request.method == "GET":

		form_args={'userreview':userreview,'company':company,'item':item, 'userinfo':userinfo}
		return render_to_response("Items/review.html",form_args,context_instance=RequestContext(request))

	if request.method =="POST":
		#check if user wants to delete previous review
		if request.POST.get("isdeletereview") == "true":
			userreview.delete()
			userreview = None
			#calculate new average rating of item
			calculate_new_item_rating(item.id)
			form_args={'userreview':userreview,'company':company,'item':item, 'userinfo':userinfo}
			return render_to_response("Items/review.html",form_args,context_instance=RequestContext(request))
		rating = request.POST.get("rating")
		comments = request.POST.get("comments")
		if userreview == None:
			userreview = ItemReview(item = item,userinfo = userinfo, postedby = request.user,comments = comments,
			 rating = rating, companyreviewed = company )
			userreview.save()
			#add review save to new activity
			recentactivity = RecentActivity(user = userinfo,activitytype = "review",review = userreview)
			recentactivity.save()
			#calculate new average rating of item
			calculate_new_item_rating(item.id)
			return HttpResponseRedirect("/items/single/"+str(item.id)+"?reviewsuccess=true")


#function that allows users to edit items
def edit_item(request,tag):
	if request.user.is_authenticated == False:
		return HttpResponseRedirect("/myaccount/login")
	#try to get item if not exist, raise Http404 error
	try:
		item  = Item.objects.get(id = tag)
	except:
		raise Http404
	
	foods = Food.objects.filter(category = "Food").order_by('foodtype')
	desserts = Food.objects.filter(category = "Dessert").order_by('foodtype')
	bakerygoods  = Food.objects.filter(category="Bakery Goods").order_by('foodtype')
	drinks  = Food.objects.filter(category="Drinks").order_by('foodtype')
	if request.method == "GET":
		#check to see if user has already submitted a request to edit items
		try:
			previousrequest = EditItem.objects.get(postedby= request.user,item=item)
		except:
			previousrequest = None	
		form_args={'item':item,'foods':foods,'desserts':desserts,'bakerygoods':bakerygoods,'previousrequest':previousrequest,'drinks':drinks}
		return render_to_response("Items/edititem.html",form_args,context_instance=RequestContext(request))
	if request.method == "POST":
		name = request.POST.get("name")
		price = request.POST.get('price')
		category = request.POST.get('category')
		description = request.POST.get('description')
		#check to see what is checked
		glutenfree= request.POST.get('glutenfree')
		fatfree= request.POST.get("fatfree")
		peanutfree= request.POST.get('peanutfree')
		vegan = request.POST.get('vegan')
		vegetarian = request.POST.get('vegetarian')
		lactosefree = request.POST.get('lactosefree')
		if glutenfree== "on":
			glutenfree= True
		else:
			glutenfree= False
		if fatfree== "on":
			fatfree= True
		else:
			fatfree= False
		if peanutfree== "on":
			peanutfree= True
		else:
			peanutfree= False
		if vegan== "on":
			vegan= True
		else:
			vegan= False
		if vegetarian== "on":
			vegetarian= True
		else:
			vegetarian= False
		if lactosefree== "on":
			lactosefree= True
		else:
			lactosefree= False
		if name == None:
			name = item.name
		
		edititem = EditItem( name = name , item = item, postedby = request.user,price = price, category = category, description = description, fatfree= fatfree, vegan = vegan, glutenfree = glutenfree,peanutfree = peanutfree, vegetarian = vegetarian, lactosefree= lactosefree)
		edititem.save()
		return HttpResponseRedirect("/items/single/%s?edititemrequest=true" % item.id)

				
#function that allows users to add tips for an item
def add_tip(request,tag):
	if request.user.is_authenticated == False:
		return HttpResponseRedirect("/myaccount/login")
	itemid = tag
	try:
		item = Item.objects.get(id = itemid)
	except:
		raise Http404

	company = item.companyinfo 
	#check to see if user is a user and not a company as companies cant rate each other
	try: 
		userinfo = UserInfo.objects.get(user=request.user)
	except:
		userinfo = None

	if request.method == "GET":

		form_args={'company':company,'item':item, 'userinfo':userinfo}
		return render_to_response("Items/addtip.html",form_args,context_instance=RequestContext(request))

	if request.method =="POST":
		tip = request.POST.get("tip")
		itemtip = ItemTip(item = item, postedby = request.user,tip = tip,userinfo=userinfo,
			 companyinfo  = company )
		itemtip.save()
			#calculate new average rating of item
		return HttpResponseRedirect("/items/single/"+str(item.id)+"?addtipsuccess=true")
	
#calculate new rating of item when item is added
def calculate_new_item_rating(itemid):
	item = Item.objects.get(id = itemid)
	reviews =ItemReview.objects.filter(item = item)
	#check to see if review count is highter then 0
	if reviews.count() > 0:
		rating = 0
		for review in reviews:
			rating = rating + review.rating
		newrating = rating / reviews.count()
		item.rating = newrating
		item.reviewcount = reviews.count()
		item.save()


		# function that deals with setting a picture as a main picture
def set_as_main(request):
	if request.method == "POST" and request.is_ajax():
		imageid = request.POST.get("id")

		#try and get the itemimage and item give id
		try:
			itemimage = ItemImage.objects.get(id= imageid)
			item = itemimage.item
		except:
			raise Http404
		#check to see if user owns the image	
		if item.postedby != request.user:
			raise Http404
		#check to see if there exists a main image
		try:
			mainimage= ItemImage.objects.get(item =item, mainimage = True)
			mainimage.mainimage = False
			mainimage.save()
			#set item mainimage back to none
			item.mainitemimage = None
			item.save()
		except:
			pass
		itemimage.mainimage = True
		itemimage.save()
		#save new itemmainimage
		item.mainitemimage = itemimage
		item.save()

		return HttpResponse(itemimage.id)
	else:
		raise Http404

# ajax function that deletes photo
@csrf_exempt
def delete_photo(request):
	if request.method == "POST" and request.is_ajax():
		imageid = request.POST.get("itemid")

		#try and get the itemimage and item give id
		try:
			itemimage = ItemImage.objects.get(id= imageid)
		except:
			raise Http404
		#check to see if user owns the image	
		if itemimage.postedby != request.user:
			raise Http404
		conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
		bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)	
		k = Key(bucket)	
		k.key = itemimage.key
		bucket.delete_key(k)
		itemimage.delete() 
		return HttpResponse(itemimage.id)
	else:
		raise Http404

@csrf_exempt
#ajax call to flag a review
def flag_review(request):

	if request.method == "POST" and request.is_ajax():
		reviewid = request.POST.get("id")

		#try and get the itemimage and item give id
		try:
			itemreview = ItemReview.objects.get(id= reviewid)
		except:
			raise Http404
		for user in itemreview.flaggedby.all():
			if user == request.user:
				raise Http404
		itemreview.flaggedby.add(request.user)
		itemreview.flagcount = itemreview.flagcount + 1
		itemreview.save()
		return HttpResponse()
	else:
		raise Http404

@csrf_exempt
#ajax call to flag an item
def flag_item(request):

	if request.method == "POST" and request.is_ajax():
		itemid = request.POST.get("id")

		#try and get the itemimage and item give id
		try:
			item = Item.objects.get(id= itemid)
		except:
			raise Http404
		for user in item.flaggedby.all():
			if user == request.user:
				raise Http404
		item.flaggedby.add(request.user)
		item.flagcount = item.flagcount + 1
		item.save()

		return HttpResponse()
	else:
		raise Http404

@csrf_exempt
#save items ajax
def save_item(request):
	if request.is_ajax() and request.method =="POST":
		companyid = request.POST.get("companyid")
		itemid = request.POST.get("itemid")
		name = request.POST.get("name")
		price = request.POST.get("price")			
		category = request.POST.get("category")
		description = request.POST.get("description")
		vegan = request.POST.get("vegan")
		peanutfree = request.POST.get("peanutfree")
		fatfree = request.POST.get("fatfree")
		glutenfree = request.POST.get("glutenfree")
		lactosefree= request.POST.get('lactosefree')
		vegetarian = request.POST.get("vegetarian")
		review = request.POST.get('review')
		rating = request.POST.get('rating')
		tips = request.POST.get("tips")	
		tags = request.POST.get("tags")
		city=  City.objects.get(name = "Vancouver")
		if vegan == "true":
			vegan = True
		else:
			vegan = False
		if glutenfree  == "true":
			glutenfree = True
		else:
			glutenfree = False
		if lactosefree == "true":
			lactosefree = True
		else:
			lactosefree = False
		if vegetarian  == "true":
			vegetarian = True
		else:
			vegetarian = False	
		if fatfree == "true":
			fatfree = True
		else:
			fatfree = False	
		if peanutfree == "true":
			peanutfree = True
		else:
			peanutfree = False	
		#check to see if price is the right format

		if price != "":
			try:
				TWOPLACES = Decimal(10) ** -2
				price =	Decimal(price).quantize(TWOPLACES)
			except:
				raise Http404
		
		#check to see if name,price or category is empty
		if name == ""  or category == "":
			raise Http404
		try:
			company = CompanyInfo.objects.get(id = companyid,city =city)
		except:
			raise Http404
		try:
			itemimage = ItemImage.objects.get(id = itemid)
		except: 
			raise Http404

		#check to see if item exists
		try:
			item= Item.objects.get(name = name,companyinfo = companyinfo)
			raise Http404
		except:
			pass
		item  = Item(postedby = request.user)
		#edit item values
		item.name = name
		item.companyinfo = company
		#check to see if price added
		if price == "":
			pass
		else:	
			item.price = price

		item.category = category
		item.description = description
		item.vegan = vegan
		item.peanutfree = peanutfree
		item.fatfree = fatfree
		item.glutenfree=glutenfree
		item.municipal = company.municipal
		item.vegetarian = vegetarian
		item.lactosefree = lactosefree
		item.neighbourhood = company.neighbourhood	
		item.city  = company.city	
		item.mainitemimage = itemimage
		itemimage.finished = True
		itemimage.mainimage= True
		item.reviewcount = 1
		item.save()
		itemimage.item = item
		itemimage.save()
		userinfo = UserInfo.objects.get(user = request.user)
		userinfo.itemsposted = userinfo.itemsposted +1
		userinfo.picturesposted = userinfo.picturesposted +1
		userinfo.numberofpoints = userinfo.numberofpoints + 3
		userinfo.save()
		company.itemsposted = company.itemsposted +1 
		#if company still has default picture, change to first item added
		if company.companylogo == "photos/static/default.jpg":
			company.companylogo = item.mainitemimage.url 
		company.save()
		itemreview = ItemReview(item = item,postedby = request.user,userinfo  = userinfo,companyreviewed=company,comments = review,rating=rating)
		itemreview.save()
		calculate_new_item_rating(item.id)
		#check if there is a tip added
		if tips != "":
			newtip = ItemTip(item = item,userinfo = userinfo,tip = tips,postedby = request.user,companyinfo=company)
			newtip.save()
			#add new recent activity that user has added tip
			
		#if there are tags, then we split array tags
		if tags != None:
			#convert tags to array of string
			tags= json.loads(tags) 
			for tag in tags:
				newtag = ItemIngredient(ingredient = tag)
				newtag.save()
				item.itemingredient.add(newtag)
		form_args = {'item':item,'itemimage':itemimage,'MEDIA_URL':settings.MEDIA_URL}		
		return HttpResponse( render_to_string('Items/savefinish.html', form_args))		
			
	else:
		raise Http404

@csrf_exempt
#delete item 
def delete_item(request):
	if request.method == "POST" and request.is_ajax():
		itemid = request.POST.get("itemid")
		try:
			item =   Item.objects.get(id=itemid, finished = False, postedby = request.user)
			itemimage=  ItemsImage.objects.get(item = item)
		except:
			raise Http404
		itemimage.delete()
		conn = S3Connection(AWS_ACCESS_KEY, AWS_SECERET_KEY)
		b = Bucket(conn, S3_BUCKET_NAME)
		k = Key(b)
		k.key = 'images/my-images/'+filename
		b.delete_key(k)
		item.delete()
		return HttpResponse()


	else:
		raise Http404
@csrf_exempt
#fork an item ajax
def fork_item(request):
	if request.method == "POST" and request.is_ajax():
		if request.user.is_authenticated() is False:
			raise Http404
		itemid = request.POST.get("id")
		#get item
		try:
			item = Item.objects.get(id = itemid)
		except:
			raise Http404
		#cheeck to see if user has already forked the item
		if request.user in item.forkedby.all():
			raise Http404
		try:
			userforked = UserForked.objects.get(user= request.user,item = item)
			raise Http404
		except:
			pass
		#save forked item in user forked page
		userforked = UserForked(user =request.user, item = item)
		userforked.save()
		item.forkedby.add(request.user)
		item.forkedcount = item.forkedcount + 1
		item.save()
		return HttpResponse(item.forkedcount)
	else:	
		raise Http404
@csrf_exempt
#unfork an item ajax
def unfork_item(request):
	if request.method == "POST" and request.is_ajax():
		itemid = request.POST.get("id")
		#get item
		try:
			item = Item.objects.get(id= itemid)
		except:
			raise Http404
		#remove user from forked by
		
		if request.user in item.forkedby.all():
			item.forkedby.remove(request.user)
		else:
			raise Http404
		
		try:
			userforked = UserForked.objects.get(user=request.user,item=item)
		except:
			raise Http404
		userforked.delete()
		#redo forked count and send back at httpresponse
		item.forkedcount = item.forkedcount -1
		item.save()
		return HttpResponse(item.forkedcount)
@csrf_exempt
#suggest company ajax for adding pictures
def company_suggest(request):
	query = request.GET.get('query')
	city=  City.objects.get(name = "Vancouver")
	companys = CompanyInfo.objects.filter(companyname__icontains = query, city =city)
	suggestions = []
	data=[]
	#add all suggestions queried from the db
	for company in companys:
		suggestions.append({'value':company.companyname +"<br/><span class='addressautosuggest'>"+ company.address+'</span>', 'data': company.id})
	
	#add "Can't Find company" to suggestions
	suggestions.append({'value':"Can't Find Company",'data':"No Company"})
		
		
	dictdata = {'suggestions':suggestions,'query':query}
	jsondata=json.dumps(dictdata)
	mimetype = 'application/json'

	return HttpResponse(jsondata,mimetype)
@csrf_exempt
#ajax item suggest whe nadding menu items
def item_suggest(request):
	query= request.GET.get('query')
	city = City.objects.get(name="Vancouver")
	company = request.GET.get("company")
	try:
		company = CompanyInfo.objects.get(id = company)
	except:
		raise Http404
	queryitems = Item.objects.filter(companyinfo = company, name__icontains = query)
	suggestions = []

	for item in queryitems:
		suggestions.append(item.name)
	data = {'suggestions':suggestions,'query':query}
	jsondata = json.dumps(data)
	mimetype = 'application/json'
	return HttpResponse(jsondata,mimetype)

# save picture if item already exists
@csrf_exempt
def save_picture(request):
	if request.is_ajax and request.method == "POST":
		pictureid = request.POST.get("itemid")
		itemname = request.POST.get("name")
		companyid = request.POST.get("companyid")
		review = request.POST.get('review')
		rating = request.POST.get('rating')
		tips = request.POST.get("tips")
		city = City.objects.get(name="Vancouver")
		try:
			itemimage = ItemImage.objects.get(id = pictureid)
			company = CompanyInfo.objects.get(id = companyid, city  = city)
			item  = Item.objects.get(name=itemname, companyinfo =company)
		except:
			raise Http404
		
		itemimage.item = item
		itemimage.finished = True
		itemimage.save()
		userinfo = UserInfo.objects.get(user = request.user)
		userinfo.picturesposted = userinfo.picturesposted + 1
		userinfo.numberofpoints = userinfo.numberofpoints + 1
		userinfo.save()
		#add tips and add rating
		#check if user has already added a rating
		try:
			userreview = ItemReview.objects.get(postedby = request.user,item = item)
		except:
			userreview= None
		#if user has not submitted a review
		if userreview == None:
			itemreview = ItemReview(item = item,postedby = request.user,userinfo  = userinfo,companyreviewed=company,comments = review,rating=rating)
			itemreview.save()
		#if user has not submtited a review
		else:
			userreview.comments = review
			userreview.rating = rating
			userreview.save()
		#calculate new rating
		calculate_new_item_rating(item.id)

		#check if there is a tip added
		if tips != "":
			newtip = ItemTip(item = item,userinfo = userinfo,tip = tips,postedby = request.user,companyinfo=company)
			newtip.save()



		form_args = {'item':item,'itemimage':itemimage,'MEDIA_URL':settings.MEDIA_URL}		
		return HttpResponse( render_to_string('Items/savefinish.html', form_args))

		
	else:
		raise Http404	
@csrf_exempt
# ajax load of more pictures of item
def load_more_pictures(request):
	itemid  = request.POST.get("id")
	try:
		item = Item.objects.get(id= itemid)
	except:
		raise Http404
	
	itemimages = ItemImage.objects.filter(item = item, mainimage= False)
	MEDIA_URL = settings.MEDIA_URL
	form_args= {'itemimages':itemimages,'MEDIA_URL':MEDIA_URL}
	return HttpResponse( render_to_string('Items/morepictures.html', form_args))

@csrf_exempt
#loading items tips
def load_item_tips(request):
	postid = request.POST.get("id")
	#type is used to determine whether we are laoding all tips posted by a user or all tips posted for an item
	loadtype = request.POST.get("type")
	#if loading all item tips for item
	if loadtype == "item":
		try:
			item = Item.objects.get(id = postid)
			
			itemtips  = ItemTip.objects.filter(item=item).order_by("-numrecommends")
		except:
			raise Http404
		#load all itemtips posted by user
	elif loadtype == "user":	
		try:
			user = User.objects.get(id = postid)
		except:
			raise Http404
		itemtips = ItemTip.objects.filter(postedby = user).order_by("-numrecommends")
	#if type is company
	elif loadtype == "company":
		try:
			company = CompanyInfo.objects.get(id= postid)
		except:
			raise Http404
		itemtips = ItemTip.objects.filter(companyinfo = company)
		
	#get item tips
	MEDIA_URL = settings.MEDIA_URL
	form_args= {'itemtips':itemtips,'MEDIA_URL':MEDIA_URL,'user':request.user, 'loadtype':loadtype}
	return HttpResponse( render_to_string('Items/load_item_tips.html', form_args))

@csrf_exempt
#ajax recommend itemtip
def recommend_tip(request):
	if request.method == "POST" and request.is_ajax:
		tipid = request.POST.get("tipid")
		#get item tips
		try:
			tip = ItemTip.objects.get(id = tipid)
		except:
			raise Http404
		#increase recommend num and add user to recommended by list
		tip.numrecommends = tip.numrecommends + 1
		if request.user in tip.recommendedby.all():
			raise Http404
		tip.recommendedby.add(request.user)
		tip.save()
		return HttpResponse(tip.numrecommends)
	else:
		raise Http404
@csrf_exempt
#recommend review ajax
def recommend_review(request):
	if request.method == "POST" and request.is_ajax:
		reviewid = request.POST.get("reviewid")
		#get item tips
		try:
			review = ItemReview.objects.get(id = reviewid)
		except:
			raise Http404
		#increase recommend num and add user to recommended by list
		review.numrecommends = review.numrecommends + 1
		if request.user in review.recommendedby.all():
			raise Http404
		review.recommendedby.add(request.user)
		review.save()
		return HttpResponse(review.numrecommends)
	else:
		raise Http404

@csrf_exempt
#if user is staff allow to feature item
def feature_item(request):
	if request.method == "POST" and request.is_ajax and request.user.is_staff:
		print "hi"
		itemid = request.POST.get("id")
		try:
			item = Item.objects.get(id = itemid)
		except:
			raise Http404
		item.featured = True
		item.save()
		return HttpResponse()
	else:
		raise Http404	

@csrf_exempt
#if user is staff allow to unfeature item
def unfeature_item(request):
	if request.method == "POST" and request.is_ajax and request.user.is_staff:
		itemid = request.POST.get("id")
		try:
			item = Item.objects.get(id = itemid)
		except:
			raise Http404
		item.featured = False
		item.save()
		return HttpResponse()
	else:
		raise Http404
def  get_categories(request):
	 
	categories = []
	return categories
