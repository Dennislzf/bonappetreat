from django.contrib import admin
from Items.models import *

admin.site.register(Item)
admin.site.register(EditItem)
admin.site.register(ItemImage)
admin.site.register(ItemIngredient)
admin.site.register(ItemReview)
admin.site.register(UserForked)
admin.site.register(ItemTip)