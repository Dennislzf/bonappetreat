
from django.conf.urls import patterns, include, url
from django.conf import settings

urlpatterns = patterns('',

		url(r'^new', 'Items.views.new'),
		url(r'^single/(?P<tag>\w+)/$', 'Items.views.single'),
		url(r'^edit/(?P<tag>\w+)/$', 'Items.views.edit_item'),
		#ajax set picture as main
		url(r'^setasmain', 'Items.views.set_as_main'),
		#ajax delete picture
		url(r'^deletephoto', 'Items.views.delete_photo'),
		url(r'^review/(?P<tag>\w+)/$','Items.views.review'),
		url(r'^addtip/(?P<tag>\w+)/$','Items.views.add_tip'),
		
		url(r'^flagreview', 'Items.views.flag_review'),
		url(r'^flagitem', 'Items.views.flag_item'),
		url(r'^saveitem', 'Items.views.save_item'),
		url(r'^deleteitem', 'Items.views.delete_item'),
		url(r'^forkitem', 'Items.views.fork_item'),
		url(r'^unforkitem', 'Items.views.unfork_item'),
		url(r'^companysuggest', 'Items.views.company_suggest'),
		url(r'^itemsuggest', 'Items.views.item_suggest'),
		url(r'^savepicture', 'Items.views.save_picture'),
		url(r'^loadmorepictures', 'Items.views.load_more_pictures'),
		url(r'^loaditemtips', 'Items.views.load_item_tips'),
		url(r'^recommendtip', 'Items.views.recommend_tip'),
		url(r'^recommendreview', 'Items.views.recommend_review'),
		url(r'^featureitem', 'Items.views.feature_item'),
		url(r'^unfeatureitem', 'Items.views.unfeature_item'),
		
	         #set root for static files (css, images, etc)
	         url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
	         url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
													    )

