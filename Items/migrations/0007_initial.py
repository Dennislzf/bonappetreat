# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Item'
        db.create_table('Items_item', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('postedby', self.gf('django.db.models.fields.related.ForeignKey')(related_name='item_postedby', to=orm['auth.User'])),
            ('companyinfo', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['UserInfo.CompanyInfo'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default=None, max_length=100, null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=None, null=True, max_digits=6, decimal_places=2, blank=True)),
            ('allowpurchase', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('category', self.gf('django.db.models.fields.CharField')(default=None, max_length=30)),
            ('municipal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Cities.Municipal'])),
            ('neighbourhood', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Cities.Neighbourhood'], null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('fatfree', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('vegan', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('glutenfree', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('peanutfree', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('vegetarian', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('lactosefree', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mainitemimage', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='main_image', null=True, to=orm['Items.ItemImage'])),
            ('rating', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('viewcount', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('featured', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('reviewcount', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('forkedcount', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('flagcount', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('Items', ['Item'])

        # Adding M2M table for field flaggedby on 'Item'
        m2m_table_name = db.shorten_name('Items_item_flaggedby')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('item', models.ForeignKey(orm['Items.item'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['item_id', 'user_id'])

        # Adding M2M table for field forkedby on 'Item'
        m2m_table_name = db.shorten_name('Items_item_forkedby')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('item', models.ForeignKey(orm['Items.item'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['item_id', 'user_id'])

        # Adding model 'ItemImage'
        db.create_table('Items_itemimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.Item'], null=True, blank=True)),
            ('postedby', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('finished', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mainimage', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('Items', ['ItemImage'])

        # Adding model 'ItemTag'
        db.create_table('Items_itemtag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.Item'])),
            ('tag', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('Items', ['ItemTag'])

        # Adding model 'ItemPurchase'
        db.create_table('Items_itempurchase', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.Item'])),
            ('priceperdozen', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('pricehalfdozen', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('allowpickup', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('allowshipping', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('Items', ['ItemPurchase'])

        # Adding model 'ItemReview'
        db.create_table('Items_itemreview', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.Item'])),
            ('postedby', self.gf('django.db.models.fields.related.ForeignKey')(related_name='itemreview_postedby', to=orm['auth.User'])),
            ('userinfo', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['UserInfo.UserInfo'], null=True, blank=True)),
            ('companyreviewed', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['UserInfo.CompanyInfo'])),
            ('rating', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('numrecommends', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
            ('dateadded', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('flagcount', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('Items', ['ItemReview'])

        # Adding M2M table for field recommendedby on 'ItemReview'
        m2m_table_name = db.shorten_name('Items_itemreview_recommendedby')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('itemreview', models.ForeignKey(orm['Items.itemreview'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['itemreview_id', 'user_id'])

        # Adding M2M table for field flaggedby on 'ItemReview'
        m2m_table_name = db.shorten_name('Items_itemreview_flaggedby')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('itemreview', models.ForeignKey(orm['Items.itemreview'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['itemreview_id', 'user_id'])

        # Adding model 'UserForked'
        db.create_table('Items_userforked', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.Item'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal('Items', ['UserForked'])

        # Adding model 'ItemTip'
        db.create_table('Items_itemtip', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.Item'])),
            ('postedby', self.gf('django.db.models.fields.related.ForeignKey')(related_name='itemtip_postedby', to=orm['auth.User'])),
            ('companyinfo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['UserInfo.CompanyInfo'])),
            ('userinfo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['UserInfo.UserInfo'])),
            ('tip', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('numrecommends', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('dateposted', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('Items', ['ItemTip'])

        # Adding M2M table for field recommendedby on 'ItemTip'
        m2m_table_name = db.shorten_name('Items_itemtip_recommendedby')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('itemtip', models.ForeignKey(orm['Items.itemtip'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['itemtip_id', 'user_id'])


    def backwards(self, orm):
        # Deleting model 'Item'
        db.delete_table('Items_item')

        # Removing M2M table for field flaggedby on 'Item'
        db.delete_table(db.shorten_name('Items_item_flaggedby'))

        # Removing M2M table for field forkedby on 'Item'
        db.delete_table(db.shorten_name('Items_item_forkedby'))

        # Deleting model 'ItemImage'
        db.delete_table('Items_itemimage')

        # Deleting model 'ItemTag'
        db.delete_table('Items_itemtag')

        # Deleting model 'ItemPurchase'
        db.delete_table('Items_itempurchase')

        # Deleting model 'ItemReview'
        db.delete_table('Items_itemreview')

        # Removing M2M table for field recommendedby on 'ItemReview'
        db.delete_table(db.shorten_name('Items_itemreview_recommendedby'))

        # Removing M2M table for field flaggedby on 'ItemReview'
        db.delete_table(db.shorten_name('Items_itemreview_flaggedby'))

        # Deleting model 'UserForked'
        db.delete_table('Items_userforked')

        # Deleting model 'ItemTip'
        db.delete_table('Items_itemtip')

        # Removing M2M table for field recommendedby on 'ItemTip'
        db.delete_table(db.shorten_name('Items_itemtip_recommendedby'))


    models = {
        'Cities.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'Cities.municipal': {
            'Meta': {'object_name': 'Municipal'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Cities.neighbourhood': {
            'Meta': {'object_name': 'Neighbourhood'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Items.item': {
            'Meta': {'object_name': 'Item'},
            'allowpurchase': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '30'}),
            'companyinfo': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['UserInfo.CompanyInfo']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'fatfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'flagcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'flaggedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'item_flaggedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'forkedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'item_forkedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'forkedcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'glutenfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lactosefree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mainitemimage': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'main_image'", 'null': 'True', 'to': "orm['Items.ItemImage']"}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'peanutfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'item_postedby'", 'to': "orm['auth.User']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': 'None', 'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'rating': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'reviewcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'vegan': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'vegetarian': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'viewcount': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'Items.itemimage': {
            'Meta': {'object_name': 'ItemImage'},
            'finished': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']", 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mainimage': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'Items.itempurchase': {
            'Meta': {'object_name': 'ItemPurchase'},
            'allowpickup': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'allowshipping': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'pricehalfdozen': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'priceperdozen': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        'Items.itemreview': {
            'Meta': {'object_name': 'ItemReview'},
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'companyreviewed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.CompanyInfo']"}),
            'dateadded': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'flagcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'flaggedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemreview_flaggedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'numrecommends': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'itemreview_postedby'", 'to': "orm['auth.User']"}),
            'rating': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'recommendedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemreview_recommendedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'userinfo': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['UserInfo.UserInfo']", 'null': 'True', 'blank': 'True'})
        },
        'Items.itemtag': {
            'Meta': {'object_name': 'ItemTag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Items.itemtip': {
            'Meta': {'object_name': 'ItemTip'},
            'companyinfo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.CompanyInfo']"}),
            'dateposted': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'numrecommends': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'itemtip_postedby'", 'to': "orm['auth.User']"}),
            'recommendedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemtip_recommendedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'tip': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'userinfo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.UserInfo']"})
        },
        'Items.userforked': {
            'Meta': {'object_name': 'UserForked'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'UserInfo.companyinfo': {
            'Meta': {'object_name': 'CompanyInfo'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'companylogo': ('django.db.models.fields.CharField', [], {'default': "'photo/static/default.jpg'", 'max_length': '200'}),
            'companyname': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'phonenumber': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'postalcode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'useradded': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visitstopage': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'UserInfo.userinfo': {
            'Meta': {'object_name': 'UserInfo'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditcardposted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instagramhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'numberofpoints': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'picturesposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pintresthandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'profilepictureurl': ('django.db.models.fields.CharField', [], {'default': "'photo/static/default.jpg'", 'max_length': '200'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            's3key': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'twitterhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'usersfollowed': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowed'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'usersfollowing': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowing'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Items']