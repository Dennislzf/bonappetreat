# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'ItemImage.key'
        db.add_column('Items_itemimage', 'key',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=100),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'ItemImage.key'
        db.delete_column('Items_itemimage', 'key')


    models = {
        'Cities.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'Cities.municipal': {
            'Meta': {'object_name': 'Municipal'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Cities.neighbourhood': {
            'Meta': {'object_name': 'Neighbourhood'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Items.item': {
            'Meta': {'object_name': 'Item'},
            'allowpurchase': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '30'}),
            'companyinfo': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['UserInfo.CompanyInfo']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'fatfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'flagcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'flaggedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'item_flaggedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'forkedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'item_forkedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'forkedcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'glutenfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lactosefree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mainitemimage': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'main_image'", 'null': 'True', 'to': "orm['Items.ItemImage']"}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'peanutfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'item_postedby'", 'to': "orm['auth.User']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': 'None', 'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'rating': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'reviewcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'vegan': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'vegetarian': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'viewcount': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'Items.itemimage': {
            'Meta': {'object_name': 'ItemImage'},
            'finished': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']", 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mainimage': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'Items.itempurchase': {
            'Meta': {'object_name': 'ItemPurchase'},
            'allowpickup': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'allowshipping': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'pricehalfdozen': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'priceperdozen': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        'Items.itemreview': {
            'Meta': {'object_name': 'ItemReview'},
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'companyreviewed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.CompanyInfo']"}),
            'dateadded': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'flagcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'flaggedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemreview_flaggedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'numrecommends': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'itemreview_postedby'", 'to': "orm['auth.User']"}),
            'rating': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'recommendedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemreview_recommendedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'userinfo': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['UserInfo.UserInfo']", 'null': 'True', 'blank': 'True'})
        },
        'Items.itemtag': {
            'Meta': {'object_name': 'ItemTag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Items.itemtip': {
            'Meta': {'object_name': 'ItemTip'},
            'companyinfo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.CompanyInfo']"}),
            'dateposted': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'numrecommends': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'itemtip_postedby'", 'to': "orm['auth.User']"}),
            'recommendedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemtip_recommendedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'tip': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'userinfo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.UserInfo']"})
        },
        'Items.userforked': {
            'Meta': {'object_name': 'UserForked'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'UserInfo.companyinfo': {
            'Meta': {'object_name': 'CompanyInfo'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'companylogo': ('django.db.models.fields.files.ImageField', [], {'default': "'companydefault.png'", 'max_length': '100'}),
            'companyname': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'phonenumber': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'postalcode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'slogan': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'useradded': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visitstopage': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'UserInfo.userinfo': {
            'Meta': {'object_name': 'UserInfo'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditcardposted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instagramhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'numberofpoints': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'picturesposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pintresthandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'profilepicture': ('django.db.models.fields.files.ImageField', [], {'default': "'defaultuser.jpg'", 'max_length': '100'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'twitterhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'usersfollowed': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowed'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'usersfollowing': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowing'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Items']