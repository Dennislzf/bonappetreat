  $(document).ready(function(){
  if (window.screen.width < 500) {   
            $.ajax({
			type:"GET",
			url:"/checkmobile",
			datatype: "json",
			success:function(data){
				if (data == null){
					alert('hi')
					 window.location.href = '/mobile'; //for example
				}

				
				
			}})                                                                     
             
          }

  })



//function that allows superusers to featureitems
$(document.body).on('click','.featureitem',function(){
	id = this.id
	$.ajax({
			type:"POST",
			url:"/items/featureitem",
			datatype: "json",
			data:{'id':id,
				},
			success:function(data){
				$('.featureitem').html("UnFeature Item")
				$('.featureitem').removeClass("featureitem btn-warning").addClass("unfeatureitem btn-danger")
				
			}})

})

//function that allows staff to unfeatureitems
$(document.body).on('click','.unfeatureitem',function(){
	id = this.id
	$.ajax({
			type:"POST",
			url:"/items/unfeatureitem",
			datatype: "json",
			data:{'id':id,
				},
			success:function(data){
				$('.unfeatureitem').html("Feature Item")
				$('.unfeatureitem').addClass("featureitem btn-warning").removeClass("unfeatureitem btn-danger")
				
			}})

})
//function that runs the next and previous button clicks
$('.buttonnav').click(function(){
	id = this.id
	//check to see if user is looking for next page
	if(id.indexOf("next")!=-1){
		action = "next"
	}
	else{
		action = "previous"
	}
	//check to see which subsection user is using
	if (id =="prevpagefeatured" || id =="nextpagefeatured"){
		sliderelement = 'featured'
	}
	else if (id == "prevpagetoprated" || id =="nextpagetoprated"){
		sliderelement='toprated'
	}
	else if (id == "prevpagehiddengems"|| id == "nextpagehiddengems"){
		sliderelement = "hiddengems"
	}
	else if (id == "prevpagecheapeats" || id == "nextpagecheapeats"){
		sliderelement= "cheapeats"
	}
	else if (id == "prevpageeverything" || id == "nextpageeverything"){
		sliderelement= "everything"
	}

		navigationSlider(action,sliderelement)
		
})
//navigation used for top rated items
function navigationSlider(action,sliderelement){
		$.ajax({
			type:"POST",
			url:"/navigationslider",
			datatype: "json",
			data:{'action':action,
				'sliderelement':sliderelement,
				},
			success:function(data){
				//if data returned isnt none
				if(data.indexOf("itemslider") != -1){

				//if action is right, then slide right
					if (action =="next"){
						$('#'+sliderelement+'container').hide('slide',{direction:'left'});
						$('#'+sliderelement+'container').html("")
						$('#'+sliderelement+'container').html(data)
						$('#'+sliderelement+'container').fadeIn("slow",function(){});
					}
					else if(action == "previous"){
						$('#'+sliderelement+'container').hide('slide',{direction:'right'});
						$('#'+sliderelement+'container').html("")
						$('#'+sliderelement+'container').html(data)
						$('#'+sliderelement+'container').fadeIn("slow",function(){});
					}
				}
				return false;
}})
}
//navigation used for featured items
function navigationFeatured(id,action){
		$.ajax({
			type:"POST",
			url:"/navigationfeatured",
			datatype: "json",
			data:{'action':action,
				},
			success:function(data){
				//if data returned isnt none
				if(data.indexOf("itemslider") != -1){

				//if action is right, then slide right
					if (action =="next"){
						$('#featureditemscontainer').hide('slide',{direction:'left'},1000);
						$('#featureditemscontainer').html(data)
						$('#featureditemscontainer').fadeIn("slow",function(){});
					}
					else if(action == "previous"){
						$('#featureditemscontainer').hide('slide',{direction:'right'},1000);
						$('#featureditemscontainer').html(data)
						$('#featureditemscontainer').fadeIn("slow",function(){});
					}
				}
				return false;
}})
}
//if user not logged in and clicks fork item, throw an error
$(".forknotloggedin").hover(function(e) {
	    $($(this).data("tooltip")).css({
		            left: e.pageX + 1,
	           	 top: e.pageY + 1
		        }).stop().show(100);
}, function() {
	    $($(this).data("tooltip")).hide();
});




//open google maps in new window
function redirectGoogleMap(address,municipalname,country){
	window.open('https://maps.google.ca/maps?q='+encodeURIComponent(address+',')+encodeURIComponent(municipalname+',')+encodeURIComponent(country));
}	

$(document.body).on('click','.recommendtipbutton',function(){
	tipid = this.name
		$.ajax({
		type:"POST",
		url:"/items/recommendtip",
		datatype: "json",
		data:{"tipid":tipid,
			},
		success:function(data){
			$("#recommendtip"+tipid).fadeOut('slow',function(){});
			$("#recommendtip"+tipid).html("Tip Recommended");
			$("#recommendtip"+tipid).addClass("btn-primary")
			$("#recommendtip"+tipid).fadeIn('slow',function(){});
			$("#numrecommended"+tipid).html(data)
			return false;
}})


})

//recommend review button click
$(document.body).on('click','.recommendreviewbutton',function(){
	reviewid = this.name
		$.ajax({
		type:"POST",
		url:"/items/recommendreview",
		datatype: "json",
		data:{"reviewid":reviewid,
			},
		success:function(data){
			$("#recommendreview"+reviewid).fadeOut('slow',function(){});
			$("#recommendreview"+reviewid).html("Review Recommended");
			$("#recommendreview"+reviewid).addClass("btn-primary")
			$("#recommendreview"+reviewid).fadeIn('slow',function(){});
			$("#numrecommendedreview"+reviewid).html(data)
			return false;
}})


})
//follow button hover- if mouse enters, chage button to red and text to unfollow. On mouse leave, cange button to green and text back to following
$(document.body).on('mouseenter mouseleave','.unfollowbutton',function(ev){
	followid = this.name
	if (ev.type =='mouseenter'){
	$('#follow'+followid).removeClass('btn-success')
	$('#follow'+followid).addClass('btn-danger')
	$('#follow'+followid).html('Unfollow')

	}
	if (ev.type == 'mouseleave'){
	$('#follow'+followid).removeClass('btn-danger')
	$('#follow'+followid).addClass('btn-success')
	$('#follow'+followid).html('Following')

	}
	return false;


})


//unfollow button click ajax
$(document.body).on('click','.unfollowbutton',function(){
	followid = this.name
		$.ajax({
		type:"POST",
		url:"/users/unfollowuser",
		datatype: "json",
		data:{"followid":followid,
			},
		success:function(data){
			if( $('#numfollowers').length){
			numfollowers = document.getElementById("numfollowers").innerHTML
			numfollowers = parseInt(numfollowers)
			numfollowers = numfollowers - 1
			document.getElementById('numfollowers').innerHTML = numfollowers
			}	
			$('#follow'+followid).fadeOut('fast',function(){})
			$('#follow'+followid).removeClass('btn-danger')
			$('#follow'+followid).addClass('btn-primary')
			$('#follow'+followid).addClass('followbutton')
			$('#follow'+followid).html('Follow')
			$('#follow'+followid).fadeIn('fast',function(){})
			$('#follow'+followid).removeClass('unfollowbutton')
			

			return false;
}})


})
//follow button click ajax
$(document.body).on('click','.followbutton',function(){
	followid = this.name
		$.ajax({
		type:"POST",
		url:"/users/followuser",
		datatype: "json",
		data:{"followid":followid,
			},
		success:function(data){
			if( $('#numfollowers').length){
			numfollowers = document.getElementById("numfollowers").innerHTML
			numfollowers = parseInt(numfollowers)
			numfollowers = numfollowers + 1
			document.getElementById('numfollowers').innerHTML = numfollowers
			}	
			$('#follow'+followid).fadeOut('fast',function(){})
			$('#follow'+followid).removeClass('btn-primary')
			$('#follow'+followid).addClass('btn-success')
			$('#follow'+followid).addClass('unfollowbutton')
			$('#follow'+followid).html('Following')
			$('#follow'+followid).fadeIn('fast',function(){})
			$('#follow'+followid).removeClass('followbutton')
			return false;
}})


})
//close button of pictureframe
$(document.body).on('click','.closepictureframe',function(){
//	$('.simpleoverlay').hide()
	$('.pictureframe').hide()
})

//load pictures and place them in frame
$(document.body).on('click','.itemframeload',function(){

	$('.picturecloseup').attr('src',this.src)	
//	$('.simpleoverlay').show()
	$('.pictureframe').show()
	
})
//function that deals with clicking search bar button
$('.searchbarbutton').click(function(){
	searchMainBar()
})
//function that allows search
function searchMainBar(){
		query = $('#mainsearchbar').val()
		category = $('#mainsearchcategory').val()
		window.location.href= "/search?q="+encodeURIComponent(query)+"&category="+category


}
//allow search of main search bar
$("#mainsearchbar").keyup(function(event){
	if(event.keyCode == 13){
		searchMainBar();
		}	
});


//if user cannt find item then display new item(items/new)
$(document.body).on('click','.noitemfound',function(){
	id = $(this).attr('itemno')
	$('#additem'+id).fadeIn('slow',function(){})
	$('#reviewstipsdiv'+id).fadeIn('slow',function(){})
	$('#additembutton'+id).fadeIn('slow',function(){})

});
//ajax add picture to item if item exists
$(document.body).on('click','.savepicturebutton',function(){
	itemid = $(this).attr("name")
	companyid =  $('#companydata'+itemid).val();
	name = $("#name"+itemid).val();
	rating = $('#itemrating'+itemid).val()
	review= $('#review'+itemid).val()
	tips = $('#tips'+itemid).val()
	$.ajax({
		type:"POST",
		url:"/items/savepicture",
		datatype: "json",
		data:{"itemid":itemid,
			"companyid":companyid,
			'tips':tips,
			'rating':rating,
			'review':review,
			"name" : name},
			
		success:function(data){
			$("#itemdiv"+itemid).fadeOut('slow',function(){});
			$("#itemdiv"+itemid).html(data);
			$("#itemdiv"+itemid).fadeIn('slow',function(){});

			return false;
}})

})

//var to see if ajax already loaded
var loaded = false;
//var to see if food filter already loaded
var foodloaded = false;
//ajax unfork an item that is already forked
$(document.body).on('click','.unforkbutton',function(){
	id = $(this).attr("itemname");
	$.ajax({
		type:"POST",
		url:"/items/unforkitem",
		datatype: "json",
		data:{
			"id":id,
			},

		success:function(data){
			//data is the number of recommended items
			$('#fork'+id).attr('class', 'forkbutton');
			$('#forkedcountheader'+id).html(data)

			return false;
}})
	});
	
//fork an item
$(document.body).on('click','.forkbutton',function(){
	id = $(this).attr("itemname");
	$.ajax({
		type:"POST",
		url:"/items/forkitem",
		datatype: "json",
		data:{
			"id":id,
			},

		success:function(data){
			//data is the number of recommended items
			$('#fork'+id).attr('class', 'unforkbutton');
			$('#forkedcountheader'+id).html(data)
			return false;
}})
	


});
//apply filters(home.html)
$('.filterbuttonhome').click(function(){
	//check to see if city is chosen
	if (document.getElementById("iscityselect").value == 'false'){
			document.getElementById('maincitysearchbar').focus()
			$("#mainsearchcityerror").show();
			return false;
	}
	sortby = $('#sortby').val();
	neighbourhoods =[];
	foods = [];
	dietaryrestrictions=[]
	search=""
	searchcategory = ""
	findtype = this.name
	if ($('.foodfilter').length >1){
		$('.foodfilter').each(function(index,value){
		if (value.checked == 1){
			foods.push(value.id);
		}
	});
	}else{
		foods.push("None")
	}
	if ($('.neighbourhoodfilter').length >1){
  		$('.neighbourhoodfilter').each(function(index,value){
		if (value.checked == 1){
			neighbourhoods.push(value.id);
		}
	})
	}else{
		neighbourhoods.push("None")
	}
	$('.dietaryfilter').each(function(index,value){
		if (value.checked == 1){
			dietaryrestrictions.push(value.id);
		}

	});	
	//if user is also using searchbar then add it to ajax
	jsonfoods = JSON.stringify(foods)
	jsonneighbourhoods = JSON.stringify(neighbourhoods)
	jsondietaryrestrictions = JSON.stringify(dietaryrestrictions)
	city = document.getElementById("maincitysearchbar").value
		$.ajax({
		type:"POST",
		url:"/applyfilters",
		datatype: "json",
		data:{"jsonfoods":jsonfoods,
			"jsonneighbourhoods":jsonneighbourhoods,
			"city" :city,
			"sortby":sortby,
			'jsondietaryrestrictions':jsondietaryrestrictions,
			'search':search,
			'searchcategory':searchcategory,
			'findtype':findtype
			},

		success:function(data){
			window.location.replace(data)
}})
});

//ajax load of food choices
$('.selectdietaryrestrictionsbutton').click(function(){
	$(".neighbourhoodframe").hide()
	$(".foodframe").hide()
	$(".dietaryrestrictionsframe").show()
})

//ajax load of food choices
$('.selectfoodbutton').click(function(){
	$(".neighbourhoodframe").hide()
	$(".dietaryrestrictionsframe").hide()
	if(foodloaded == false){
	$.ajax({
		type:"GET",
		url:"/getfoods",
		datatype: "json",
		success:function(data){
			$(".foodframe").html(data);
			$(".foodframe").show()
			foodloaded= true
			return false;
}})

		

	}
	//if food items are loaded
	else{
		$(".foodframe").show()
		return false;
	}
})

//close neighbourhood div in home.html
$(document.body).on('click','.applyfilters, .closefilterframe',function(){
	$(".neighbourhoodframe").hide()
	$(".foodframe").hide()
	$(".dietaryrestrictionsframe").hide()
});
//ajax load neighbourhoodchoices
$(document.body).on('click','.selectneighbourhoodbutton',function(){
	$('.foodframe').hide()
	$(".dietaryrestrictionsframe").hide()	
	if (loaded == false){
		city = document.getElementById("maincitysearchbar").value;
		$.ajax({
		type:"POST",
		url:"/getneighbourhoods",
		datatype: "json",
		data:{"city":city,
			},

		success:function(data){
			$(".neighbourhoodframe").html(data);
			$(".neighbourhoodframe").show()
			loaded= true
			return false;
}})
		}
		else{
			$(".neighbourhoodframe").show();
		}

})
//ajax delete item (items/new)
$(document.body).on('click','.deletepicturebutton',function(){
	itemid =  $(this).attr("name");
		$.ajax({
		type:"POST",
		url:"/items/deletephoto",
		datatype: "json",
		data:{"itemid":itemid,
			},

		success:function(data){
			$("#itemdiv"+itemid).fadeOut('slow',function(){});
			$("#itemdiv"+itemid).html("")
			return false;
}})


})
//ajax save item(items/new)
$(document.body).on('click', '.saveitembutton',function(){
	itemid = $(this).attr("name");
	//hide error message
	$('#error'+itemid).hide();
	name = $("#name"+itemid).val();
	price = $("#price"+itemid).val();
	category = $('#category'+itemid).val();
	description = $('#description'+itemid).val();
	glutenfree= $('#glutenfree'+itemid).is(":checked");
	fatfree= $('#fatfree'+itemid).is(":checked");
	vegan= $('#vegan'+itemid).is(":checked");
	peanutfree= $('#peanutfree'+itemid).is(":checked");
	rating = $('#itemrating'+itemid).val()
	review= $('#review'+itemid).val()
	tips = $('#tips'+itemid).val()
	vegetarian= $('#vegetarian'+itemid).is(":checked");
	lactosefree= $('#lactosefree'+itemid).is(":checked");
	companynid =  $('#companydata'+itemid).val();
	//change all tags into a string seperated by a comma
	tags=[]
	$('.tagitlist'+itemid+' '+ '.tagit-label').each(function(index,value){
		tags.push(value.innerHTML);
	});
	//check if user has entered name
	if (name == ""){
		 $("#name"+itemid).focus();
		$('#error'+itemid).html("You must enter a name");
		$('#error'+itemid).show();
		return false;

	}
	if (category == ""){
		$('#error'+itemid).html("You must enter a category");
		$('#error'+itemid).show();
		$('html, body').animate({ scrollTop: $('#itemname'+itemid).offset().top }, 'slow');
		return false;
	}
	price = price.replace("$","");	
	var jsonString = JSON.stringify(tags)
	$.ajax({
		type:"POST",
		url:"/items/saveitem",
		datatype: "json",
		data:{"itemid":itemid,
		      "name":name,
			"price":price,
			"category":category,
			"description":description,
			"vegan":vegan,
			"peanutfree":peanutfree,
			"fatfree":fatfree,
		       'tags':jsonString,
			"glutenfree":glutenfree,
			'companyid':companyid,
			'vegetarian':vegetarian,
			'lactosefree':lactosefree,
			'tips':tips,
			'rating':rating,
			'review':review,
			},

		success:function(data){
			//if item exists in database
			if(data == "itemexists"){
				$("#error"+itemid).html("This company already has an item with this name")
				return false	

			}
			else{

			$("#itemdiv"+itemid).fadeOut('slow',function(){});
			$("#itemdiv"+itemid).html(data);
			$("#itemdiv"+itemid).fadeIn('slow',function(){});

			return false;
			}
}})
});

//ajax flag item if inapproprate(items/single)
$(document.body).on('click', '.flagitemtext',function(){
	id = $(this).attr("name");

	$.ajax({
		type:"POST",
		url:"/items/flagitem",
		datatype: "json",
		data:{"id":id,
			   "name":name},
		success:function(){
			$('#reportitem'+id).html("item flagged")
			$('#reportitem'+id).className="reviewflagged"
			return false;
}})
});

//ajax load of item frame when user clicks on a reviewd item in company profile (user/company.html)
$(document.body).on('click', '.flagreviewtext',function(){
	id = $(this).attr("name");

	$.ajax({
		type:"POST",
		url:"/items/flagreview",
		datatype: "json",
		data:{"id":id,
			   "name":name},
		success:function(){
			$('#reportspan'+id).html("reported")
			$('#reportspan'+id).className="reviewflagged"

			return false;
}})
});

//ajax load of item frame when user clicks on a reviewd item in company profile (user/company.html)
$(document.body).on('click', '.itemreviewedframe',function(){
	id = this.id;
	$.ajax({
		type:"POST",
		url:"itemframe",
		datatype: "json",
		data:{"id":id},
		success:function(data){
		//	$('.simpleoverlay').show();
			$('.itemframe').html("");
			$('.itemframe').html(data);
			$('.itemframe').show();

			return false;
}})
});

//change tabs in item page(Item/single.html)
$('.itemmenuitems').click(function(){

	//if user clicks forked tab
	if (this.id == "itemreviewtab"){
		$('.itempicturecontainer').hide()
		$('.itemtipscontainer').hide();
		$('.itemreviewcontainer').show();
		$(this).addClass("selected");
		$('#morepicturestab').removeClass('selected');
		$('#itemtipstab').removeClass('selected');
		$('.itemreviewcontainer').focus();

	}
	else if (this.id == "morepicturestab"){
		$('.itempicturecontainer').show()
		$('.itemreviewcontainer').hide();
		$('.itemtipscontainer').hide();
		$(this).addClass("selected");
		$('#itemreviewtab').removeClass('selected');
		$('#itemtipstab').removeClass('selected');
		pictureid = $(this).attr("name")
		loadMorePictures(pictureid);
		$('.itempicturecontainer').focus();
	}
	else if (this.id == "itemtipstab"){
		$('.itempicturecontainer').hide()
		$('.itemreviewcontainer').hide();
		$('.itemtipscontainer').show()
		$(this).addClass("selected");
		$('#itemreviewtab').removeClass('selected');
		$('#morepicturestab').removeClass('selected');
		itemid = $(this).attr("name")
		type = $(this).attr("type")	
		loadItemTips(itemid,type);
		$('.itemtipscontainer').focus();
	}

})

//change tabs in user page(users/user.html)
$('.usermenuitems').click(function(){
	//if user clicks forked tab
	if (this.id == "itemsforkedtab"){
		$('.userreviews').hide();
		$('#itemspostedcontainer').hide();
		$('.itemtipscontainer').hide();
		$('#picturespostedcontainer').hide();
		$('#itemsforkedcontainer').show();
		$(this).addClass("selected");
		$('#reviewtab').removeClass('selected');
		$('#tipspostedtab').removeClass('selected');
		$('#picturespostedtab').removeClass('selected');
		$('#itemspostedtab').removeClass('selected');
		$('#itemsforkedcontainer').focus();

	}
	else if (this.id == "itemspostedtab"){
		$('.userreviews').hide();
		$('#itemspostedcontainer').show();
		$('#itemsforkedcontainer').hide();
		$('.itemtipscontainer').hide();
		$('#tipspostedcontainer').hide();
		$('#picturespostedcontainer').hide();	
		$(this).addClass("selected");
		$('#reviewspostedtab').removeClass('selected');
		$('#itemsforkedtab').removeClass('selected');
		$('#tipspostedtab').removeClass('selected');
		$('#picturespostedtab').removeClass('selected');
		$('#itemspostedcontainer').focus();
		userid = $(this).attr("name") 
		loadItemsPosted(userid)

	}
	//if user wants to look at pictures posted
	else if (this.id == "picturespostedtab"){
		$('.userreviews').hide();
		$('#itemspostedcontainer').hide();
		$('#itemsforkedcontainer').hide();
		$('.itemtipscontainer').hide();
		$('#tipspostedcontainer').hide();
		$('#picturespostedcontainer').show();
		$(this).addClass("selected");
		$('#reviewspostedtab').removeClass('selected');
		$('#itemspostedtab').removeClass('selected');
		$('#tipspostedtab').removeClass('selected');
		$('#itemsforkedtab').removeClass('selected');
		$('#picturespostedcontainer').focus();
		userid = $(this).attr("name") 
		loadPicturesPosted(userid)

	
	}
	//if user wants to look at reviews posted
	else if (this.id == "reviewspostedtab"){
		$('.userreviews').show();
		$('#itemspostedcontainer').hide();
		$('.itemtipscontainer').hide();
		$('#itemsforkedcontainer').hide();
		$('#picturespostedcontainer').hide();
		$('#tipspostedcontainer').hide();
		$(this).addClass("selected");
		$('#itemspostedtab').removeClass('selected');
		$('#picturespostedtab').removeClass('selected');
		$('#itemsforkedtab').removeClass('selected');
		$('#tipspostedtab').removeClass('selected');
		$('.userreview').focus();
		userid = $(this).attr("name")
		type = $(this).attr("type")	
		loadReviews(userid,type)
		
	}

	//if user clicks item tips
	else if (this.id == "tipspostedtab"){
		$('.userreviews').hide();
		$('#itemspostedcontainer').hide();
		$('#itemsforkedcontainer').hide();
		$('#picturespostedcontainer').hide();
		$('.itemtipscontainer').show();
		$(this).addClass("selected");
		$('#itemspostedtab').removeClass('selected');
		$('#picturespostedtab').removeClass('selected');
		$('#itemsforkedtab').removeClass('selected');
		$('#reviewspostedtab').removeClass('selected');
		$('.itemtipscontainer').focus();
		userid = $(this).attr("name")
		type = $(this).attr("type")	
		loadItemTips(userid,type)
		
	}



});

$('.applydietaryfilter').click(function(){
$('.dietaryrestrictionsframe').hide()
});

//change tabs in company and user page (users/company.html) 
$(".menuitems").click(function(){
	//if user clicks menu items
	if (this.id == "menutab"){
		$('.companyreviews').hide()
		$('#itemsmenucontainer').show();
		$('.itemtipscontainer').hide()	
		$(this).addClass("selected");
		$('#reviewtab').removeClass("selected");
		$('#itemtipstab').removeClass("selected");
		$('#itemsmenucontainer').focus();
	}
	else if (this.id =="reviewtab"){
		$('#itemsmenucontainer').hide();
		$('.companyreviews').show()
		$('.itemtipscontainer').hide()	
		$(this).addClass("selected") ;
		$('#itemtipstab').removeClass("selected");
		$('#menutab').removeClass("selected");
		//get company id
		companyid = $(this).attr('name');
		type = $(this).attr('type');
		loadReviews(companyid,type)
		
	}
	else if (this.id =="itemtipstab"){
		$('#itemsmenucontainer').hide();
		$('.companyreviews').hide()
		$('.itemtipscontainer').show()	
		$(this).addClass("selected") ;
		$('#menutab').removeClass("selected");
		$('#reviewtab').removeClass("selected");
		//get company id
		id = $(this).attr('name');
		type = $(this).attr('type');
		loadItemTips(id,type)
		
	}

})
//variable to see if itemtips have been loaded
var itemtipsloaded = false;
// load more pictures posted for the given item
function loadItemTips(id,type){
	
	// if items have been loaded already	
	if (itemtipsloaded){
	       	return false;
	}	
	$.ajax({
		type:"POST",
		url:"/items/loaditemtips",
		datatype: "json",
		data:{"id":id,
			'type':type,
			  },
		success:function(data){
			itemtipsloaded = true;
			$('.itemtipscontainer').append(data);
			return false;
			
		}
})}

//variable to see if more pictures have been loader
var morepicturesloaded = false;
// load more pictures posted for the given item
function loadMorePictures(itemid){
	id = itemid
	// if items have been loaded already	
	if (morepicturesloaded){
	       	return false;
	}	
	$.ajax({
		type:"POST",
		url:"/items/loadmorepictures",
		datatype: "json",
		data:{"id":id,
			  },
		success:function(data){
			
			morepicturesloaded = true;
			$('.itempicturecontainer').append(data);
			if(data.indexOf("item0")==-1){
				$('.itempicturecontainer').append("<h3 style ='margin-top:20px;text-align:center'>No more pictures found   </h3>") 
			}
		
			else{
				$('.itempicturecontainer').masonry({
  				isAnimated: true,
				  itemSelector: '.item0'
});
			}	
			return false;
			
		}
})}
var loadpicturesloaded = false;
//ajax load of items that have been posted by the user
function loadPicturesPosted(userid){
	id = userid
	// if items have been loaded already	
	if (loadpicturesloaded){
	       	return false;
	}	
	$.ajax({
		type:"POST",
		url:"/users/loadpicturesposted",
		datatype: "json",
		data:{"id":id,
			  },
		success:function(data){
			$('#picturespostedcontainer').append(data);
			if(data.indexOf("item0")==-1){
				$('#picturespostedcontainer').append("<h1 style ='margin-top:20px;text-align:center'>No pictures added :( </h1>") 
			}
		
			else{
				$('#picturespostedcontainer').masonry({
  				isAnimated: true,
				  itemSelector: '.item0'
});
			}	
			loadpicturesloaded = true;
			return false;
			
		}
})}

//var to make sure that items posted doesnt load twice
var loaditemsloaded = false;
//ajax load of items that have been posted by the user
function loadItemsPosted(userid){
	id = userid
	// if items have been loaded already	
	if (loaditemsloaded) return	
	$.ajax({
		type:"POST",
		url:"/users/loaditemsposted",
		datatype: "json",
		data:{"id":id,
			  },
		success:function(data){
			$('#itemspostedcontainer').append(data);
			if(data.indexOf("item0")==-1){
				$('#itemspostedcontainer').append("<h1 style ='margin-top:20px;text-align:center'>No items added :( </h1>") 
			}
				$('#itemspostedcontainer').masonry({
				 itemSelector: '.item0'
});
			loaditemsloaded = true;
			return false;
			
		}
})}
//make sure ajax load doesnt load again


		



//ajax load reviews for company profile page, (users/company)
function loadReviews(companyid,type){
	id = companyid;
	type = type
	if (loaded) return
	
	$.ajax({
		type:"POST",
		url:"/users/loadreviews",
		datatype: "json",
		data:{"id":id,
			  'type':type},
		success:function(data){
			$('.'+type+'reviews').append(data);
			$('.'+type+'reviews').show();
			return false;
}})
//make sure ajax load doesnt load again
	loaded= true;
}



// delete review so user can write new one
$(".deletereviewbutton").click(function(){
document.getElementById('isdeletereview').value = "true"
document.forms['reviewform'].submit();
});

//login page
$('loginbutton').click(function(){
document.forms['loginform'].submit();
});

// on home page, if hover over an image, show facebook/twitter
$(document).on('mouseenter', '.imagecontainer',function(){
	$('[id=socialmediabackground'+this.id+']').fadeIn('slow',function(){})
	$('[id=socialmediacontainer'+this.id+']').fadeIn('slow',function(){})
}).on("mouseleave",".imagecontainer",function(){
	$('[id=socialmediabackground'+this.id+']').fadeOut('slow',function(){})
	$('[id=socialmediacontainer'+this.id+']').fadeOut('slow',function(){})
});

//ajax load of item frame when user clicks thumbnailimage (home.html)
$(document.body).on('click', '.framemainpageload',function(){
	id = this.id;
	$.ajax({
		type:"POST",
		url:"itemframe",
		datatype: "json",
		data:{"id":id},
		success:function(data){
		//	$('.simpleoverlay').show();
			$('.itemframe').html("");
			$('.itemframe').html(data);
			$('.itemframe').show();

			return false;
}})
});

//close button on frame (home.html)

$(document.body).on('click', '.closeframe',function(){
$('.itemframe').hide();
//$('.simpleoverlay').hide();
	
});


// On single item page, when not chosen pictured is clicked, that picture becomes the one displayd(items/single.html)
$(document.body).on('click', '.itempicturenotchosen',function(){
 	id = this.id
 	$(".itemchosen").attr("class", "itempicturenotchosen") ;
 	document.getElementById(id).className = "itemchosen";
 	document.getElementById('itemdisplayed').src = this.src
 });

// create account button click( signup/user.html and signup/company.html)
$(".createaccountbutton").click(function(e){
	password = document.getElementById("password").value;
	confirmpassword = document.getElementById("confirmpassword").value;
	if(password != confirmpassword){
		document.getElementById("passwordsnotmatching").style.display = "inline";
		document.getElementById("passwordsnotmatching").focus();
		e.preventDefault();

	}else{
	doument.forms["createaccountform"].submit();
	}
});

//make button click in company_customize-first submit form
$('.customizebutton').click(function(){
		//get all items in tags to array
	arraytags=[]
	$('.tagit-label').each(function(index,value){
		arraytags.push(value.innerHTML);
	});
	JSON.stringify(arraytags); 
	document.getElementById('itemtags').value = arraytags;
	document.forms['customizeform'].submit()
});
//change profile picture in update stage
$('#profilepicture').change(function(){
	document.getElementById("isaddpicture").value = "true";
	document.forms['editaccountform'].submit()
});
// logo upload for company step 2
$('#logoupload').change(function(e){
	document.getElementById("isuploadpicture").value = "True";
	document.forms['customizeform'].submit();
});

//function that deals with adding tags to items
$('.additemnextstep').click(function(){
	//get all items in tags to array
	arraytags=[]
	$('.tagit-label').each(function(index,value){
		arraytags.push(value.innerHTML);
	});
	JSON.stringify(arraytags); 
	document.getElementById('itemtags').value = arraytags;
	document.forms['additemform'].submit()
});

// submit form when uploading pictures to item
$('#uploadpictures').change(function(e){
	document.getElementById("isuploadpicture").value = "True";
	document.forms['addpictureform'].submit();
});

// if add purchase checkbox is checked, show purchasing options(addpicturenew.html)
$('#allowonlinesales').change(function(e){
	ischecked = document.getElementById("allowonlinesales");
	if (ischecked.checked){
		$('#addshippingdiv').fadeIn('slow', function() {
      });

	}
	else{
		$('#addshippingdiv').fadeOut('slow', function() {
      });
	}
});


// if allow price per dozen is checked, dont disable input box(addpicturenew.html)
$('#priceperdozencheckbox').change(function(e){
	ischecked = document.getElementById("priceperdozencheckbox");
	if (ischecked.checked){
		$('#priceperdozen').prop( "disabled", false );

	}
	else{
		document.getElementById("priceperdozen").value = "";
		$('#priceperdozen').prop( "disabled", true );
	}
});

// if company wants to allow both shipping and pickup (addpicturenew.html)
$('#deliverymethod').change(function(e){
	deliverymethod = document.getElementById("deliverymethod").value;
	if (deliverymethod == "both" || deliverymethod == "shipping"){
		$('#shippingpricecontainer').fadeIn('slow', function() {

      });

	}
	else{
		$('#shippingpricecontainer').fadeOut('slow', function() {
			document.getElementById("shippingprice").value = ""
      });
	}
});
// if user wants to add another treat (addpicturenew.html)
$(".addanotheritem").click(function(){
	document.getElementById("nextaction").value = "addanother";
	document.forms['addpictureform'].submit();

});
// if user wants to add only this treat, redirect to single treat page(addpicturenew.html)
$(".createitem").click(function(){
	document.forms['addpictureform'].submit();
})


// ajax function that sets a picture as the main picture (companycustomizesecond.html)
$(document.body).on('click', '.setasmainbutton',function(e){
	e.preventDefault();
	id = this.name;
	$.ajax({
		type:"POST",
		url:"/items/setasmain",
		datatype: "json",
		data:{"id":id},
		success:function(data){
			previousmain = document.getElementById("mainimage");
			if (previousmain != null){
			previousmain.innerHTML = "Set As Main";
			previousmain.className = "btn btn-success setasmainbutton";
			previousmain.id ="setasmain"+data;
		}
			document.getElementById("setasmain"+id).innerHTML = "Main Picture";
			document.getElementById("setasmain"+id).className = "btn btn-primary mainbutton";
			document.getElementById("setasmain"+id).id = "mainimage";
			return false;
}})
});
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
	$('.loading').show().css('display','block');
    },
   complete:function(){
	   $('.loading').hide();
   }	   
});

