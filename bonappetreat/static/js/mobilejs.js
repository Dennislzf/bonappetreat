$(document.body).on('click','.viewfullsite',function(){
	$.ajax({
		type:"GET",
		url:"/mobile/viewfullsite",
		success:function(data){
		}
	})
	
});



$(document.body).live('keyup','.mobilesearch',function(){
	if(event.keyCode == 13){
		query = document.getElementById("mobilesearch").value
		
		//check which button is selected
		radios = document.getElementsByName("radio-choice")
		for(i=0; i <radios.length; i++){
			if(radios[i].checked){
				radiotype = radios[i]
				break;
			}
		}
		radiovalue = radiotype.id;
		querystring = "?q="+query+"&category="+radiovalue;
		window.location.href = "/mobile/search"+querystring


	}
});

$(document.body).on('change','#selectmenu',function(){
	value = this.value
	if (value == "Featured"){
		$('.searchtoprateditems').hide()
		$('.searchhiddengemsitems').hide()
		$('.searchcheapeatsitems').hide()
		$('.searcheverythingitems').hide()
		//featured items already loaded so dont need ajax call
		$('.searchfeatureditems').show()
	}
	else if (value == "TopRated"){
		$('.searchfeatureditems').hide()
		$('.searchhiddengemsitems').hide()
		$('.searchcheapeatsitems').hide()
		$('.searcheverythingitems').hide()
		getSearchItems(value)
	}
	else if (value == "HiddenGems"){
		$('.searchfeatureditems').hide()
		$('.searchtoprateditems').hide()
		$('.searchcheapeatsitems').hide()
		$('.searcheverythingitems').hide()
		getSearchItems(value)
	}
	else if (value == "CheapEats"){
		$('.searchfeatureditems').hide()
		$('.searchhiddengemsitems').hide()
		$('.searchtoprateditems').hide()
		$('.searcheverythingitems').hide()
		getSearchItems(value)
	}
	else if (value == "Everything"){
		$('.searchfeatureditems').hide()
		$('.searchhiddengemsitems').hide()
		$('.searchcheapeatsitems').hide()
		$('.searchtoprateditems').hide()
		getSearchItems(value)
	}
	return false;



});

var loadtoprated = false;
var loadhiddengems = false;
var loadcheapeats = false;
var loadeverything=false;
//get search items does an ajax call and loads other search categories or shows container if search had been made
function getSearchItems(value){
	$.ajax({
		type:"POST",
		url:"/mobile/loadsearchitems",
		datatype: "json",
		data:{"value":value,
			},

		success:function(data){
			if(value =="Everything"){
				if(loadeverything == false){
					if(data.indexOf("itemmobile") == -1){
						$('.searcheverythingcontainer').append("<h3 class='noitemsfound'>We couldn't find any items </h3>")
						$('#everything').hide()
					}
					else{
						$('.searcheverythingcontainer').append(data)
					}
					loadeverything=true
					}
				$('.searcheverythingitems').show()

			}
			else if (value == "CheapEats"){
				if (loadcheapeats == false){
					if(data.indexOf("itemmobile") == -1){
						$('.searchcheapeatscontainer').append("<h3 class='noitemsfound'>We couldn't find any items </h3>")
						$('#cheapeats').hide()
					}
					else{
						$('.searchcheapeatscontainer').append(data)
					}
					loadcheapeats = true
				}
				$('.searchcheapeatsitems').show()
			}
			else if (value == "HiddenGems"){
				if(loadhiddengems == false){
					if(data.indexOf("itemmobile") == -1){
						$('.searchhiddengemscontainer').append("<h3 class='noitemsfound'>We couldn't find any items </h3>")
						$('#hiddengems').hide()
					}
					else{
						$('.searchhiddengemscontainer').append(data)
					}
					loadhiddengems = true
				}
				$('.searchhiddengemsitems').show()
			}
			else if (value == "TopRated"){
				if(loadtoprated == false){
					if(data.indexOf("itemmobile") == -1){
						$('.searchtopratedcontainer').append("<h3 class='noitemsfound'>We couldn't find any items </h3>")
						$('#toprated').hide()
					}
					else{
						$('.searchtopratedcontainer').append(data)
					}
					loadtoprated = true
				}
				$('.searchtoprateditems').show()
			}
	
}})


}
//functiong that deals with the loadmore button when user is searching
$(document.body).on('click','.searchloadmore',function(){
	loadmoretype = this.id
	if(loadmoretype == "featured"){
		document.getElementById('featureditemspage').value = parseInt(document.getElementById('featureditemspage').value) + 1 
		pageno = document.getElementById('featureditemspage').value 
	}
	else if (loadmoretype == "toprated"){
		document.getElementById('topratedpage').value = parseInt(document.getElementById('topratedpage').value) + 1 
		pageno = document.getElementById('topratedpage').value
	}
	else if (loadmoretype == "hiddengems"){
		document.getElementById('hiddengemspage').value = parseInt(document.getElementById('hiddengemspage').value) + 1 
		pageno = document.getElementById('hiddengemspage').value
	}
	else if(loadmoretype == "cheapeats"){
		document.getElementById('cheapeatspage').value = parseInt(document.getElementById('cheapeatspage').value) + 1 
		pageno = document.getElementById('cheapeatspage').value
	}
	else if (loadmoretype == "everything"){
		document.getElementById('everythingpage').value = parseInt(document.getElementById('everythingpage').value) + 1 
		pageno = document.getElementById('everythingpage').value
	}


	$.ajax({
		type:"POST",
		url:"/mobile/loadmoresearch",
		datatype: "json",
		data:{"loadmoretype":loadmoretype,
			"pageno":pageno
			},

		success:function(data){
			itemdata= data.data
			isnextpage =data.isnextpage
			if(loadmoretype == "featured"){
				if(itemdata.indexOf("itemmobile") == -1){
					$('.searchfeaturedcontainer').append("<h3 class='noitemsfound'>No More Items Found</h3>")
					$('#featured').hide()	
					}
				else{
					
					$('.searchfeaturedcontainer').append(itemdata)
				}		
				if(isnextpage == null){
					$('#featured').hide()
				}
					
			}
			else if (loadmoretype == "toprated"){
				if(itemdata.indexOf("itemmobile") == -1){
						$('.searchtopratedcontainer').append("<h3 class='noitemsfound'>No More Items Found</h3>")
					$('#toprated').hide()	
					}
				else{
					
					$('.searchtopratedcontainer').append(itemdata)
				}		
				if(isnextpage == null){
					$('#toprated').hide()
				}
				
			}
			else if (loadmoretype == "hiddengems"){
				if(itemdata.indexOf("itemmobile") == -1){
					$('.searchhiddengemscontainer').append("<h3 class='noitemsfound'>No More Items Found</h3>")
					$('#hiddengems').hide()	
					}
				else{
					
					$('.searchhiddengemscontainer').append(itemdata)
				}		
				if(isnextpage == null){
					$('#hiddengems').hide()
				}
				

			}
			else if (loadmoretype == "cheapeats"){
						if(itemdata.indexOf("itemmobile") == -1){
					$('.searchcheapeatscontainer').append("<h3 class='noitemsfound'>No More Items Found</h3>")
					$('#cheapeats').hide()	
					}
				else{
					
					$('.searchcheapeatscontainer').append(itemdata)
				}		
				if(isnextpage == null){
					$('#cheapeats').hide()
				}
				
			}
			else if (loadmoretype == "everything"){
				if(itemdata.indexOf("itemmobile") == -1){
					$('.searcheverythingcontainer').append("<h3 class='noitemsfound'>No More Items Found</h3>")
					$('#everything').hide()	
					}
				else{
					
					$('.searcheverythingcontainer').append(itemdata)
				}		
				if(isnextpage == null){
					$('#everything').hide()
				}
				
			}




			
		}
	
	})
});

