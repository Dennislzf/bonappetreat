from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.utils import simplejson
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
import urllib
from django.contrib.sites.models import Site
import string
import ast
import random
import logging
import operator
from django.http import Http404 ,HttpResponse 
from UserInfo.models import  UserInfo, CompanyInfo
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.core.validators import validate_email
from Items.models import *
import Image
import json
from random import randint, shuffle
from django.template.loader import render_to_string
from Cities.models import Neighbourhood
from Foods.models import Food
from django.db.models import Q
import math
from django.core.cache import cache
from Cities.models import *
from UserInfo.views import get_client_ip
import pygeoip



def home(request):
	if request.user.is_authenticated():
		try:
			userinfo = UserInfo.objects.get(user = request.user)
		except:
			userinfo = None
	else:
		#if user is not logged in, then send to about page
		userinfo = None
		try:
			hasvisited= request.session["newvisitabout"] 
		except:
			return HttpResponseRedirect('/about')
		
	#clear cache of previous city searches
	cache.clear()
	#clear some session variables
	delete_session_variables(request)
	#get user city		
	get_user_city(request)
	city = City.objects.get(name = request.session['cityname'], provincestate =request.session['provincestate'])
	#get featured items
	featureditems= get_featured_items(city,request,None)
	isfeatureditems= check_page_exists(featureditems)
		
	#get top rated items
	toprateditems = get_top_rated_items(city,request,None)
	istoprated = check_page_exists(toprateditems)
	#get cheap eats 	
	cheapeatitems = get_cheap_eats(city,request,None)
	ischeapeats =  check_page_exists(cheapeatitems)

	#get hidden gems
	hiddengemitems = get_hidden_gems(city,request,None)
	ishiddengem = check_page_exists(hiddengemitems)


	#get everything
	everythingitems = get_everything(city,request,None)
	iseverything = check_page_exists(everythingitems)		

	form_args={ 'featureditems':featureditems,'userinfo':userinfo,'isfeatureditems':isfeatureditems,'toprateditems':toprateditems,'istoprated':istoprated,'cheapeatitems':cheapeatitems,'ischeapeats':ischeapeats,'hiddengemitems':hiddengemitems,'ishiddengem':ishiddengem,'everythingitems':everythingitems,'iseverything':iseverything}
	return render_to_response("home.html",form_args, context_instance=RequestContext(request))


#view that loads about page for first time users
def about(request):
	request.session["newvisitabout"]= True
	return render_to_response("about.html", context_instance=RequestContext(request))
#view that loads company page
def company(request):
	return render_to_response("company.html", context_instance=RequestContext(request))
	
@csrf_exempt
#function that loads the itemframe on click ajax
def itemframe(request):
	if request.is_ajax() and request.method == "POST":
		MEDIA_URL = settings.MEDIA_URL
		STATIC_URL = settings.STATIC_URL
		itemid = request.POST.get("id")

		try:
			item = Item.objects.get(id = itemid)
			company = item.companyinfo
		except:
			raise Http404

		itemimages=  ItemImage.objects.filter(item = item)
		form_args= {'item':item,'itemimages':itemimages,'company':company, 'MEDIA_URL':MEDIA_URL,'STATIC_URL':STATIC_URL}
		return HttpResponse( render_to_string('itemframe.html', form_args))
	else:
		raise Http404
@csrf_exempt
#ajax call to load more data		
def load_more(request,devicetype,findtype,tag):
	MEDIA_URL = settings.MEDIA_URL
	STATIC_URL = settings.STATIC_URL
	pageno = tag
			

	if findtype =="featured":
		items = request.session['featureditems']
	elif findtype == "toprated":
		items = request.session['toprateditems']
	elif findtype == "hiddengems":
		items = request.session['hiddengemitems']
	elif findtype == "cheapeats":
		items = request.session['cheapeatitems']
	elif findtype == "everything":
		items = request.session['everythingitems']
	#get companies in load	
	elif findtype == "company":
		items = request.session['companies']
	#try to get next page of items if it exists
	try:
		paginator = Paginator(items , 25)
		items = paginator.page(pageno)
	except:
		raise Http404
	form_args= {'items':items, 'MEDIA_URL':MEDIA_URL,'STATIC_URL':STATIC_URL}
	if devicetype == "web":
		return HttpResponse( render_to_string('loadmore.html', form_args))
	elif devicetype == "mobile":
		if findtype == "company":
			return HttpResponse( render_to_string('Mobile/loadmorecompany.html', form_args))

		return HttpResponse( render_to_string('Mobile/loadmore.html', form_args))
	else:
		raise Http404		
		
@csrf_exempt
#get neighbourhoods based on city.
def get_neighbourhoods(request):
	if request.is_ajax() and request.method == "POST":
		#get user location information
		get_user_city(request)
		print request.session['cityname']
		city = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])	
		neighbourhoods = Neighbourhood.objects.filter(city = city)
		#split neighbourhoods into 2 columns
		countdivide = neighbourhoods.count() 
		countdivide = math.ceil(countdivide/float(2))
		countdivide = int(countdivide)
		#get values in cache that are chosen as filter
		try:
			neighbourhoodfilter =request.session['neighbourhoodfilter']
		except:
			neighbourhoodfilter=None

		form_args ={'neighbourhoods':neighbourhoods,'countdivide':countdivide,'neighbourhoodfilter':neighbourhoodfilter}
		return HttpResponse(render_to_string('neighbourhoodfilter.html',form_args))

	else:
		raise Http404
@csrf_exempt
#ajax load of all the foods
def get_foods(request):
	if request.is_ajax():
		foods = Food.objects.filter(category ="Food").order_by("foodtype")
		desserts = Food.objects.filter(category = "Dessert").order_by("foodtype")
		bakerygoods = Food.objects.filter(category="Bakery Goods").order_by("foodtype")
		drinks = Food.objects.filter(category="Drinks").order_by("foodtype")
		try:
			foodfilter = request.session['foodfilter']
		except:
			foodfilter = None
		form_args= {'foods':foods,'desserts':desserts,'bakerygoods':bakerygoods,'foodfilter':foodfilter,'drinks':drinks}
		return HttpResponse(render_to_string('foodfilter.html',form_args))


	else:	
		raise Http404;
@csrf_exempt
#apply filters to items
def apply_filters(request):
	sortby = request.POST.get("sortby")
	foods = request.POST.get("jsonfoods")
	neighbourhoods= request.POST.get("jsonneighbourhoods")
	dietaryrestrictions = request.POST.get("jsondietaryrestrictions")
	findtype = request.POST.get('findtype')
	#checks to see if user has already filter results
	if "None" in neighbourhoods:
		try:
			if request.session['neighbourhoodfilter'] != None:
				neighbourhoods = request.session['neighbourhoodfilter']
				neighbourhoods = ','.join(neighbourhoods)
			else:
				neighbourhoods=[]
		except:
			neighbourhoods=[]
	#check to see if user has food filter		
	if "None" in foods:
		try:
			if request.session['foodfilter'] != None:		
				foods = request.session['foodfilter']
				foods = ','.join(foods)
			else:
				foods=[]
		except:
			foods=[]
	search = request.POST.get('search')
	searchcategory=request.POST.get('searchcategory')
	city  =  request.POST.get('city')
	city = city.split(',')	
	#change foods into array only if string returned isnt "[]" in unicode
	if len(foods) >  2:
		foods = foods.replace("[","")
		foods= foods.replace ("]","")
		foods = foods.replace("\"","")
		foods = foods.split(",")
	else:
		foods = None

	#change neighbourhoods into array of values only if string returned insnt "[]" in unicode
	if len(neighbourhoods) > 2:
		neighbourhoods = neighbourhoods.replace("[","")
		neighbourhoods= neighbourhoods.replace ("]","")
		neighbourhoods = neighbourhoods.replace("\"","")
		neighbourhoods = neighbourhoods.split(",")
	else:
		neighbourhoods = None
	
	#change dietary restrictinos into array of values. If empty, return none
	if len(dietaryrestrictions) > 2:
		dietaryrestrictions = dietaryrestrictions.replace("[","")
		dietaryrestrictions = dietaryrestrictions.replace ("]","")
		dietaryrestrictions = dietaryrestrictions.replace("\"","")
		dietaryrestrictions = dietaryrestrictions.split(",")
	else:
		dietaryrestrictions = None

	if foods is not None:
		foodsquerystring=""
		for food in foods:
			foodsquerystring = foodsquerystring + food+","
		foodsquerystring = urllib.quote_plus(foodsquerystring)	
	#change dietaryrestrinctions to querystring
	if dietaryrestrictions is not None:
		dietaryrestrictionsquerystring=""
		for dietaryrestrictions in dietaryrestrictions:
			dietaryrestrictionsquerystring = dietaryrestrictionsquerystring + dietaryrestrictions+","
		dietaryrestrictionsquerystring = urllib.quote_plus(dietaryrestrictionsquerystring)
	#change neighbourhoods to querystring
	if neighbourhoods is not None:
		neighbourhoodquerystring = ""
		for neighbourhood in neighbourhoods:
			neighbourhoodquerystring= neighbourhoodquerystring + neighbourhood+","
		neighbourhoodquerystring=	urllib.quote_plus(neighbourhoodquerystring)	
	#figure out what querystring will be used	
	if findtype =="featured":
		responseredirect = "/featured"
	elif findtype == "toprated":
		responseredirect = "/toprated"
	elif findtype == "hiddengems":
		responseredirect = "/hiddengems"	
	elif findtype == "cheapeats":
		responseredirect = "/cheapeats"
	elif findtype == "everything":
		responseredirect = "/everything"
	else:
		responseredirect ="/find"
	#add city to querystring	
	responseredirect = responseredirect +"?city="+city[0]+"%2C"+city[1]
	# add sortby to querystring if it exists

	if sortby != "Sort By" and sortby is not None:
		responseredirect = responseredirect + "&sortby="+sortby
	#add neighbourhoods to querystring if exists	
	if neighbourhoods is not None:
		responseredirect = responseredirect +"&neighbourhoods="+ neighbourhoodquerystring
	# add foods to querystring if exists	
	
	if foods is not None:
		responseredirect= responseredirect  + "&foods=" +foodsquerystring
	#add dietaryrestrictions to querystring if exists	
	if dietaryrestrictions is not None:
		responseredirect= responseredirect  + "&dietaryrestrictions=" +dietaryrestrictionsquerystring
	#add search query to the querystring	

	try:
		search = request.session['searchquery']
		searchcategory = request.session['searchcategory']
		#if there is a search value then add to the querystring
		if search != "":
			responseredirect = responseredirect + "&q=" + search
			responseredirect= responseredirect + "&category=" + searchcategory
	except:
		pass
	#get redirect text
	if findtype =="featured":
		redirecttext = responseredirect.replace('/featured',"")
	elif findtype == "toprated":
		redirecttext = responseredirect.replace('/toprated',"")
	elif findtype == "hiddengems":
		redirecttext = responseredirect.replace('/hiddengems',"")
	elif findtype == "cheapeats":
		redirecttext = responseredirect.replace('/cheapeats',"")
	elif findtype == "everything":
		redirecttext = responseredirect.replace('/everything',"")
	else:
		redirecttext = responseredirect.replace('/find',"")
	request.session['filterquerystring'] = redirecttext
	return HttpResponse(responseredirect)	


def find(request):
	return finditem(request,None)
@csrf_exempt
#find
def finditem(request,findtype):
	neighbourhoods = request.GET.get("neighbourhoods")
	sortby = request.GET.get("sortby")
	foods = request.GET.get("foods")
	dietaryrestrictions = request.GET.get('dietaryrestrictions')
	city = request.GET.get('city')
	query = request.GET.get('q')
	category = request.GET.get('category')
	#if city is found in the querystring
	if city !=None:
		city = city.split(',')
		city[0] = city[0].strip
		city[1]=city[1].strip
	#if city is not found in the querystring	
	else:
		city =[]
		#check to see if city has been set
		try:
			request.session['cityname']
		#if city not set in session variable, then set it up	
		except:
			get_user_city(request)
		city.append(  request.session['cityname'])
		city.append( request.session['provincestate'])

	#check to see if city name is city. If it is then return city else its munipal
	try:
		citymunicipal = City.objects.get(name = city[0], provincestate = city[1])
		iscity = True
		cityname = citymunicipal.name
		provincestatename = citymunicipal.provincestate
	except:			
	#if city doesnt exist, then try to get municipal
		try:
			citymunicipal = Municipal.objects.get(name=city[0],city__provincestate=city[1])
			iscity= False
			cityname = citymunicipal.name
			provincestatename = citymunicipal.city.provincestate
		except:
			citymunicipal = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
			iscity= True
			cityname = citymunicipal.name
			provincestatename = citymunicipal.provincestate
	#set all values in the cache
	#this value is to prove if its a city
	request.session['iscity']=iscity
	#cache set city/municipal name
	request.session['cityname']=cityname
	#cache set provincestate name
	request.session['provincestate']= provincestatename

				
	#remove last comma from strings
	if foods is not None:
		foods = foods[:-1]
		#seperate foods into array of foods
		foods = foods.split(',')
	if neighbourhoods is not None:
		neighbourhoods = neighbourhoods[:-1]
		#seperate neighbourhoods into array of neighbourhoods
		neighbourhoods = neighbourhoods.split(',')
		#get neighbourhoods from db 
		neighbourhoodlist  = []
		for neighbourhood in neighbourhoods:
			try:
				neighbourhooddb = Neighbourhood.objects.get(name = neighbourhood, city = citymunicipal)
				neighbourhoodlist.append(neighbourhooddb)
			except:
				pass

	if dietaryrestrictions is not None:
		dietaryrestrictions = dietaryrestrictions[:-1]
		dietaryrestrictions = dietaryrestrictions.split(',')

	#set foods, neighbourhoods, city, sortby and dietary restrictions filter values in cache
	request.session['foodfilter'] = foods
	request.session['neighbourhoodfilter'] = neighbourhoods
	request.session['sortby'] = sortby
	request.session['dietaryrestrictions'] = dietaryrestrictions
	#get initial list of items
	if iscity == True:
		items = Item.objects.filter(city= citymunicipal)
	else:
		items = Item.objects.filter(municipal = citymunicipal)
	#if user is also searching for an item by name else return regular
	if foods is not None and neighbourhoods is not None:
		items = items.filter(reduce(operator.or_,(Q(neighbourhood=y) for y in neighbourhoodlist)))
		items = items.filter(reduce(operator.or_,(Q(category=y) for y in foods)))
	#if user is filtering for neighbourhood only	
	if foods is None and neighbourhoods is not None:	
		items = items.filter(reduce(operator.or_,(Q(neighbourhood=y) for y in neighbourhoodlist)))
	#if user is filtering for food types only	
	if foods is not None and neighbourhoods is None:
		items = items.filter(reduce(operator.or_,(Q(category=y) for y in foods)))
	# set all dietary restrictions to None in order to see which ones will be checked when page is rendered	
	peanutfree= None
	vegetarian= None
	vegan  = None
	lactosefree= None
	fatfree= None
	glutenfree = None

	#filter for dietary restrictions
	if dietaryrestrictions != None:
		for restriction in dietaryrestrictions:
			if restriction == "peanutfree":
				items = items.filter(peanutfree = True)
				peanutfree= True
			elif restriction == "vegan":
				items = items.filter(vegan = True)	
				vegan = True
			elif restriction == "vegetarian":
				items = items.filter(vegetarian = True)
				vegetarian = True
			elif restriction == "glutenfree":
				items = items.filter(glutenfree = True)	
				glutenfree= True
			elif restriction == "fatfree":
				items = items.filter(fatfree = True)
				fatfree = True
			elif restriction == "lactosefree":
				items = items.filter(lactosefree = True)	
				lactosefree= True
	#finish the sortby	
	if sortby != "sortby":
		if sortby == "toprated":
			items = items.order_by("-rating")
		if sortby == "mostreviewed":
			items = items.order_by('-reviewcount')
		if sortby == "mostforked":
			items = items.order_by('-forkedcount')
		if sortby == "pricelowtohigh":
			items = items.order_by('price')
		if sortby == "pricehightolow":
			items = items.order_by('-price')
	#set querystring with all search parameters			
	request.session['filterquerystring']  = "?"+request.GET.urlencode()
	#get userinfo
	try:
		userinfo = UserInfo.objects.get(user = request.user)
	except:
		userinfo = None		
	#check to see if there is also a search query linked	
	if category =="Item":	
		#if findtype is featured, then get featureditems
		if findtype == 'featured' or findtype == None:
			featureditems=search_featured_items(query,items,"Item",iscity,citymunicipal,request)
		if findtype == 'toprated' or findtype == None:	
			toprateditems=search_top_rated_items(query,items,"Item",iscity,citymunicipal,request)
		if findtype == 'hiddengems' or findtype == None:
			hiddengemitems=search_hiddem_gem_items(query,items,"Item",iscity,citymunicipal,request)
		if findtype == 'cheapeats' or findtype == None:
			cheapeatitems=search_cheap_eat_items(query,items,"Item",iscity,citymunicipal,request)
		if findtype == 'everything' or findtype == None:
			everythingitems = search_everything_items(query,items,'Item',iscity,citymunicipal,request)
	#if user is searching for ingredient	
	elif category == "Ingredient":	
		if findtype == 'featured' or findtype == None:
			featureditems=search_featured_items(query,items,"Ingredient",iscity,citymunicipal,request)
		if findtype == 'toprated' or findtype == None:
			toprateditems=search_top_rated_items(query,items,"Ingredient",iscity,citymunicipal,request)
		if findtype == 'hiddengems' or findtype == None:
			hiddengemitems=search_hiddem_gem_items(query,items,"Ingredient",iscity,citymunicipal,request)
		if findtype == 'cheapeats' or findtype == None:
			cheapeatitems=search_cheap_eat_items(query,items,"Ingredient",iscity,citymunicipal,request)
		if findtype == 'everything' or findtype == None:
			everythingitems = search_everything_items(query,items,'Ingredient',iscity,citymunicipal,request)
	#if user is not searching for anything		
	else:	
		if findtype == 'featured' or findtype == None:
			featureditems= get_featured_items(citymunicipal,request,items)
		if findtype == 'toprated' or findtype == None:
			toprateditems= get_top_rated_items(citymunicipal,request,items)
		if findtype == 'hiddengems' or findtype == None:	
			hiddengemitems=get_hidden_gems(citymunicipal,request,items)
		if findtype == 'cheapeats' or findtype == None:	
			cheapeatitems=get_cheap_eats(citymunicipal,request,items)
		if findtype == 'everything' or findtype == None:
			everythingitems=get_everything(citymunicipal,request,items)
	#check to see if page exists	
	if findtype == None:
		isfeatureditems = check_page_exists(featureditems)
		istoprated = check_page_exists(toprateditems)
		ischeapeats = check_page_exists(cheapeatitems)
		ishiddengem = check_page_exists(hiddengemitems)
		iseverything = check_page_exists(everythingitems)	
	#check to see if we are looking for only featured items	with search parameters	
	if findtype=="featured":
		findtype = "featured"
		featureditems = request.session['featureditems']
		featureditemspagination = Paginator(featureditems,25)
		featureditems = featureditemspagination.page(1)
		form_args={'items':featureditems, 'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
	#if looking at all toprated items in viewall with all the unique searchparams
	elif findtype =="toprated":
		findtype = "toprated"
		toprateditems = request.session['toprateditems']
		pagination = Paginator(toprateditems,25)
		toprateditems = pagination.page(1)
		form_args={'items':toprateditems, 'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
	#if looking for hiddengems in view all with all unique search params
	elif findtype =="hiddengems":
		findtype = "hiddengems"
		hiddengemitems = request.session['hiddengemitems']
		pagination = Paginator(hiddengemitems,25)
		hiddengemitems = pagination.page(1)
		form_args={'items':hiddengemitems, 'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
	# looking for all the cheap eats in view all with all the unique search	
	elif findtype =="cheapeats":
		findtype = "cheapeats"
		cheapeatitems = request.session['cheapeatitems']
		pagination = Paginator(cheapeatitems,25)
		cheapeatitems = pagination.page(1)
		form_args={'items':cheapeatitems, 'userinfo':userinfo,'findtype' :findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
	#look for all items in view all.	
	elif findtype =="everything":
		findtype = "everything"
		everythingitems = request.session['everythingitems']
		pagination = Paginator(everythingitems,25)
		everythingitems = pagination.page(1)
		form_args={'items':everythingitems, 'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
	#if there is also a query, then search. Else, return items in pages
	#this search returns the main page with the seperate types of items
		
		#get userinfo
		
	else:		#set querystring	
		#return dietaryrestricion sortbys
		form_args={ 'featureditems':featureditems,'userinfo':userinfo,'isfeatureditems':isfeatureditems,'toprateditems':toprateditems,'istoprated':istoprated,'cheapeatitems':cheapeatitems,'ischeapeats':ischeapeats,'hiddengemitems':hiddengemitems,'ishiddengem':ishiddengem,'iseverything':iseverything,'everythingitems':everythingitems}

		return render_to_response("home.html",form_args, context_instance=RequestContext(request))








@csrf_exempt
#view that runs when user runs a search top bar only
def search(request):
	query = request.GET.get("q")
	category =request.GET.get('category')
	#set these query as session variablese
	request.session['searchquery'] = query
	request.session['searchcategory'] = category
	#get user location information
	get_user_city(request)
	try:
		userinfo = UserInfo.objects.get(user = request.user)
	except:
		userinfo = None
	#get whether a municipal is being searhed
	cityname = request.session['cityname']
	provincestatename = request.session['provincestate']
	#if last search was a city
	if request.session['iscity']:
		city  = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
		cityname = city.name
		iscity= True
		provincestatename = city.provincestate
	else:	
		municipal = Municipal.objects.get(name = request.session['cityname'], city__provincestate= request.session['provincestate'])
		cityname = municipal.name
		iscity = False
		provincestatename = municipal.city.provincestate

	if category == "Item" or category == "Ingredient":
		#check to see if input municipal or city
		if iscity == True:
			citymunicipal = city
			items = Item.objects.filter(city = citymunicipal)
		else:
			citymunicipal = municipal
			items = Item.objects.filter(municipal= citymunicipal)
		if category =="Item":
			featureditems=search_featured_items(query,items,"Item",iscity,citymunicipal,request)
			toprateditems=search_top_rated_items(query,items,"Item",iscity,citymunicipal,request)
			hiddengemitems=search_hiddem_gem_items(query,items,"Item",iscity,citymunicipal,request)
			cheapeatitems=search_cheap_eat_items(query,items,"Item",iscity,citymunicipal,request)
			everythingitems = search_everything_items(query,items,'Item',iscity,citymunicipal,request)
		#if user is searching for ingredient	
		elif category == "Ingredient":	
			featureditems=search_featured_items(query,items,"Ingredient",iscity,citymunicipal,request)
			toprateditems=search_top_rated_items(query,items,"Ingredient",iscity,citymunicipal,request)
			hiddengemitems=search_hiddem_gem_items(query,items,"Ingredient",iscity,citymunicipal,request)
			cheapeatitems=search_cheap_eat_items(query,items,"Ingredient",iscity,citymunicipal,request)
			everythingitems = search_everything_items(query,items,'Ingredient',iscity,citymunicipal,request)
	
		isfeatureditems = check_page_exists(featureditems)
		istoprated = check_page_exists(toprateditems)
		ischeapeats = check_page_exists(cheapeatitems)
		ishiddengem = check_page_exists(hiddengemitems)
		iseverything = check_page_exists(everythingitems)
	#set querystring
		request.session['filterquerystring']  = "?"+request.GET.urlencode()
		form_args={ 'featureditems':featureditems,'userinfo':userinfo,'isfeatureditems':isfeatureditems,'toprateditems':toprateditems,'istoprated':istoprated,'cheapeatitems':cheapeatitems,'ischeapeats':ischeapeats,'hiddengemitems':hiddengemitems,'ishiddengem':ishiddengem,'iseverything':iseverything,'everythingitems':everythingitems}
		return render_to_response("home.html",form_args, context_instance=RequestContext(request))

	if category == "Company":
		#if search is for city and not just municipal
		if request.session['iscity'] == True:
			companies = CompanyInfo.objects.filter(city= city, companyname__icontains = query)
		else:
			companies = CompanyInfo.objects.filter(municipal= municipal, companyname__icontains = query)
		#check to see if there are companies found
		if len(companies) == 0:
			companies = "None"

		form_args={'companies':companies}

	if category == "People":
		names = query.split()
		people = UserInfo.objects.filter(first_name__icontains=names[0], last_name__icontains= names[1])
		if len(people) ==0:
			people  = "None"
		form_args= {'people':people}
	form_args["query"]= query
	form_args["mainsearchcategory"]=category
	return render_to_response("home.html",form_args, context_instance=RequestContext(request))
#get all the featured items in viewall
def featured(request):
	#check to see if there are any search filter query
	if request.GET.urlencode() != ""  :
		return finditem(request,'featured')
	#if there are no search filters	
	else:
		get_user_city(request)
		city = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
		get_featured_items(city, request,None)
		featureditems = request.session['featureditems']
		featureditemspaginator = Paginator(featureditems,25)
		featureditems = featureditemspaginator.page(1)
		try:
			userinfo = UserInfo,objects.get(user  = request.user)
		except:
			userinfo = None
		findtype = "featured"	
		request.session['filterquerystring']  = ""
		form_args={'items':featureditems,'userinfo':userinfo,'findtype':findtype }
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
		
#get all the hidden gem items in viewall
def hiddengems(request):
	#check to see if there are any search filter query
	if request.GET.urlencode() != ""  :
		return finditem(request,'hiddengems')
	#if there are no search filters	
	else:
		get_user_city(request)
		city = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
		#check to see if there are already items in the session variable
		hiddengemitems = request.session['hiddengemitems']
		#set session variables
		get_hidden_gems(city, request,None)
		hiddengemitems = request.session['hiddengemitems']
		hiddengempaginator = Paginator(hiddengemitems,25)
		hiddengemitems = hiddengempaginator.page(1)
		try:
			userinfo = UserInfo,objects.get(user  = request.user)
		except:
			userinfo = None
		findtype = "hiddengems"	
		request.session['filterquerystring']  = ""
		form_args={'items':hiddengemitems,'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
		
#get all the toprated items in viewall
def toprated(request):
	#check to see if there are any search filter query
	if request.GET.urlencode() != ""  :
		return finditem(request,'toprated')
	#if there are no search filters	
	else:
		get_user_city(request)
		city = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
		#check to see if there are already items in the session variable
		#set session variables
		get_top_rated_items(city, request,None)
		toprateditems = request.session['toprateditems']
		
		topratedpaginator = Paginator(toprateditems,25)
		toprateditems = topratedpaginator.page(1)
		try:
			userinfo = UserInfo,objects.get(user  = request.user)
		except:
			userinfo = None
		findtype = "toprated"	
		request.session['filterquerystring']  = ""
		form_args={'items':toprateditems,'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
	
#get all cheap eat items in viewall			
def cheapeats(request):
	#check to see if there are any search filter query
	if request.GET.urlencode() != ""  :
		return finditem(request,'cheapeats')
	#if there are no search filters	
	else:
		get_user_city(request)
		city = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
		#check to see if there are already items in the session variable
		#set session variables
		get_cheap_eats(city, request,None)
		cheapeatitems = request.session['cheapeatitems']
		
		cheapeatpagination = Paginator(cheapeatitems,25)
		cheapeatitems = cheapeatpagination.page(1)
		try:
			userinfo = UserInfo,objects.get(user  = request.user)
		except:
			userinfo = None
		findtype = "cheapeats"	
		request.session['filterquerystring']  = ""
		form_args={'items':cheapeatitems,'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))

#get everything items in viewall
def everything(request):
	#check to see if there are any search filter query
	if request.GET.urlencode() != ""  :
		return finditem(request,'everything')
	#if there are no search filters	
	else:
		get_user_city(request)
		city = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
		#set session variables
		get_everything(city, request,None)
		everythingitems = request.session['everythingitems']
		
		everythingpaginator = Paginator(everythingitems,25)
		everythingitems = everythingpaginator.page(1)
		try:
			userinfo = UserInfo,objects.get(user  = request.user)
		except:
			userinfo = None
		request.session['filterquerystring']  = ""
		findtype = "everything"	
		form_args={'items':everythingitems,'userinfo':userinfo,'findtype':findtype}
		return render_to_response("viewall.html",form_args, context_instance=RequestContext(request))
	
@csrf_exempt
#ajax functino that runs auto suggestion of cities
def city_suggest(request):
	query = request.GET.get('query')
	municipals = Municipal.objects.filter(name__icontains = query)
	suggestions = []
	for municipal in municipals:
		suggestions.append(municipal.name+","+ municipal.city.provincestate)
		
	data = {'suggestions':suggestions,'query':query}
	jsondata=json.dumps(data)
	mimetype = 'application/json'
	return HttpResponse(jsondata,mimetype)

@csrf_exempt
#ajax neighbourhood suggest	
def neighbourhood_suggest(request):
	query = request.GET.get('query')
	municipal= request.GET.get("municipal")
	#get both city name and province
	municipal = municipal.split(',')
	municipalqueryset = Municipal.objects.filter(name = municipal[0])
	#check to see if db has a municipal with same name
	chosenmunicipal=None
	if municipalqueryset.count() > 1:
		for individualmunicipal in municipalqueryset:
			#if they have same provincestate then this is the selected municipal
			if individualmunicipal.city.provincestate == municipal[1]:
				chosenmunicipal = individualmunicipal
				break
	else:
		chosenmunicipal = municipalqueryset[0]
	print chosenmunicipal	

	#now that we have the chosen municipal, we can get all the neighbourhoods

	neighbourhoods = Neighbourhood.objects.filter(name__icontains=query,municipal = chosenmunicipal)			
	suggestions = []
	for neighbourhood in neighbourhoods:
		suggestions.append(neighbourhood.name)
		
	data = {'suggestions':suggestions,'query':query}
	jsondata=json.dumps(data)
	mimetype = 'application/json'
	return HttpResponse(jsondata,mimetype)

#set session variables to city
def get_user_city(request):
	#see if sesssion variables exist
	try:
		request.session['cityname']
		request.session['provincestate']
		request.session ['iscity'] 
		return
	except:
		
		GEOLOCATION_DIR = settings.GEOLOCATION_DIR
		gi4 = pygeoip.GeoIP(GEOLOCATION_DIR+'GeoLiteCity.dat', pygeoip.MEMORY_CACHE)
		clientipaddress = get_client_ip(request)
		clientaddress =  gi4.record_by_addr(clientipaddress)	
		#check to see if city exists in db
		try:
			cityname = clientaddress.get('city')
			provincestatename = clientaddress.get('region_name')
			city = City.objects.get(name=cityname,provincestate=provincestatename)
		except:
			city = City.objects.get(name="Vancouver",provincestate="BC")
			cityname = "Vancouver"
			provincestatename="BC"
		#set city in cache
		request.session['cityname']= city.name
		request.session['provincestate']= city.provincestate
		request.session ['iscity'] = True
		


def delete_session_variables(request):
	try:
		del request.session['searchquery']
	except:
		pass

	try:
		del request.session['searchcategory']
	except:
		pass
	try:
		del request.session['cityname']
	except:
		pass
	try:
		del rquest.session['provincestate']
	except:
		pass
	try:
		del request.session['iscity']
	except:	
		pass	
	try: 
		del request.session['foodfilter']
	except:
		pass
	try:
		del request.session['neighbourhoodfilter']
	except:
		pass
	try:
		del request.session['filterquerystring']
	except:
		pass


	
	
def raise_500(request):
		return render_to_response("500.html", context_instance=RequestContext(request))
	
def raise_404(request):
		return render_to_response("404.html", context_instance=RequestContext(request))
def get_featured_items(citymunicipal,request,items):
	if items is not None:
		featureditems = items.filter(featured=True)
	else:	
		iscity = request.session['iscity']
		if iscity:
			featureditems = Item.objects.filter(city= citymunicipal,featured = True)
		else:
			featureditems = Item.objects.filter(municipal=citymunicipal,featured =True)
	featureditems = list(featureditems)
	random.shuffle(featureditems)
	#save items into session variable	
	request.session['featureditems'] = featureditems
	featureditemspaginator = Paginator(featureditems,4)
	featureditems = featureditemspaginator.page(1)
	request.session['featureditemspage'] =1
	return featureditems

def get_top_rated_items(citymunicipal,request,items):
	print items
	if items is not None:
		toprateditems = items.order_by('-rating')
	else:	
		iscity = request.session['iscity']
		if iscity:
			toprateditems = Item.objects.filter(city= citymunicipal).order_by('-rating')
		else:
			toprateditems = Item.objects.filter(municipal= citymunicipal).order_by('-rating')
	toprateditems = list(toprateditems)
	#save toprated items into session variable	
	request.session['toprateditems'] = toprateditems
	paginator=Paginator(toprateditems,4)
	toprateditems = paginator.page(1)
	request.session['toprateditemspage'] =1
	return toprateditems

def get_cheap_eats(citymunicipal,request,items):
	if items is not None:
		cheapeatitems = items.filter(price__lte=10.00).order_by('-rating')
	else:		
		iscity = request.session['iscity']
		if iscity:
			cheapeatitems = Item.objects.filter(city= citymunicipal,price__lte=15.00).order_by('-rating')
		else:	
			cheapeatitems = Item.objects.filter(municipal= citymunicipal,price__lte=15.00).order_by('-rating')
	cheapeatitems = list(cheapeatitems)
	request.session['cheapeatitems'] = cheapeatitems
	paginator=Paginator(cheapeatitems,4)
	cheapeatitems = paginator.page(1)
	request.session['cheapeatitemspage'] =1
	return cheapeatitems

def get_hidden_gems(citymunicipal,request,items):
	if items is not None:
		hiddengemitems = items.filter(Q(rating=4)|Q(rating=5), reviewcount__lte=10)
	else:	
		iscity = request.session['iscity']
		if iscity:
			hiddengemitems = Item.objects.filter(Q(rating=4)|Q(rating=5),city=citymunicipal, reviewcount__lte=10)
		else:
			hiddengemitems = Item.objects.filter(Q(rating=4)|Q(rating=5),municipal=citymunicipal, reviewcount__lte=10)
	hiddengemitems=list(hiddengemitems)
	random.shuffle(hiddengemitems)
	request.session['hiddengemitems'] = hiddengemitems
	paginator=Paginator(hiddengemitems,4)
	hiddengemitems = paginator.page(1)
	request.session['hiddengemitemspage'] =1	
	return hiddengemitems


def get_everything(citymunicipal,request,items):
	if items is not None:
		everythingitems = items
	else:
		iscity= request.session['iscity']
		if iscity:
			everythingitems = Item.objects.filter(city=citymunicipal)		
		else:	
			everythingitems = Item.objects.filter(municipal=citymunicipal)
	everythingitems=list(everythingitems)
	random.shuffle(everythingitems)
	request.session['everythingitems'] = everythingitems
	paginator=Paginator(everythingitems,4)
	everythingitems = paginator.page(1)
	request.session['everythingitemspage'] =1	
	return everythingitems

#function to check if page in paginator exists
def check_page_exists(paginationpage):
	if len(paginationpage.object_list) == 0 :
		return None
	else:
		return  True
#return featured items from running search bar
def search_featured_items(query,items,searchtype,iscity,citymunicipal,request):
	if searchtype == "Item":
			featureditems = items.filter(Q(name__icontains = query)|Q(category__icontains=query),featured = True)
	elif searchtype =="Ingredient":
			featureditems = items.filter(featured = True,itemingredient__ingredient__icontains=query)


	featureditems = list(featureditems)
	random.shuffle(featureditems)
	#save items into session variable	
	request.session['featureditems'] = featureditems
	featureditemspaginator = Paginator(featureditems,4)
	featureditems = featureditemspaginator.page(1)
	request.session['featureditemspage'] =1
	return featureditems

#search for top rated items		
def search_top_rated_items(query,items,searchtype,iscity,citymunicipal,request):
	#check to see if we are searching city 
	if searchtype == "Item":
		toprateditems = items.filter(Q(name__icontains = query)|Q(category__icontains=query)).order_by('-rating')
	elif searchtype =="Ingredient":
		toprateditems = items.filter(itemingredient__ingredient__icontains=query).order_by('-rating')

	toprateditems = list(toprateditems)
	#save toprated items into session variable	
	request.session['toprateditems'] = toprateditems
	paginator=Paginator(toprateditems,4)
	toprateditems = paginator.page(1)
	request.session['toprateditemspage'] =1
	return toprateditems

#return hidden gem items from running search bar
def search_hiddem_gem_items(query,items,searchtype,iscity,citymunicipal,request):
	#check to see if we are searching city 
	if searchtype == "Item":
		hiddengemitems = items.filter(Q(name__icontains = query)|Q(category__icontains=query),Q(rating=4)|Q(rating=5),reviewcount__lte=10)
	elif searchtype =="Ingredient":
		hiddengemitems = items.filter(Q(rating=4)|Q(rating=5),reviewcount__lte=10,itemingredient__ingredient__icontains=query)


	hiddengemitems = list(hiddengemitems)
	random.shuffle(hiddengemitems)
	#save items into session variable	
	request.session['hiddengemitems'] = hiddengemitems
	itemspaginator = Paginator(hiddengemitems,4)
	hiddengemitems = itemspaginator.page(1)
	request.session['hiddengemitemspage'] =1
	return hiddengemitems

#return cheap eats
def search_cheap_eat_items(query,items,searchtype,iscity,citymunicipal,request):
	#check to see if we are searching city 
	if searchtype == "Item":
		cheapeatitems = items.filter(Q(name__icontains = query)|Q(category__icontains=query),price__lte=10.00).order_by('-rating')
	elif searchtype =="Ingredient":
		cheapeatitems = items.filter(price__lte=10.00,itemingredient__ingredient__icontains=query).order_by('-rating')


	cheapeatitems = list(cheapeatitems)
	random.shuffle(cheapeatitems)
	#save items into session variable	
	request.session['cheapeatitems'] = cheapeatitems
	itemspaginator = Paginator(cheapeatitems,4)
	cheapeatitems = itemspaginator.page(1)
	request.session['cheapeatitemspage'] =1
	return cheapeatitems

#search everything(all items)
def search_everything_items(query,items,searchtype,iscity,citymunicipal,request):
	if searchtype == "Item":
		everythingitems = items.filter(Q(name__icontains = query)|Q(category__icontains=query))
	elif searchtype =="Ingredient":
		everythingitems = items.filter(itemingredient__ingredient__icontains=query)


	everythingitems = list(everythingitems)
	random.shuffle(everythingitems)
	#save items into session variable	
	request.session['everythingitems'] = everythingitems
	itemspaginator = Paginator(everythingitems,4)
	everythingitems = itemspaginator.page(1)
	request.session['everythingitemspage'] =1
	return everythingitems

@csrf_exempt
#function deals with the navigation of featured items
def navigation_slider(request):
	if request.method == "POST" and request.is_ajax():
		action  = request.POST.get('action')
		sliderelement=request.POST.get('sliderelement')
		print action
		print sliderelement
		#check to see what slider is being interacted with
		#if slider type is featured
		if sliderelement == "featured":
			#get featured items
			items = request.session['featureditems']
			#get page number
			pageno = request.session['featureditemspage']
			sessionpage= 'featureditemspage'
		if sliderelement == "toprated":
			#get top rated items
			items = request.session['toprateditems']
			#get page number
			pageno = request.session['toprateditemspage']
			sessionpage='toprateditemspage'
		if sliderelement == "hiddengems":
			#get hidden gems items
			items = request.session['hiddengemitems']
			#get page number
			pageno = request.session['hiddengemitemspage']
			sessionpage = 'hiddengemitemspage'
		if sliderelement == "cheapeats":
			#get cheap eat  items
			items = request.session['cheapeatitems']
			#get page number
			pageno = request.session['cheapeatitemspage']
			sessionpage = 'cheapeatitemspage'
		if sliderelement == "everything":
			#get cheap eat  items
			items = request.session['everythingitems']
			#get page number
			pageno = request.session['everythingitemspage']
			sessionpage = 'everythingitemspage'
	
		#paginate the items	
		itemspaginator = Paginator(items,4)
		if action == "next":
			pageno = pageno+1		
			#try to get items. If there exists, set pageno if not return none
			try:
				items = itemspaginator.page(pageno)
				request.session[sessionpage] = pageno		
			except:
				items = None
		if action == "previous":	
			pageno = pageno -1
			#try to get items. If it exists, set page no, if not return none
			try:
				items = itemspaginator.page(pageno)
				request.session[sessionpage] = pageno		
			except:
				items = None
		STATIC_URL = settings.STATIC_URL
		MEDIA_URL = settings.MEDIA_URL
		form_args={'MEDIA_URL':MEDIA_URL,'STATIC_URL':STATIC_URL,'items':items}
		return HttpResponse( render_to_string('loadmoreslider.html', form_args))
	
#navigation for top rated items	
def navigation_top_rated(request):
	if request.method == "POST" and request.is_ajax():
		action  = request.POST.get('action')
		#get featured items
		toprateditems = request.session['toprateditems']
		#get page number
		pageno = request.session['toprateditemspage']
		toprateditemspagination = Paginator(toprateditems,4)
		if action == "next":
			pageno = pageno+1		
			#try to get items. If there exists, set pageno if not return none
			try:
				items = toprateditemspagination.page(pageno)
				request.session['toprateditemspage'] = pageno		
			except:
				items = None
		if action == "previous":	
			pageno = pageno -1
			#try to get items. If it exists, set page no, if not return none
			try:
				items = toprateditems.page(pageno)
				request.session['featureditemspage'] = pageno		
			except:
				items = None
		STATIC_URL = settings.STATIC_URL
		MEDIA_URL = settings.MEDIA_URL
		form_args={'MEDIA_URL':MEDIA_URL,'STATIC_URL':STATIC_URL,'items':items}
		return HttpResponse( render_to_string('loadmoreslider.html', form_args))
	
#get items using json for mobile
def get_items(request):
	get_user_city(request)
	iscity = request.session['iscity']
	citymunicipal = City.objects.get(name = request.session['cityname'], provincestate =request.session['provincestate'])
	featureditemsqset = Item.objects.filter(city= citymunicipal,featured = True)
	featureditems = list(featureditemsqset)
	random.shuffle(featureditems)
	#save items into session variable	
	request.session['featureditems'] = featureditems
	#featured items are now a list of dictionary objects
	featureditems= featureditemsqset.values()
	featureditemspaginator = Paginator(featureditems,10)
	featureditems = featureditemspaginator.page(1)
	request.session['featureditemspage'] =1
	for itemlist , itemqset in zip(featureditems.object_list,featureditemsqset):
		itemlist['companyname'] = itemqset.companyinfo.companyname
		itemlist['companyaddress'] = itemqset.companyinfo.address
		itemlist['companyprovincestate'] = itemqset.companyinfo.city.provincestate
		itemlist['companymunicipal'] = itemqset.companyinfo.municipal.name
		itemlist['pictureurl'] = itemqset.mainitemimage.url
		
	dict= {'featureditems':list(featureditems.object_list)}
	return HttpResponse(simplejson.dumps(dict))		

def check_mobile(request):
	try:
		viewfullsite = request.session['viewfullsite']
	except:
		viewfullsite = None
	
	return HttpResponse(viewfullsite)
	
