from django.conf.urls import patterns, include, url
from django.conf import settings
#enable admin
from django.contrib import admin
admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
handler500 = 'bonappetreat.views.raise_500'
handler404 = 'bonappetreat.views.raise_404'

urlpatterns = patterns('',

    (r'^myaccount/', include('UserInfo.urls')),
    (r'^items/', include('Items.urls')),
    (r'^mobile/', include('Mobile.urls')),
    (r'users/', include('Users.urls')),
   

    url(r'loadmore/(?P<devicetype>\w+)/(?P<findtype>\w+)/(?P<tag>\w+)/$', 'bonappetreat.views.load_more'),
    url(r'^$', 'bonappetreat.views.home', name='home'),
    url(r'citysuggest', 'bonappetreat.views.city_suggest'),
    # admin site url
    url(r'^admin/', include(admin.site.urls)),
    url(r'^featured', 'bonappetreat.views.featured'),
    url(r'^toprated', 'bonappetreat.views.toprated'),
    url(r'^hiddengems', 'bonappetreat.views.hiddengems'),
    url(r'^cheapeats', 'bonappetreat.views.cheapeats'),
    url(r'^everything', 'bonappetreat.views.everything'),
    url(r'neighbourhoodsuggest', 'bonappetreat.views.neighbourhood_suggest'),
    url(r'about', 'bonappetreat.views.about'),
    url(r'companyinfo', 'bonappetreat.views.company'),
    url(r'itemframe', 'bonappetreat.views.itemframe'),
    url(r'^find', 'bonappetreat.views.find'),
    url(r'^search', 'bonappetreat.views.search'),
    url(r'getneighbourhoods', 'bonappetreat.views.get_neighbourhoods'),
    url(r'getfoods', 'bonappetreat.views.get_foods'),
    url(r'applyfilters', 'bonappetreat.views.apply_filters'),
    url(r'navigationslider', 'bonappetreat.views.navigation_slider'),
    #check to see if mobile user wants to see full site
   url(r'checkmobile', 'bonappetreat.views.check_mobile'),

    #all mobile json data
     url(r'getfeatureditems', 'bonappetreat.views.get_featured_items'),

    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    # Examples:
    # url(r'^$', 'bonappetreat.views.home', name='home'),
    # url(r'^bonappetreat/', include('bonappetreat.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
