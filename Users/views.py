
from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.http import Http404  ,HttpResponse 
from UserInfo.models import  UserInfo, CompanyInfo
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
import Image
from django.core.validators import validate_email
from Items.models import *
from django.template.loader import render_to_string
from Users.models import *




#load company profile
def company_profile(request,tag):
	#try to get company to see if it exists
	try: 
		company = CompanyInfo.objects.get(id = tag)
	except:
		raise Http404
	items = Item.objects.filter(companyinfo = company)
	if request.method == "GET":
		form_args={'company':company, 'items':items}
		return render_to_response("Users/company_profile.html",form_args,context_instance=RequestContext(request))

#load user profile
def user_profile(request,tag):
	#try to get company to see if it exists
	try: 
		user  = User.objects.get(id = tag)
		userinfo = UserInfo.objects.get(user = user)
	except:
		raise Http404
	items = Item.objects.filter(postedby = user)
	if request.method == "GET":
		recentactivity = RecentActivity.objects.filter(user = userinfo).order_by('-datetime')
		itemsforked = UserForked.objects.filter(user = user)
		userfollowers = UserFollowedBy.objects.get(user=userinfo).followedby
		userfollowingcount = UserFollowing.objects.get(user=userinfo).following.count()
		userfollowerscount = userfollowers.count()
		#check to see if request user is following user.
		try: 
			checkuser = UserInfo.objects.get(user  = request.user)
			if checkuser in userfollowers.all():
				useralreadyfollowing = True
			else:
				useralreadyfollowing = None
		except:
			useralreadyfollowing = None


		items =[]
		for item in itemsforked:
			items.append(item.item)
		numpoints = userinfo.numberofpoints
		if numpoints <30:
			foodielevel = 1
		if numpoints>=30 and numpoints<80:
			foodielevel = 2
		if numpoints >=80 and numpoints <160:
			foodielevel = 3
		form_args={'userinfo':userinfo, 'items':items,'chosenuser':user,'foodielevel':foodielevel,'userfollowingcount':userfollowingcount,'userfollowerscount':userfollowerscount,'useralreadyfollowing':useralreadyfollowing,'recentactivity':recentactivity}
		return render_to_response("Users/user_profile.html",form_args,context_instance=RequestContext(request))


#show users following and followedby	
def followers(request,tag):
#try to get user to see if it exists
	if request.user.is_authenticated():
		try:
			requestuserinfo = UserInfo.objects.get(user=request.user)
			requestuserinfofollowing = UserFollowing.objects.get(user=requestuserinfo)
		except:
			requestuserinfo = None
	else:
		requestuserinfo = None
	try: 
		user  = User.objects.get(id = tag)
		userinfo = UserInfo.objects.get(user = user)
	except:
		raise Http404
	if request.method == "GET":
		try:
			usersfollowing = UserFollowing.objects.get(user= userinfo)
			usersfollowers= UserFollowedBy.objects.get(user=userinfo)		
		except:
			raise Http404


		form_args = {'usersfollowing':usersfollowing, 'usersfollowers':usersfollowers,'userinfo':userinfo,'requestuserinfofollowing':requestuserinfofollowing,'requestuserinfo':requestuserinfo}
		return render_to_response("Users/followers.html",form_args,context_instance=RequestContext(request))

@csrf_exempt
#ajax load of reviews 
def load_reviews(request):

	if request.is_ajax() and request.method == "POST":
		MEDIA_URL = settings.MEDIA_URL
		STATIC_URL = settings.STATIC_URL
		userid =  request.POST.get("id")
		usertype =   request.POST.get("type")
		#if we are loading reviews created by company
		if usertype == "company":
			try:
				company = CompanyInfo.objects.get(id = userid)
				itemreviews = ItemReview.objects.filter(companyreviewed = company)
			except:
				raise Http404
		#if we want to load reviews created by user	
		else:
			try:
				user = User.objects.get(id = userid)
				itemreviews = ItemReview.objects.filter(postedby = user)
			except:
				raise Http404
		form_args= {'itemreviews':itemreviews, 'MEDIA_URL':MEDIA_URL,'STATIC_URL':STATIC_URL,'user':request.user,'usertype':usertype}
		print render_to_string('Users/reviews_load.html', form_args)
		return HttpResponse( render_to_string('Users/reviews_load.html', form_args))
	else:
		raise Http404
@csrf_exempt
#load items posted tab
def load_items_posted(request):
	if request.is_ajax() and request.method == "POST":
		userid = request.POST.get('id')
		try:
			user =  User.objects.get(id =userid)
		except: 
			raise Http404

		items = Item.objects.filter(postedby = user)
		MEDIA_URL = settings.MEDIA_URL
		STATIC_URL = settings.STATIC_URL
		form_args= {'items':items,'user':request.user, 'MEDIA_URL':MEDIA_URL,'STATIC_URL':STATIC_URL}
		return HttpResponse( render_to_string('loadmore.html', form_args))
	else: 
		raise Http404
@csrf_exempt
#load pictures posted tab
def load_pictures_posted(request):
		userid = request.POST.get('id')
		try:
			user =  User.objects.get(id =userid)
		except: 
			raise Http404

		items = ItemImage.objects.filter(postedby = user,finished = True)
		MEDIA_URL = settings.MEDIA_URL
		STATIC_URL = settings.STATIC_URL
		form_args= {'items':items, 'MEDIA_URL':MEDIA_URL,'STATIC_URL':STATIC_URL}
		return HttpResponse( render_to_string('Users/load_pictures_posted.html', form_args))
		raise Http404
@csrf_exempt
#ajax call to follow user when userclicks follow user button
def follow_user(request):
	if request.method == "POST" and request.is_ajax():
		followinguserid = request.POST.get('followid')
		#get all the users that request user is following
		try:
			userinfo = UserInfo.objects.get(user=request.user)
			userfollowing = UserInfo.objects.get(id = followinguserid)
			userfollowinglist = UserFollowing.objects.get(user = userinfo)
		except:
			raise Http404
		#check to see if user is in userfollowinglist. If not, add to list
		if userfollowing  in userfollowinglist.following.all():
			raise Http404
		else:

			userfollowinglist.following.add(userfollowing)
			userfollowinglist.save()
		#now add request user to userfollowing followedby list	
		try:
			userfollowedlist = UserFollowedBy.objects.get(user = userfollowing)
		except: 
			raise Http404
		#check to see if request usr is in list alreadty
		if userinfo in userfollowedlist.followedby.all():
			raise Http404
		else:
			userfollowedlist.followedby.add(userinfo)
			userfollowedlist.save()

		return HttpResponse()	
		
	else:	
		raise Http404

@csrf_exempt
#ajax call to unfollow user when userclicks unfollow user button
def unfollow_user(request):
	if request.method == "POST" and request.is_ajax():
		followinguserid = request.POST.get('followid')

		print followinguserid
		#get all the users that request user is following
		try:
			userinfo = UserInfo.objects.get(user=request.user)
			userfollowing = UserInfo.objects.get(id = followinguserid)
			userfollowinglist = UserFollowing.objects.get(user = userinfo)
		except:
			raise Http404
		#check to see if user is in userfollowinglist. If not, add to list
		if userfollowing  in userfollowinglist.following.all():
			userfollowinglist.following.remove(userfollowing)
			userfollowinglist.save()

		else:

			raise Http404
					#now add request user to userfollowing followedby list	
		try:
			userfollowedlist = UserFollowedBy.objects.get(user = userfollowing)
		except: 
			raise Http404
		#check to see if request usr is in list alreadty
		if userinfo in userfollowedlist.followedby.all():
			userfollowedlist.followedby.remove(userinfo)
			userfollowedlist.save()
		else:

			raise Http404
			

		return HttpResponse()	
		
	else:	
		raise Http404
