from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('',
    url(r'^company/(?P<tag>\w+)/$', 'Users.views.company_profile'),
    url(r'^user/(?P<tag>\w+)/$', 'Users.views.user_profile'),
    url(r'^followers/(?P<tag>\w+)/$', 'Users.views.followers'),
    url(r'^loadreviews', 'Users.views.load_reviews'),
    url(r'^loaditemsposted', 'Users.views.load_items_posted'),
    url(r'^loadpicturesposted', 'Users.views.load_pictures_posted'),
    url(r'^followuser', 'Users.views.follow_user'),
    url(r'^unfollowuser', 'Users.views.unfollow_user'),
    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
) 
