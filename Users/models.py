from django.db import models
# Create your models here.
from django.contrib.auth.models import User
from UserInfo.models import CompanyInfo,UserInfo
from django.conf import settings
from Cities.models import *
from Items.models import *

class UserFollowing(models.Model):
	user = models.ForeignKey(UserInfo,related_name= "userfollowing_user")
	following = models.ManyToManyField(UserInfo,related_name="userfollowing_following")

class UserFollowedBy(models.Model):
	user = models.ForeignKey(UserInfo,related_name ="userfollowedby_user")
	followedby = models.ManyToManyField(UserInfo,related_name="userfollowedby_followedby")


class RecentActivity(models.Model):
	user = models.ForeignKey(UserInfo)
	activitytype=  models.CharField(max_length=20)
	item = models.ForeignKey(Item,blank = True, null = True)
	review = models.ForeignKey(ItemReview, blank = True,  null=True)
	tip = models.ForeignKey(ItemTip, blank = True, null=True)
	picture = models.ForeignKey(ItemImage,blank = True,null=True)
	datetime = models.DateTimeField(auto_now = True,auto_now_add = True)



# Create your models here.
