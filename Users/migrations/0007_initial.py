# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserFollowing'
        db.create_table('Users_userfollowing', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='userfollowing_user', to=orm['UserInfo.UserInfo'])),
        ))
        db.send_create_signal('Users', ['UserFollowing'])

        # Adding M2M table for field following on 'UserFollowing'
        m2m_table_name = db.shorten_name('Users_userfollowing_following')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userfollowing', models.ForeignKey(orm['Users.userfollowing'], null=False)),
            ('userinfo', models.ForeignKey(orm['UserInfo.userinfo'], null=False))
        ))
        db.create_unique(m2m_table_name, ['userfollowing_id', 'userinfo_id'])

        # Adding model 'UserFollowedBy'
        db.create_table('Users_userfollowedby', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='userfollowedby_user', to=orm['UserInfo.UserInfo'])),
        ))
        db.send_create_signal('Users', ['UserFollowedBy'])

        # Adding M2M table for field followedby on 'UserFollowedBy'
        m2m_table_name = db.shorten_name('Users_userfollowedby_followedby')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userfollowedby', models.ForeignKey(orm['Users.userfollowedby'], null=False)),
            ('userinfo', models.ForeignKey(orm['UserInfo.userinfo'], null=False))
        ))
        db.create_unique(m2m_table_name, ['userfollowedby_id', 'userinfo_id'])

        # Adding model 'RecentActivity'
        db.create_table('Users_recentactivity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['UserInfo.UserInfo'])),
            ('activitytype', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.Item'], null=True, blank=True)),
            ('review', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.ItemReview'], null=True, blank=True)),
            ('tip', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.ItemTip'], null=True, blank=True)),
            ('picture', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Items.ItemImage'], null=True, blank=True)),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('Users', ['RecentActivity'])


    def backwards(self, orm):
        # Deleting model 'UserFollowing'
        db.delete_table('Users_userfollowing')

        # Removing M2M table for field following on 'UserFollowing'
        db.delete_table(db.shorten_name('Users_userfollowing_following'))

        # Deleting model 'UserFollowedBy'
        db.delete_table('Users_userfollowedby')

        # Removing M2M table for field followedby on 'UserFollowedBy'
        db.delete_table(db.shorten_name('Users_userfollowedby_followedby'))

        # Deleting model 'RecentActivity'
        db.delete_table('Users_recentactivity')


    models = {
        'Cities.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'Cities.municipal': {
            'Meta': {'object_name': 'Municipal'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Cities.neighbourhood': {
            'Meta': {'object_name': 'Neighbourhood'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Items.item': {
            'Meta': {'object_name': 'Item'},
            'allowpurchase': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '30'}),
            'companyinfo': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['UserInfo.CompanyInfo']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'fatfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'flagcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'flaggedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'item_flaggedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'forkedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'item_forkedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'forkedcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'glutenfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lactosefree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mainitemimage': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'main_image'", 'null': 'True', 'to': "orm['Items.ItemImage']"}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'peanutfree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'item_postedby'", 'to': "orm['auth.User']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': 'None', 'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'rating': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'reviewcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'vegan': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'vegetarian': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'viewcount': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'Items.itemimage': {
            'Meta': {'object_name': 'ItemImage'},
            'finished': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']", 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mainimage': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'Items.itemreview': {
            'Meta': {'object_name': 'ItemReview'},
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'companyreviewed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.CompanyInfo']"}),
            'dateadded': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'flagcount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'flaggedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemreview_flaggedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'numrecommends': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'itemreview_postedby'", 'to': "orm['auth.User']"}),
            'rating': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'recommendedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemreview_recommendedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'userinfo': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['UserInfo.UserInfo']", 'null': 'True', 'blank': 'True'})
        },
        'Items.itemtip': {
            'Meta': {'object_name': 'ItemTip'},
            'companyinfo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.CompanyInfo']"}),
            'dateposted': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']"}),
            'numrecommends': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'postedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'itemtip_postedby'", 'to': "orm['auth.User']"}),
            'recommendedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'itemtip_recommendedby'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'tip': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'userinfo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.UserInfo']"})
        },
        'UserInfo.companyinfo': {
            'Meta': {'object_name': 'CompanyInfo'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'companylogo': ('django.db.models.fields.CharField', [], {'default': "'photo/static/default.jpg'", 'max_length': '200'}),
            'companyname': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'municipal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Municipal']"}),
            'neighbourhood': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.Neighbourhood']", 'null': 'True', 'blank': 'True'}),
            'phonenumber': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'postalcode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'useradded': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visitstopage': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'UserInfo.userinfo': {
            'Meta': {'object_name': 'UserInfo'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditcardposted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instagramhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'itemsposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'numberofpoints': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'numusersfollowing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'picturesposted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pintresthandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'profilepictureurl': ('django.db.models.fields.CharField', [], {'default': "'photo/static/default.jpg'", 'max_length': '200'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            's3key': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'twitterhandle': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'usersfollowed': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowed'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'usersfollowing': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userinfo_usersfollowing'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Users.recentactivity': {
            'Meta': {'object_name': 'RecentActivity'},
            'activitytype': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.Item']", 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.ItemImage']", 'null': 'True', 'blank': 'True'}),
            'review': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.ItemReview']", 'null': 'True', 'blank': 'True'}),
            'tip': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Items.ItemTip']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['UserInfo.UserInfo']"})
        },
        'Users.userfollowedby': {
            'Meta': {'object_name': 'UserFollowedBy'},
            'followedby': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userfollowedby_followedby'", 'symmetrical': 'False', 'to': "orm['UserInfo.UserInfo']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'userfollowedby_user'", 'to': "orm['UserInfo.UserInfo']"})
        },
        'Users.userfollowing': {
            'Meta': {'object_name': 'UserFollowing'},
            'following': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userfollowing_following'", 'symmetrical': 'False', 'to': "orm['UserInfo.UserInfo']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'userfollowing_user'", 'to': "orm['UserInfo.UserInfo']"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Users']