from django.conf.urls import patterns, include, url
from django.conf import settings

urlpatterns = patterns('',
		
		url(r'^featured', 'Mobile.views.featured'),
		url(r'^toprated', 'Mobile.views.toprated'),
		url(r'^hiddengems', 'Mobile.views.hiddengems'),
		url(r'^cheapeats', 'Mobile.views.cheapeats'),
		url(r'^everything', 'Mobile.views.everything'),
		url(r'^additem','Mobile.views.add_item'),
		url(r'^viewfullsite','Mobile.views.view_full_site'),
		url(r'^login','Mobile.views.mobilelogin'),
		url(r'^signup','Mobile.views.signup'),
		
		url(r'^search','Mobile.views.search'),
		url(r'^items/(?P<tag>\w+)','Mobile.views.item'),
		url(r'^review/(?P<reviewtype>\w+)/(?P<tag>\w+)','Mobile.views.review'),
		url(r'^tips/(?P<tiptype>\w+)/(?P<tag>\w+)','Mobile.views.tip'),
		url(r'^morepictures/(?P<tag>\w+)','Mobile.views.morepictures'),
		url(r'^company/menu/(?P<tag>\w+)','Mobile.views.menu'),
		url(r'^company/(?P<tag>\w+)','Mobile.views.company'),
		#ajax loads
		url(r'^loadsearchitems','Mobile.views.loadsearchitems'),
		url(r'^loadmoresearch','Mobile.views.loadmoresearch'),

		
		
		url(r'^', 'Mobile.views.home'),
	         #set root for static files (css, images, etc)
	         url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
	         url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	         )
