from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from boto.s3.key import Key
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404 ,HttpResponse 
import boto
from UserInfo.models import  UserInfo, CompanyInfo
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
from django.core.validators import validate_email
from Items.models import *
import Image
import json
from random import randint
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.template.loader import render_to_string
from Foods.models import Food
import os
from Cities.models import *
from Users.models import *
import cStringIO
import imghdr
import urllib
from bonappetreat.views import *
from django.contrib.auth.models import User,Group
from django.core.validators import validate_email
from django.core.mail import EmailMessage,send_mail

from django.contrib.auth import authenticate,login,get_user

def home(request):
	return render_to_response('Mobile/home.html',context_instance=RequestContext(request))

def featured(request):
	get_user_city(request)
	city = City.objects.get(name = request.session['cityname'], provincestate =request.session['provincestate'])
	#get featured items
	get_featured_items(city,request,None)
	findtype = "featured"
	featureditemspaginator = Paginator(request.session['featureditems'],25)
	featureditems = featureditemspaginator.page(1)
	isitemsexist= check_page_exists(featureditems)
	form_args= {'items':featureditems,'findtype':findtype,'isitemsexist':isitemsexist}
	return render_to_response('Mobile/viewall.html',form_args,context_instance=RequestContext(request))

def toprated(request):
	get_user_city(request)
	city = City.objects.get(name = request.session['cityname'], provincestate =request.session['provincestate'])
	#get featured items
	get_top_rated_items(city,request,None)
	findtype = "toprated"
	paginator = Paginator(request.session['toprateditems'],25)
	toprateditems = paginator.page(1)
	isitemsexist= check_page_exists(toprateditems)
	form_args= {'items':toprateditems,'findtype':findtype,'isitemsexist':isitemsexist}
	return render_to_response('Mobile/viewall.html',form_args,context_instance=RequestContext(request))

def hiddengems(request):
	get_user_city(request)
	city = City.objects.get(name = request.session['cityname'], provincestate =request.session['provincestate'])
	#get featured items
	get_hidden_gems(city,request,None)
	findtype = "hiddengems"
	paginator = Paginator(request.session['hiddengemitems'],25)
	hiddengemitems = paginator.page(1)
	isitemsexist= check_page_exists(hiddengemitems)
	form_args= {'items':hiddengemitems,'findtype':findtype,'isitemsexist':isitemsexist}
	return render_to_response('Mobile/viewall.html',form_args,context_instance=RequestContext(request))

def cheapeats(request):
	get_user_city(request)
	city = City.objects.get(name = request.session['cityname'], provincestate =request.session['provincestate'])
	#get featured items
	get_cheap_eats(city,request,None)
	findtype = "cheapeats"
	paginator = Paginator(request.session['cheapeatitems'],25)
	cheapeatitems = paginator.page(1)
	isitemsexist= check_page_exists(cheapeatitems)
	form_args= {'items':cheapeatitems,'findtype':findtype,'isitemsexist':isitemsexist}
	return render_to_response('Mobile/viewall.html',form_args,context_instance=RequestContext(request))

def everything(request):
	get_user_city(request)
	city = City.objects.get(name = request.session['cityname'], provincestate =request.session['provincestate'])
	#get featured items
	get_everything(city,request,None)
	findtype = "everything"
	paginator = Paginator(request.session['everythingitems'],25)
	everythingitems = paginator.page(1)
	isitemsexist= check_page_exists(everythingitems)
	form_args= {'items':everythingitems,'findtype':findtype,'isitemsexist':isitemsexist}
	return render_to_response('Mobile/viewall.html',form_args,context_instance=RequestContext(request))


def add_item(request):
	if request.user.is_authenticated == False:
		return HttpResponseRedirect("/mobile/login")
		#get all the food categories
	foods = Food.objects.filter(category = "Food").order_by('foodtype')
	desserts = Food.objects.filter(category = "Dessert").order_by('foodtype')
	bakerygoods  = Food.objects.filter(category="Bakery Goods").order_by('foodtype')
	drinks = Food.objects.filter(category ="Drinks").order_by("foodtype")
	if request.method == "GET":
		items = ItemImage.objects.filter(postedby=request.user, finished = False)
		print items
		form_args = {'items':items,'foods':foods,'desserts':desserts,'bakerygoods':bakerygoods,"drinks":drinks}
		return render_to_response("Mobile/additem.html",form_args, context_instance=RequestContext(request))
	if request.method == "POST":
		images = request.FILES.getlist('uploadpictures')
		isuploadpicture = request.POST.get("isuploadpicture")
		
		#if user is uploading pictures
		if isuploadpicture == "True":
			for image in images:
					try:
				    		testimage= Image.open(image)

					except:
							#if file type is not an image that is supported	
					    	errormessage= "The image with filename " + image.name + " is not supported."
						items= ItemImage.objects.filter(postedby = request.user, finished = False)
						form_args={'errormessage':errormessage,'items':items}
						return render_to_response('Mobile/additem.html',form_args,context_instance=RequestContext(request))
							
							#if imagefile is supported
					#check to see that image size is no larger then 4mb
					if image.size > 5242880:
						errormessage= "The image with filename " + image.name + " is too large (over 4mb)."
						items= ItemImage.objects.filter(postedby = request.user, finished = False)
						form_args={'errormessage':errormessage,'items':items}
						return render_to_response('Mobile/additem.html',form_args,context_instance=RequestContext(request))
					#else is image is not that size

					newimage  = ItemImage( postedby = request.user)
					#scale image
					maxwidth= 400
					maxheight=400

					if int(testimage.size[0]) > maxwidth or int(testimage.size[1])>maxheight:
						#see if width is more is more
						if float(testimage.size[0])/float(testimage.size[1]) >1:
							widthratio=  float(testimage.size[1])/float(testimage.size[0])
							height = int(maxwidth*widthratio)
							size = (maxwidth,height)
							testimage = testimage.resize(size,Image.ANTIALIAS)
						#see if height is more is more
						if float(testimage.size[1])/float(testimage.size[0]) >1:
							heightratio=  float(testimage.size[0])/float(testimage.size[1])
							width = int(heightratio*maxwidth)
							size = (width,maxheight)
							testimage = testimage.resize(size,Image.ANTIALIAS)
	

							


					#connect to s3 bucket
					conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
					bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)	
					k= Key(bucket)

					
					#get time and format to make eah filename unique
					time = str(datetime.datetime.now())
					time.replace(" ","")
					time.replace(".","")
					#get file name and extention time
					filename = image.name
					#check to see if file type is image	
					if filename.endswith('.jpg') or filename.endswith('.jpeg'):
						fileextention = 'jpeg'
					elif filename.endswith('.png'):
						fileextention = 'png'
					elif filename.endswith('.gif'):
						fileextention = 'gif'
					else:
						fileextention = None
					#get filename to save to
					filename = filename.split('.')
					filename = filename[0]
					if fileextention == None:

						errormessage= "The image with filename " + image.name + " is not an image"
						items= ItemImage.objects.filter(postedby = request.user, finished = False)
						form_args={'errormessage':errormessage,'items':items}
						return render_to_response('Mobile/viewall.html',form_args,context_instance=RequestContext(request))

					#saveurl to point there
					url ='photos/items/'+filename+time+"."+fileextention
					#get key name to upload to s3, filename
					k.key='photos/items/'+filename+time+"."+fileextention
					out_im2 = cStringIO.StringIO()
					#save image to s3	
					testimage.save(out_im2, fileextention, quality=50)
					k.set_contents_from_string(out_im2.getvalue())
					#set as public
					k.set_acl('public-read')
					#set url and key in s3 server
					url=urllib.quote_plus(url)	
					newimage.url = url	
					newimage.key = k.key
					newimage.save()
					

			 #itemimages =add_images(request,images,item)
			items= ItemImage.objects.filter(postedby= request.user,finished = False) 
		
			form_args={'items':items,'desserts':desserts,'foods':foods,'bakerygoods':bakerygoods}
			return render_to_response("Mobile/additem.html",form_args,context_instance=RequestContext(request))
	

def item(request,tag):
	try:
		item = Item.objects.get(id= tag)
	except: 
		raise Http404
	#get item tags and itemimages	
	itemtags = item.itemingredient.all()
	itemimages = ItemImage.objects.filter(item = item)
	itemreviews = ItemReview.objects.filter(item=item)
	try:
		userinfo = UserInfo.objects.get(user=item.postedby)
	except:
		raise Http404
	#get Company
	try:
		company = CompanyInfo.objects.get(user=item.postedby)
	except:
		company = None
	if request.method =="GET":
		#check to see if user got redirected back after sending review querystring paramenter "reviewsuccess"
		if request.GET.get("reviewsuccess") == "true":
			reviewsuccess = "Thank you for submitting your review!"
		else:
			reviewsuccess = None
		if request.GET.get('addtipsuccess')=='true':
			addtipsuccess= "Thank you for submitting your tip!"
		else:
			addtipsuccess=None
		if request.GET.get("itemeditrequest")=='true':
			editrequest= "Thanks for submitting an edit request! It may take a couple of days to process it"
		else:
			editrequest = False
			#check to see if user has flagged the item
		flagged = None
		for userflagged in item.flaggedby.all():
			if request.user == userflagged:
				flagged = True
		#get number of stars that arent starred
		numunfilledstars = 5- item.rating
		form_args = {'item':item,'itemtags':itemtags,'itemimages':itemimages,'userinfo':userinfo,
				'company':company,'itemreviews':itemreviews,'reviewsuccess':reviewsuccess,'flagged':flagged,'addtipsuccess':addtipsuccess,'editrequest':editrequest,'numunfilledstars':numunfilledstars}
		return render_to_response("Mobile/itemsingle.html",form_args,context_instance=RequestContext(request))


def review(request,reviewtype,tag):
	if reviewtype == "item":
		#get item
		try:
			item = Item.objects.get(id = tag)
		except:
			raise Http404	

		reviews  = ItemReview.objects.filter(item = item)
	elif reviewtype =="company":
		try:
			company = CompanyInfo.objects.get(id = tag)
		except:
			raise Http404
		reviews = ItemReview.objects.filter(companyreviewed = company)
		item = None

	form_args= {'item':item, 'reviews':reviews,'reviewtype':reviewtype}
	return render_to_response("Mobile/reviews.html",form_args,context_instance=RequestContext(request))			

def tip(request,tiptype,tag):

	if tiptype == "item":
		#get item
		try:
			item = Item.objects.get(id = tag)
		except:
			raise Http404	

		tips  = ItemTip.objects.filter(item = item)
	elif tiptype =="company":
		try:
			company = CompanyInfo.objects.get(id = tag)
		except:
			raise Http404
		tips = ItemTip.objects.filter(companyinfo = company)
		item=None



	form_args= {'item':item, 'tips':tips,'tiptype':tiptype}
	return render_to_response("Mobile/tips.html",form_args,context_instance=RequestContext(request))		

def morepictures(request,tag):

	try:
		item = Item.objects.get(id= tag)
	except:
		raise Http404
	
	itemimages = ItemImage.objects.filter(item = item, mainimage= False)
	MEDIA_URL = settings.MEDIA_URL
	form_args= {'itemimages':itemimages,'item':item}
	return render_to_response("Mobile/morepictures.html",form_args,context_instance=RequestContext(request))		
	

def company(request,tag):
	try:
		company = CompanyInfo.objects.get(id= tag)
	except:
		raise Http404
	
	form_args= {'company':company}
	return render_to_response("Mobile/company.html",form_args,context_instance=RequestContext(request))

def menu(request,tag):
	print "hi"
	try:
		company = CompanyInfo.objects.get(id = tag)
	except:
		raise Http404

	items = Item.objects.filter(companyinfo = company)
	form_args={'items':items,'company':company}
	return render_to_response("Mobile/menu.html",form_args,context_instance=RequestContext(request))


def search(request):
	query = request.GET.get("q")
	category =request.GET.get('category')
	#set these query as session variablese
	request.session['searchquery'] = query
	request.session['searchcategory'] = category
	#get user location information
	get_user_city(request)
	try:
		userinfo = UserInfo.objects.get(user = request.user)
	except:
		userinfo = None
	#get whether a municipal is being searhed
	cityname = request.session['cityname']
	provincestatename = request.session['provincestate']
	#if last search was a city
	if request.session['iscity']:
		city  = City.objects.get(name = request.session['cityname'],provincestate = request.session['provincestate'])
		cityname = city.name
		iscity= True
		provincestatename = city.provincestate
	else:	
		municipal = Municipal.objects.get(name = request.session['cityname'], city__provincestate= request.session['provincestate'])
		cityname = municipal.name
		iscity = False
		provincestatename = municipal.city.provincestate
	#check to see if input municipal or city
	if iscity == True:
		citymunicipal = city
		items = Item.objects.filter(city = citymunicipal)
	else:
		citymunicipal = municipal
		items = Item.objects.filter(municipal= citymunicipal)
	

	if category == "Item" or category == "Ingredient":
		if category =="Item":
			featureditems=search_featured_items(query,items,"Item",iscity,citymunicipal,request)
			toprateditems=search_top_rated_items(query,items,"Item",iscity,citymunicipal,request)
			hiddengemitems=search_hiddem_gem_items(query,items,"Item",iscity,citymunicipal,request)
			cheapeatitems=search_cheap_eat_items(query,items,"Item",iscity,citymunicipal,request)
			everythingitems = search_everything_items(query,items,'Item',iscity,citymunicipal,request)
		#if user is searching for ingredient	
		elif category == "Ingredient":	
			featureditems=search_featured_items(query,items,"Ingredient",iscity,citymunicipal,request)
			toprateditems=search_top_rated_items(query,items,"Ingredient",iscity,citymunicipal,request)
			hiddengemitems=search_hiddem_gem_items(query,items,"Ingredient",iscity,citymunicipal,request)
			cheapeatitems=search_cheap_eat_items(query,items,"Ingredient",iscity,citymunicipal,request)
			everythingitems = search_everything_items(query,items,'Ingredient',iscity,citymunicipal,request)
	
	#set querystring
		request.session['filterquerystring']  = "?"+request.GET.urlencode()
		isfeatureditems = check_page_exists(featureditems)
		form_args={ 'category':category,'query':query,'featureditems':featureditems,'userinfo':userinfo,'isfeatureditems':isfeatureditems}
		return render_to_response("Mobile/search.html",form_args, context_instance=RequestContext(request))
	elif category == "Company":
		if iscity == True:
			companies = CompanyInfo.objects.filter(companyname__icontains = query, city=citymunicipal)
		else:			
			companies = CompanyInfo.objects.filter(companyname__icontains = query, municipal=citymunicipal)

		request.session['filterquerystring']  = "?"+request.GET.urlencode()	
		request.session['companies'] = companies
		paginator = Paginator(companies,25)
		companies = paginator.page(1)
		form_args={ 'category':category,'query':query,'userinfo':userinfo,'companies':companies}
		return render_to_response("Mobile/companylist.html",form_args, context_instance=RequestContext(request))


def mobilelogin(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect("/mobile")
	else:
		if request.method == "GET":
			return render_to_response("Mobile/login.html", context_instance=RequestContext(request))
		elif request.method =="POST":
		    username = request.POST.get('email')
		    password = request.POST.get('password')
	        user = authenticate(username=username, password = password)
	        if user is not None:
	            login(request, user)
	            return HttpResponseRedirect('/mobile')
	        else:
	            errormessage = "Your username or password is incorrect please try again"
	            form_args = {'errormessage':errormessage,'username':username}
	            return render_to_response("Mobile/login.html",form_args,context_instance = RequestContext(request))	


def signup(request):
	if request.method =="GET":
		return render_to_response("Mobile/signup.html",context_instance=RequestContext(request))
	if request.method == "POST":
		email = request.POST.get("email")
		password = request.POST.get("password")
		confirmpassword = request.POST.get("confirmpassword")
		firstname = request.POST.get("firstname")
		lastname = request.POST.get("lastname")
		GEOLOCATION_DIR = settings.GEOLOCATION_DIR
		gi4 = pygeoip.GeoIP(GEOLOCATION_DIR+'GeoLiteCity.dat', pygeoip.MEMORY_CACHE)
		clientipaddress= get_client_ip(request)
		clientipaddress= '174.7.194.220'
		clientaddress =  gi4.record_by_addr(clientipaddress)	
		city = clientaddress.get('city')
		provincestate = clientaddress.get('region_name')
		country = clientaddress.get('country_name')
		form_args = {'email':email,'password':password,'firstname':firstname,'lastname':lastname}
		#backend to check if all fields are filled
		if email is None:
			form_args['errormessage']= "You must enter an email"
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))
		if password is None:
			form_args['errormessage']= "You must enter an password"
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))
		if confirmpassword is None:
			form_args['errormessage']= "You must confirm your password"
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))
		if firstname is None:
			form_args['errormessage']= "You must enter your first name "
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))
		if lastname is None:
			form_args['errormessage']= "You must enter your last name"
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))
		if confirmpassword != password  is None:
			form_args['errormessage']= "Your passwords do not match "
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))
		#check to see if user exists in the system
		try: 
			User.objects.get(email = email)

			form_args['errormessage']= "That email address has been taken"
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))
		except:
			pass
		try:
			validate_email(email)
		except:
			form_args['errormessage']= "You must use a valid email"
			return render_to_response("Mobile/signup.html",form_args,context_instance=RequestContext(request))

		user =User.objects.create_user(email,email,password)
		user.first_name = firstname
		user.last_name = lastname
		user.save()
		userinfo = UserInfo(user=user)
		userinfo.first_name = firstname
		userinfo.last_name = lastname
		userinfo.country = country
		userinfo.provincestate = provincestate
		userinfo.city =city
		userinfo.save()
		#create following list 
		userfollowing = UserFollowing(user=userinfo)
		userfollowedby= UserFollowedBy(user=userinfo)
		userfollowing.save()
		userfollowedby.save()
		# send email to user
		form_args={'user':user}
		msg = EmailMessage('Welcome to WhereTheFork', render_to_string('Emails/signupuseremail.txt', form_args), 'info@wherethefork.com', ['dennislzf@gmail.com'])
		
		msg.send()
		loginuser=authenticate(username=email,password=password)
		login(request,loginuser)
		return HttpResponseRedirect("/mobile")
#view full site
def view_full_site(request):
	request.session['viewfullsite']= True
	request.session['hasvisited']=True
	return HttpResponse()

#function that deals with loading the different kinds of search items from dropdown
def loadsearchitems(request):
	searchitemtype = request.POST.get('value')
	if searchitemtype == "TopRated":
		items = request.session['toprateditems']
	elif searchitemtype == "HiddenGems":		
		items= request.session['hiddengemitems']
	elif searchitemtype == "CheapEats":		
		items = request.session['cheapeatitems']
	elif searchitemtype == "Everything":		
		items = request.session['everythingitems']
	else:
		raise Http404

	paginator = Paginator(items,25)
	items=  paginator.page(1)
	form_args ={'items':items,'MEDIA_URL':settings.MEDIA_URL,'STATIC_URL':settings.STATIC_URL}
	return HttpResponse(render_to_string('Mobile/loadmore.html',form_args))

#functiong that handles user clicking loadmore while doing search			
def loadmoresearch(request):
	searchitemtype = request.POST.get("loadmoretype")
	pageno = int(request.POST.get("pageno")) 
	if searchitemtype == "toprated":
		items = request.session['toprateditems']
	elif searchitemtype == "hiddengems":		
		items= request.session['hiddengemitems']
	elif searchitemtype == "cheapeats":		
		items = request.session['cheapeatitems']
	elif searchitemtype == "everything":		
		items = request.session['everythingitems']
	elif searchitemtype == "featured":		
		items = request.session['featureditems']
	else:
		raise Http404
	
	paginator = Paginator(items,25)
	#check to see if there are items on that page
	try:
		items=  paginator.page(pageno)
	except:
		items = None
	form_args ={'items':items,'MEDIA_URL':settings.MEDIA_URL,'STATIC_URL':settings.STATIC_URL}
	data = render_to_string('Mobile/loadmore.html',form_args)
	#check to see if there is next page
	try:
		isnextpage = paginator.page(pageno+1)
		isnextpage = True
	except:
		isnextpage  = None
	data = json.dumps({
		        'data': data,
			        'isnextpage': isnextpage,
				    })
	return HttpResponse(data, content_type='application/json')


