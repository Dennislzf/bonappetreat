-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: wherethefork
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `wherethefork`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `wherethefork` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `wherethefork`;

--
-- Table structure for table `Cities_city`
--

DROP TABLE IF EXISTS `Cities_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cities_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `provincestate` varchar(3) NOT NULL,
  `country` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cities_city`
--

LOCK TABLES `Cities_city` WRITE;
/*!40000 ALTER TABLE `Cities_city` DISABLE KEYS */;
INSERT INTO `Cities_city` VALUES (1,'Greater Vancouver','BC','Canada');
/*!40000 ALTER TABLE `Cities_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cities_municipal`
--

DROP TABLE IF EXISTS `Cities_municipal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cities_municipal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Cities_municipal_586a73b5` (`city_id`),
  CONSTRAINT `city_id_refs_id_3d00b01b` FOREIGN KEY (`city_id`) REFERENCES `Cities_city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cities_municipal`
--

LOCK TABLES `Cities_municipal` WRITE;
/*!40000 ALTER TABLE `Cities_municipal` DISABLE KEYS */;
INSERT INTO `Cities_municipal` VALUES (1,'West Vancouver',1),(2,'Vancouver',1);
/*!40000 ALTER TABLE `Cities_municipal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cities_neighbourhood`
--

DROP TABLE IF EXISTS `Cities_neighbourhood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cities_neighbourhood` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `municipal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Cities_neighbourhood_143b4d83` (`municipal_id`),
  CONSTRAINT `municipal_id_refs_id_429fddb8` FOREIGN KEY (`municipal_id`) REFERENCES `Cities_municipal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cities_neighbourhood`
--

LOCK TABLES `Cities_neighbourhood` WRITE;
/*!40000 ALTER TABLE `Cities_neighbourhood` DISABLE KEYS */;
INSERT INTO `Cities_neighbourhood` VALUES (1,'Ambleside',1);
/*!40000 ALTER TABLE `Cities_neighbourhood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Foods_food`
--

DROP TABLE IF EXISTS `Foods_food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Foods_food` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `foodtype` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Foods_food`
--

LOCK TABLES `Foods_food` WRITE;
/*!40000 ALTER TABLE `Foods_food` DISABLE KEYS */;
INSERT INTO `Foods_food` VALUES (1,'Food','Japanese');
/*!40000 ALTER TABLE `Foods_food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_item`
--

DROP TABLE IF EXISTS `Items_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postedby_id` int(11) NOT NULL,
  `companyinfo_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `allowpurchase` tinyint(1) NOT NULL,
  `category` varchar(30) NOT NULL,
  `neighbourhood_id` int(11),
  `description` varchar(300) NOT NULL,
  `fatfree` tinyint(1) NOT NULL,
  `vegan` tinyint(1) NOT NULL,
  `glutenfree` tinyint(1) NOT NULL,
  `peanutfree` tinyint(1) NOT NULL,
  `vegetarian` tinyint(1) NOT NULL,
  `lactosefree` tinyint(1) NOT NULL,
  `mainitemimage_id` int(11) DEFAULT NULL,
  `rating` smallint(5) unsigned NOT NULL,
  `viewcount` int(11) NOT NULL,
  `featured` tinyint(1) NOT NULL,
  `reviewcount` int(11) NOT NULL,
  `forkedcount` int(11) NOT NULL,
  `flagcount` int(11) NOT NULL,
  `municipal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Items_item_547003b2` (`postedby_id`),
  KEY `Items_item_86fb99c6` (`companyinfo_id`),
  KEY `Items_item_8016c63c` (`mainitemimage_id`),
  KEY `Items_item_5ce11cee` (`neighbourhood_id`),
  KEY `Items_item_143b4d83` (`municipal_id`),
  CONSTRAINT `companyinfo_id_refs_id_fdc7bc20` FOREIGN KEY (`companyinfo_id`) REFERENCES `UserInfo_companyinfo` (`id`),
  CONSTRAINT `mainitemimage_id_refs_id_7385ee21` FOREIGN KEY (`mainitemimage_id`) REFERENCES `Items_itemimage` (`id`),
  CONSTRAINT `municipal_id_refs_id_99c15e5b` FOREIGN KEY (`municipal_id`) REFERENCES `Cities_municipal` (`id`),
  CONSTRAINT `neighbourhood_id_refs_id_f3227c26` FOREIGN KEY (`neighbourhood_id`) REFERENCES `Cities_neighbourhood` (`id`),
  CONSTRAINT `postedby_id_refs_id_c011dfdc` FOREIGN KEY (`postedby_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_item`
--

LOCK TABLES `Items_item` WRITE;
/*!40000 ALTER TABLE `Items_item` DISABLE KEYS */;
INSERT INTO `Items_item` VALUES (1,1,1,'sushi',2.00,0,'Japanese',NULL,'So good',0,0,0,0,0,0,1,0,0,0,0,1,0,1);
/*!40000 ALTER TABLE `Items_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_item_flaggedby`
--

DROP TABLE IF EXISTS `Items_item_flaggedby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_item_flaggedby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Items_item_flaggedby_item_id_4297ba9280897024_uniq` (`item_id`,`user_id`),
  KEY `Items_item_flaggedby_67b70d25` (`item_id`),
  KEY `Items_item_flaggedby_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_93f9fab6` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `item_id_refs_id_ef3aacd9` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_item_flaggedby`
--

LOCK TABLES `Items_item_flaggedby` WRITE;
/*!40000 ALTER TABLE `Items_item_flaggedby` DISABLE KEYS */;
/*!40000 ALTER TABLE `Items_item_flaggedby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_item_forkedby`
--

DROP TABLE IF EXISTS `Items_item_forkedby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_item_forkedby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Items_item_forkedby_item_id_6c3e4ea8a235aef8_uniq` (`item_id`,`user_id`),
  KEY `Items_item_forkedby_67b70d25` (`item_id`),
  KEY `Items_item_forkedby_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_2a058a32` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `item_id_refs_id_539bd535` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_item_forkedby`
--

LOCK TABLES `Items_item_forkedby` WRITE;
/*!40000 ALTER TABLE `Items_item_forkedby` DISABLE KEYS */;
INSERT INTO `Items_item_forkedby` VALUES (2,1,1);
/*!40000 ALTER TABLE `Items_item_forkedby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itemimage`
--

DROP TABLE IF EXISTS `Items_itemimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itemimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `postedby_id` int(11) NOT NULL,
  `finished` tinyint(1) NOT NULL,
  `mainimage` tinyint(1) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Items_itemimage_67b70d25` (`item_id`),
  KEY `Items_itemimage_547003b2` (`postedby_id`),
  CONSTRAINT `postedby_id_refs_id_8671e82` FOREIGN KEY (`postedby_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `item_id_refs_id_eb3f65db` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itemimage`
--

LOCK TABLES `Items_itemimage` WRITE;
/*!40000 ALTER TABLE `Items_itemimage` DISABLE KEYS */;
INSERT INTO `Items_itemimage` VALUES (1,1,1,1,1,'','photos/Item/None/download_18.jpg'),(2,NULL,1,0,0,'','photos/Item/None/download_19.jpg');
/*!40000 ALTER TABLE `Items_itemimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itempurchase`
--

DROP TABLE IF EXISTS `Items_itempurchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itempurchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `priceperdozen` decimal(6,2) DEFAULT NULL,
  `pricehalfdozen` decimal(6,2) DEFAULT NULL,
  `allowpickup` tinyint(1) NOT NULL,
  `allowshipping` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Items_itempurchase_67b70d25` (`item_id`),
  CONSTRAINT `item_id_refs_id_dc3147a6` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itempurchase`
--

LOCK TABLES `Items_itempurchase` WRITE;
/*!40000 ALTER TABLE `Items_itempurchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `Items_itempurchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itemreview`
--

DROP TABLE IF EXISTS `Items_itemreview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itemreview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `postedby_id` int(11) NOT NULL,
  `userinfo_id` int(11) DEFAULT NULL,
  `companyreviewed_id` int(11) NOT NULL,
  `rating` smallint(5) unsigned NOT NULL,
  `numrecommends` smallint(5) unsigned NOT NULL,
  `comments` varchar(1000) DEFAULT NULL,
  `dateadded` datetime NOT NULL,
  `flagcount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Items_itemreview_67b70d25` (`item_id`),
  KEY `Items_itemreview_547003b2` (`postedby_id`),
  KEY `Items_itemreview_6b192ca7` (`userinfo_id`),
  KEY `Items_itemreview_a082dbc5` (`companyreviewed_id`),
  CONSTRAINT `companyreviewed_id_refs_id_c0a0b69a` FOREIGN KEY (`companyreviewed_id`) REFERENCES `UserInfo_companyinfo` (`id`),
  CONSTRAINT `item_id_refs_id_bf1a7ae1` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`),
  CONSTRAINT `postedby_id_refs_id_d7a8ff3e` FOREIGN KEY (`postedby_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `userinfo_id_refs_id_7dbf7b71` FOREIGN KEY (`userinfo_id`) REFERENCES `UserInfo_userinfo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itemreview`
--

LOCK TABLES `Items_itemreview` WRITE;
/*!40000 ALTER TABLE `Items_itemreview` DISABLE KEYS */;
INSERT INTO `Items_itemreview` VALUES (1,1,1,1,1,5,1,'This was by far the best sushi ever','2013-09-12 01:44:14',0);
/*!40000 ALTER TABLE `Items_itemreview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itemreview_flaggedby`
--

DROP TABLE IF EXISTS `Items_itemreview_flaggedby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itemreview_flaggedby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemreview_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Items_itemreview_flaggedby_itemreview_id_625b7deb4052ca4c_uniq` (`itemreview_id`,`user_id`),
  KEY `Items_itemreview_flaggedby_e3306bdf` (`itemreview_id`),
  KEY `Items_itemreview_flaggedby_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_3a138da0` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `itemreview_id_refs_id_ca354e97` FOREIGN KEY (`itemreview_id`) REFERENCES `Items_itemreview` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itemreview_flaggedby`
--

LOCK TABLES `Items_itemreview_flaggedby` WRITE;
/*!40000 ALTER TABLE `Items_itemreview_flaggedby` DISABLE KEYS */;
/*!40000 ALTER TABLE `Items_itemreview_flaggedby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itemreview_recommendedby`
--

DROP TABLE IF EXISTS `Items_itemreview_recommendedby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itemreview_recommendedby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemreview_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Items_itemreview_recommende_itemreview_id_2b7593361b8bad9d_uniq` (`itemreview_id`,`user_id`),
  KEY `Items_itemreview_recommendedby_e3306bdf` (`itemreview_id`),
  KEY `Items_itemreview_recommendedby_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_ae02424f` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `itemreview_id_refs_id_eaef5a46` FOREIGN KEY (`itemreview_id`) REFERENCES `Items_itemreview` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itemreview_recommendedby`
--

LOCK TABLES `Items_itemreview_recommendedby` WRITE;
/*!40000 ALTER TABLE `Items_itemreview_recommendedby` DISABLE KEYS */;
INSERT INTO `Items_itemreview_recommendedby` VALUES (1,1,1);
/*!40000 ALTER TABLE `Items_itemreview_recommendedby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itemtag`
--

DROP TABLE IF EXISTS `Items_itemtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itemtag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Items_itemtag_67b70d25` (`item_id`),
  CONSTRAINT `item_id_refs_id_83b18754` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itemtag`
--

LOCK TABLES `Items_itemtag` WRITE;
/*!40000 ALTER TABLE `Items_itemtag` DISABLE KEYS */;
INSERT INTO `Items_itemtag` VALUES (1,1,'delicious'),(2,1,'yummy');
/*!40000 ALTER TABLE `Items_itemtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itemtip`
--

DROP TABLE IF EXISTS `Items_itemtip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itemtip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `postedby_id` int(11) NOT NULL,
  `companyinfo_id` int(11) NOT NULL,
  `userinfo_id` int(11) NOT NULL,
  `tip` varchar(200) NOT NULL,
  `numrecommends` int(11) NOT NULL,
  `dateposted` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Items_itemtip_67b70d25` (`item_id`),
  KEY `Items_itemtip_547003b2` (`postedby_id`),
  KEY `Items_itemtip_86fb99c6` (`companyinfo_id`),
  KEY `Items_itemtip_6b192ca7` (`userinfo_id`),
  CONSTRAINT `userinfo_id_refs_id_83662665` FOREIGN KEY (`userinfo_id`) REFERENCES `UserInfo_userinfo` (`id`),
  CONSTRAINT `companyinfo_id_refs_id_6093c020` FOREIGN KEY (`companyinfo_id`) REFERENCES `UserInfo_companyinfo` (`id`),
  CONSTRAINT `item_id_refs_id_260e6475` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`),
  CONSTRAINT `postedby_id_refs_id_f9e81c24` FOREIGN KEY (`postedby_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itemtip`
--

LOCK TABLES `Items_itemtip` WRITE;
/*!40000 ALTER TABLE `Items_itemtip` DISABLE KEYS */;
INSERT INTO `Items_itemtip` VALUES (1,1,1,1,1,'Order with some green tea to make it the best',1,'2013-09-12 01:44:14');
/*!40000 ALTER TABLE `Items_itemtip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_itemtip_recommendedby`
--

DROP TABLE IF EXISTS `Items_itemtip_recommendedby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_itemtip_recommendedby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemtip_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Items_itemtip_recommendedby_itemtip_id_472f05979912c51d_uniq` (`itemtip_id`,`user_id`),
  KEY `Items_itemtip_recommendedby_ad4c6ee1` (`itemtip_id`),
  KEY `Items_itemtip_recommendedby_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_6bde01e9` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `itemtip_id_refs_id_53c2fc66` FOREIGN KEY (`itemtip_id`) REFERENCES `Items_itemtip` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_itemtip_recommendedby`
--

LOCK TABLES `Items_itemtip_recommendedby` WRITE;
/*!40000 ALTER TABLE `Items_itemtip_recommendedby` DISABLE KEYS */;
INSERT INTO `Items_itemtip_recommendedby` VALUES (1,1,1);
/*!40000 ALTER TABLE `Items_itemtip_recommendedby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items_userforked`
--

DROP TABLE IF EXISTS `Items_userforked`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items_userforked` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Items_userforked_67b70d25` (`item_id`),
  KEY `Items_userforked_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_89d5ff0d` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `item_id_refs_id_7894836a` FOREIGN KEY (`item_id`) REFERENCES `Items_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items_userforked`
--

LOCK TABLES `Items_userforked` WRITE;
/*!40000 ALTER TABLE `Items_userforked` DISABLE KEYS */;
INSERT INTO `Items_userforked` VALUES (2,1,1);
/*!40000 ALTER TABLE `Items_userforked` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserInfo_companyinfo`
--

DROP TABLE IF EXISTS `UserInfo_companyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInfo_companyinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(200) NOT NULL,
  `companylogo` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city_id` int(11) NOT NULL,
  `neighbourhood_id` int(11) DEFAULT NULL,
  `municipal_id` int(11) NOT NULL,
  `postalcode` varchar(10) NOT NULL,
  `phonenumber` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `visitstopage` int(11) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `slogan` varchar(400) DEFAULT NULL,
  `itemsposted` int(11) NOT NULL,
  `useradded` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UserInfo_companyinfo_586a73b5` (`city_id`),
  KEY `UserInfo_companyinfo_5ce11cee` (`neighbourhood_id`),
  KEY `UserInfo_companyinfo_143b4d83` (`municipal_id`),
  CONSTRAINT `municipal_id_refs_id_cf08f8a` FOREIGN KEY (`municipal_id`) REFERENCES `Cities_municipal` (`id`),
  CONSTRAINT `city_id_refs_id_c5fc7770` FOREIGN KEY (`city_id`) REFERENCES `Cities_city` (`id`),
  CONSTRAINT `neighbourhood_id_refs_id_b29f95f5` FOREIGN KEY (`neighbourhood_id`) REFERENCES `Cities_neighbourhood` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserInfo_companyinfo`
--

LOCK TABLES `UserInfo_companyinfo` WRITE;
/*!40000 ALTER TABLE `UserInfo_companyinfo` DISABLE KEYS */;
INSERT INTO `UserInfo_companyinfo` VALUES (1,'The Best Sweets','companydefault.png','1428 Bramwell Road',1,NULL,1,'v7s2n0','6043452101','',0,NULL,NULL,1,0);
/*!40000 ALTER TABLE `UserInfo_companyinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserInfo_userfoodpreference`
--

DROP TABLE IF EXISTS `UserInfo_userfoodpreference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInfo_userfoodpreference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tag` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UserInfo_userfoodpreference_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_77867c63` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserInfo_userfoodpreference`
--

LOCK TABLES `UserInfo_userfoodpreference` WRITE;
/*!40000 ALTER TABLE `UserInfo_userfoodpreference` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserInfo_userfoodpreference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserInfo_userinfo`
--

DROP TABLE IF EXISTS `UserInfo_userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInfo_userinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `profilepicture` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `creditcardposted` tinyint(1) NOT NULL,
  `country` varchar(50) NOT NULL,
  `provincestate` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `twitterhandle` varchar(30) NOT NULL,
  `website` varchar(50) NOT NULL,
  `instagramhandle` varchar(30) NOT NULL,
  `itemsposted` int(11) NOT NULL,
  `pintresthandle` varchar(30) NOT NULL,
  `picturesposted` int(11) NOT NULL,
  `numusersfollowing` int(11) NOT NULL,
  `numusersfollowed` int(11) NOT NULL,
  `numberofpoints` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UserInfo_userinfo_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_7cc3d754` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserInfo_userinfo`
--

LOCK TABLES `UserInfo_userinfo` WRITE;
/*!40000 ALTER TABLE `UserInfo_userinfo` DISABLE KEYS */;
INSERT INTO `UserInfo_userinfo` VALUES (1,1,'Dennis','Lau','defaultuser.jpg','',0,'','','','','','',1,'',1,0,0,3);
/*!40000 ALTER TABLE `UserInfo_userinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserInfo_userinfo_usersfollowed`
--

DROP TABLE IF EXISTS `UserInfo_userinfo_usersfollowed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInfo_userinfo_usersfollowed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userinfo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserInfo_userinfo_usersfollow_userinfo_id_4ca4f136c4405cc5_uniq` (`userinfo_id`,`user_id`),
  KEY `UserInfo_userinfo_usersfollowed_6b192ca7` (`userinfo_id`),
  KEY `UserInfo_userinfo_usersfollowed_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_1365e853` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `userinfo_id_refs_id_e834d6c` FOREIGN KEY (`userinfo_id`) REFERENCES `UserInfo_userinfo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserInfo_userinfo_usersfollowed`
--

LOCK TABLES `UserInfo_userinfo_usersfollowed` WRITE;
/*!40000 ALTER TABLE `UserInfo_userinfo_usersfollowed` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserInfo_userinfo_usersfollowed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserInfo_userinfo_usersfollowing`
--

DROP TABLE IF EXISTS `UserInfo_userinfo_usersfollowing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInfo_userinfo_usersfollowing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userinfo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserInfo_userinfo_usersfollow_userinfo_id_5fede6487d5afcc3_uniq` (`userinfo_id`,`user_id`),
  KEY `UserInfo_userinfo_usersfollowing_6b192ca7` (`userinfo_id`),
  KEY `UserInfo_userinfo_usersfollowing_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_abd17ab5` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `userinfo_id_refs_id_f43dda3e` FOREIGN KEY (`userinfo_id`) REFERENCES `UserInfo_userinfo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserInfo_userinfo_usersfollowing`
--

LOCK TABLES `UserInfo_userinfo_usersfollowing` WRITE;
/*!40000 ALTER TABLE `UserInfo_userinfo_usersfollowing` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserInfo_userinfo_usersfollowing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add log entry',7,'add_logentry'),(20,'Can change log entry',7,'change_logentry'),(21,'Can delete log entry',7,'delete_logentry'),(22,'Can add migration history',8,'add_migrationhistory'),(23,'Can change migration history',8,'change_migrationhistory'),(24,'Can delete migration history',8,'delete_migrationhistory'),(25,'Can add user info',9,'add_userinfo'),(26,'Can change user info',9,'change_userinfo'),(27,'Can delete user info',9,'delete_userinfo'),(28,'Can add company info',10,'add_companyinfo'),(29,'Can change company info',10,'change_companyinfo'),(30,'Can delete company info',10,'delete_companyinfo'),(31,'Can add user food preference',11,'add_userfoodpreference'),(32,'Can change user food preference',11,'change_userfoodpreference'),(33,'Can delete user food preference',11,'delete_userfoodpreference'),(34,'Can add item',12,'add_item'),(35,'Can change item',12,'change_item'),(36,'Can delete item',12,'delete_item'),(37,'Can add item image',13,'add_itemimage'),(38,'Can change item image',13,'change_itemimage'),(39,'Can delete item image',13,'delete_itemimage'),(40,'Can add item tag',14,'add_itemtag'),(41,'Can change item tag',14,'change_itemtag'),(42,'Can delete item tag',14,'delete_itemtag'),(43,'Can add item purchase',15,'add_itempurchase'),(44,'Can change item purchase',15,'change_itempurchase'),(45,'Can delete item purchase',15,'delete_itempurchase'),(46,'Can add item review',16,'add_itemreview'),(47,'Can change item review',16,'change_itemreview'),(48,'Can delete item review',16,'delete_itemreview'),(49,'Can add user forked',17,'add_userforked'),(50,'Can change user forked',17,'change_userforked'),(51,'Can delete user forked',17,'delete_userforked'),(52,'Can add item tip',18,'add_itemtip'),(53,'Can change item tip',18,'change_itemtip'),(54,'Can delete item tip',18,'delete_itemtip'),(55,'Can add food',19,'add_food'),(56,'Can change food',19,'change_food'),(57,'Can delete food',19,'delete_food'),(58,'Can add city',20,'add_city'),(59,'Can change city',20,'change_city'),(60,'Can delete city',20,'delete_city'),(61,'Can add municipal',21,'add_municipal'),(62,'Can change municipal',21,'change_municipal'),(63,'Can delete municipal',21,'delete_municipal'),(64,'Can add neighbourhood',22,'add_neighbourhood'),(65,'Can change neighbourhood',22,'change_neighbourhood'),(66,'Can delete neighbourhood',22,'delete_neighbourhood');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'d@d.com','Dennis','Lau','d@d.com','pbkdf2_sha256$10000$8ighMExOCbCt$ZAH4DUFf3bcEHcIbjs2AipBsyhWzq0fNL/QkUCJbzUg=',0,1,0,'2013-09-11 20:09:01','2013-09-11 20:08:58');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`),
  CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'log entry','admin','logentry'),(8,'migration history','south','migrationhistory'),(9,'user info','UserInfo','userinfo'),(10,'company info','UserInfo','companyinfo'),(11,'user food preference','UserInfo','userfoodpreference'),(12,'item','Items','item'),(13,'item image','Items','itemimage'),(14,'item tag','Items','itemtag'),(15,'item purchase','Items','itempurchase'),(16,'item review','Items','itemreview'),(17,'user forked','Items','userforked'),(18,'item tip','Items','itemtip'),(19,'food','Foods','food'),(20,'city','Cities','city'),(21,'municipal','Cities','municipal'),(22,'neighbourhood','Cities','neighbourhood');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0fe956e6cc2b31f0a4c867d2cd66acbd','NjdmOWEyMjk2ZjM2YTNmMDg4ZmM1YjZmYTcxOTRiMTk5OWQ3ZWI0YzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-09-25 20:09:01');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'UserInfo','0001_initial','2013-09-11 19:16:50'),(2,'Users','0001_initial','2013-09-11 19:17:05'),(3,'Items','0001_initial','2013-09-11 19:17:22'),(4,'Foods','0001_initial','2013-09-11 19:17:36'),(5,'Cities','0001_initial','2013-09-11 19:17:51'),(6,'Cities','0002_auto__del_field_neighbourhood_city__add_field_neighbourhood_municipal','2013-09-11 20:27:08'),(7,'Items','0002_auto__del_field_item_city__add_field_item_municipal__chg_field_item_ne','2013-09-11 22:19:18'),(8,'Items','0003_auto__chg_field_item_neighbourhood__chg_field_item_category','2013-09-12 01:44:10');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-11 21:21:30
