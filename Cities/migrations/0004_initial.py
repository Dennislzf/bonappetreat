# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'City'
        db.create_table('Cities_city', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('provincestate', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal('Cities', ['City'])

        # Adding model 'Municipal'
        db.create_table('Cities_municipal', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Cities.City'])),
        ))
        db.send_create_signal('Cities', ['Municipal'])

        # Adding model 'Neighbourhood'
        db.create_table('Cities_neighbourhood', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Cities.City'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('Cities', ['Neighbourhood'])


    def backwards(self, orm):
        # Deleting model 'City'
        db.delete_table('Cities_city')

        # Deleting model 'Municipal'
        db.delete_table('Cities_municipal')

        # Deleting model 'Neighbourhood'
        db.delete_table('Cities_neighbourhood')


    models = {
        'Cities.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'Cities.municipal': {
            'Meta': {'object_name': 'Municipal'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Cities.neighbourhood': {
            'Meta': {'object_name': 'Neighbourhood'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['Cities']