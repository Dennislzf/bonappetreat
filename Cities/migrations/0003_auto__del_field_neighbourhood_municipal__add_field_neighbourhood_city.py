# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Neighbourhood.municipal'
        db.delete_column('Cities_neighbourhood', 'municipal_id')

        # Adding field 'Neighbourhood.city'
        db.add_column('Cities_neighbourhood', 'city',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['Cities.City']),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Neighbourhood.municipal'
        db.add_column('Cities_neighbourhood', 'municipal',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['Cities.Municipal']),
                      keep_default=False)

        # Deleting field 'Neighbourhood.city'
        db.delete_column('Cities_neighbourhood', 'city_id')


    models = {
        'Cities.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincestate': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'Cities.municipal': {
            'Meta': {'object_name': 'Municipal'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'Cities.neighbourhood': {
            'Meta': {'object_name': 'Neighbourhood'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Cities.City']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['Cities']