#add admin capabilities
from django.contrib import admin
from Cities.models import City, Municipal, Neighbourhood

admin.site.register(City)
admin.site.register(Municipal)
admin.site.register(Neighbourhood)