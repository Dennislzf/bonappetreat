from django.db import models

class City(models.Model):
	name = models.CharField(max_length=50)
	provincestate = models.CharField(max_length=3)
	country = models.CharField(max_length=30)
class Municipal(models.Model):
	name = models.CharField(max_length=50)
	city = models.ForeignKey(City)

# Create your models here.
class Neighbourhood(models.Model):
	city = models.ForeignKey(City)
	name = models.CharField(max_length=50)

 
