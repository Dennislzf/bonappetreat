# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Food'
        db.create_table('Foods_food', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('foodtype', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('Foods', ['Food'])


    def backwards(self, orm):
        # Deleting model 'Food'
        db.delete_table('Foods_food')


    models = {
        'Foods.food': {
            'Meta': {'object_name': 'Food'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'foodtype': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['Foods']